#include <gpiod.h>

namespace Gpio
{
	using Line = struct gpiod_line*;
	void init();
	Line getLine(int line_num);
	Line p(int line_num);
	void pinModeOutput(Line line, bool val=0);
	void pinModeInput(Line line);
	void digitalWrite(Line line, bool val);
	bool digitalRead(Line line);
}


