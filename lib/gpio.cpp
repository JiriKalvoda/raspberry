#include "gpio.h"
#include <stdio.h>
#include <unistd.h>

namespace Gpio
{
	struct gpiod_chip *chip;
	void init()
	{
		const char *chipname = "gpiochip0";
		chip = gpiod_chip_open_by_name(chipname);
		if (!chip) {
			perror("Open chip failed\n");
			exit(1);
		}
	}
	Line line_cache[200];
	Line getLine(int line_num)
	{
		auto line = gpiod_chip_get_line(chip, line_num);
		if (!line) {
			perror("Get line failed\n");
			exit(1);
		}
		return line;
	}
	Line p(int line_num)
	{
		if(!line_cache[line_num])
			line_cache[line_num] = getLine(line_num);
		return line_cache[line_num];
	}
	void pinModeOutput(Line line, bool val)
	{
		int ret = gpiod_line_request_output(line, "Consumer", val);
		if (ret < 0) {
			perror("Request line as output failed");
			exit(1);
		}
	}
	void pinModeInput(Line line)
	{
		int ret = gpiod_line_request_input(line, "Consumer");
		if (ret < 0) {
			perror("Request line as input failed\n");
			exit(1);
		}
	}
	void digitalWrite(Line line, bool val)
	{
		int ret = gpiod_line_set_value(line, val);
		if (ret < 0) {
			perror("Set line output failed\n");
			exit(1);
		}
	}
	bool digitalRead(Line line)
	{
		int val = gpiod_line_get_value(line);
		if (val < 0) {
			perror("Read line input failed\n");
			exit(1);
		}
		return val;
	}
}
