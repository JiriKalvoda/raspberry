#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wiringPi.h>
#include <sys/stat.h>

#include "../led/led-include.h"
 
#define TRIG 13
#define ECHO 12

LED led;


using ui = unsigned int;

const char * fName="/tmp/ultrasonic-pokojik";

#define MAXTIMINGS	85
volatile ui times[MAXTIMINGS];
volatile ui vals[MAXTIMINGS];
volatile int timesLen;
volatile bool interuptActive=0;
void interupt(void)
{
	if(timesLen < MAXTIMINGS)
	{
		times[timesLen]=micros();
		vals[timesLen]=digitalRead(ECHO);
		timesLen++;
	}
}
 
void setup() {
        wiringPiSetup();
        pinMode(TRIG, OUTPUT);
        pinMode(ECHO, INPUT);
 
        //TRIG pin must start LOW
        digitalWrite(TRIG, 0);
        delay(100);
	if ( wiringPiISR (ECHO, INT_EDGE_BOTH, &interupt) < 0 )
		fprintf (stderr, "Unable to setup ISR:\n");
}
 
int getCM() {
	timesLen=0;
	bool onstate=0;
	digitalWrite(TRIG, onstate);
	delayMicroseconds(100);
	digitalWrite(TRIG, !onstate);
	delay(10);
	if(timesLen != 2 && timesLen!=3) return -1;
	if(vals[timesLen-2]!=1||vals[timesLen-1]!=0) return -2;
	int travelTime = times[timesLen-1]-times[timesLen-2];
	int distance = travelTime / 5.8;
	return distance;
}
 
int main() {
    setup();
    led.loadTree(Tree::root);
    {
	    FILE  * f = fopen(fName,"w");
	    fprintf(f,"1\n");
	    fclose(f);
	    chmod(fName,0666);
    }
    int last=-1;
    int numInLine=0;
    int longState=0;
    int fInput;
    int fIterator=1<<30;
    FILE * logf = fopen("/tmp/ultraLog","w");
    while (1)
    {
	    if(fIterator > 100)
	{
	    FILE  * f = fopen(fName,"r");
	    if(f)
	    {
		    fscanf(f,"%d",&fInput);
		    fclose(f);
	    }
		if(!fInput)
		{
			delay(1000);
			continue;
		}
		fIterator=0;
	}
	    fIterator++;
        int dist=getCM();
	int state = dist < 800;
	fprintf(logf,"%c%d\n",state?'Y':'N',dist);
        //printf("Distance: %dmm %d\n", dist,state);
	if(state!=last)
	{
		//printf("%d\n",numInLine);
		last=state;
		numInLine=0;
	}
	//if(state) printf("\t%d\n",dist);
	if(numInLine>=10 && longState!=state && state==0) 
	{
		longState = state;
		fprintf(logf,"OFF\n");
	}
	if(numInLine>=3 && longState!=state && state==1) 
	{
		fprintf(logf,"CHANGE\n");
		longState = state;
		printf("CHANGE\n");
		fflush(stdout);
		led.f("pokojik","0","1","1","1","_","_","second",LED::IFNOTEXIST);
		led.operation("pokojik","_","second",LED::FILESOUTPUT,LED::SWAP);
	}
	numInLine++;
    }
 
    return 0;
}

