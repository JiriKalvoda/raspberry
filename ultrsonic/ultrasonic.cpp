#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wiringPi.h>
#include <sys/stat.h>

#include "../led/led-include.h"


LED led;


using ui = unsigned int;

const char * fName="/tmp/ultrasonic-pokojik";

#define MAXTIMINGS	85
volatile ui times[MAXTIMINGS];
volatile ui vals[MAXTIMINGS];
volatile int timesLen;
volatile bool interuptActive=0;
int actual_pin_echo = -1;
void interupt(void)
{
	if(actual_pin_echo < 0) return;
	if(timesLen < MAXTIMINGS)
	{
		times[timesLen]=micros();
		vals[timesLen]=digitalRead(actual_pin_echo);
		timesLen++;
	}
}

struct Ultrasonic
{
	int pin_trig;
	int pin_echo;
	void setup(int TRIG, int ECHO)
	{
		pin_trig = TRIG;
		pin_echo = ECHO;
		wiringPiSetup();
		pinMode(pin_trig, OUTPUT);
		pinMode(pin_echo, INPUT);

		//pin_trig pin must start LOW
		digitalWrite(pin_trig, 0);
		delay(100);
		if ( wiringPiISR (pin_echo, INT_EDGE_BOTH, &interupt) < 0 )
			fprintf (stderr, "Unable to setup ISR:\n");
	}

	int get() {
		timesLen=0;
		actual_pin_echo = pin_echo;
		bool onstate=0;
		digitalWrite(pin_trig, onstate);
		delayMicroseconds(100);
		digitalWrite(pin_trig, !onstate);
		delay(20);
		if(timesLen != 2 && timesLen!=3) return -1;
		if(vals[timesLen-2]!=1||vals[timesLen-1]!=0) return -2;
		int travelTime = times[timesLen-1]-times[timesLen-2];
		int distance = travelTime / 5.8;
		//for(int i=0;i<timesLen;i++) printf("%d@%3d\n", vals[i], times[i]);
		return distance;
	}
};

int main() {
	Ultrasonic ultra_a;
	Ultrasonic ultra_b;
	ultra_a.setup(24,25);
	ultra_b.setup(22,23);

    while (1)
    {
		int a,b;
        a=ultra_a.get();
		delay(10);
        b=ultra_b.get();
		delay(10);
		printf("   %7d %7d\n", a, b);
    }

    return 0;
}
