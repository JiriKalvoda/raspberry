#include <string>
#include <functional>
#include "../led/led-include.h"
#include <map>

using namespace std;
using ld = long double;
using ll = long long;
using ui = unsigned int;
using Tree::Key;

extern LED ledImp;

const int MAPH=17;
const int MAPW=14;

using KeyModeKey =  function<void ()>;
struct KeyMode
{
	KeyModeKey * map[MAPH][MAPW];
};

extern map<pair<string,int>,KeyMode> keyModes;

extern const char * mode;
extern Key::MOD modif;


struct Grup
{
	int root_row, root_col;
	vector<pair<int,int>> keys;
	Grup();
	Grup(vector<pair<int,int>> in);
};
