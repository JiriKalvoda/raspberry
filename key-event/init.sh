#!/bin/bash


device="$1"
name="$2"

dn="$device-$name"


i3_lib=~/userconfig/i3/config-gen

confln(){
	~/userconfig/confln "$@"
}


confln $i3_lib/led-cmddef.km ./
confln $i3_lib/ssh-cmddef.km ./
confln $i3_lib/led-blatto-1.km ./
confln $i3_lib/led-blatto-sp.km ./

if [[ $device = blattes ]]
then
cat > map-gen << AMEN
#!/bin/env keymap-cpp-keyboard
LOAD("led-blatto-sp.km")
AMEN
chmod +x map-gen
	exit
fi

cat > map-gen << AMEN
#!/bin/env keymap-cpp-keyboard
LOAD("led-blatto-1.km")
AMEN
chmod +x map-gen


if [[ $dn == rpi0-k1 ]] || [[ $dn == rpi7-k1 ]]
then
cat >> map-gen << AMEN
M(K_Pause, release_action=MONITOR_POWER("toggle"))
AMEN
fi
