
#include "include.h"
namespace k
{
	namespace led
	{
		const int NUMSTRLEN=10;
		KeyModeKey * light(const char * name, int par, int maxVal){return new KeyModeKey([name,par,maxVal]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.light(name,val, maxVal);
			});}
		KeyModeKey * lightBoost(const char * name, int par, int maxVal, double maxLightBoost){return new KeyModeKey([name,par,maxVal,maxLightBoost]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.lightBoost(name,val, maxVal,maxLightBoost);
			});}
		KeyModeKey * color(const char * name, int par, int maxVal){return new KeyModeKey([name,par,maxVal]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.color(name,val, maxVal);
			});}
		KeyModeKey * stop(const char * name){return new KeyModeKey([name]()
			{
				ledImp.f(name,"-1","-1","-1","-1");
			});}
		KeyModeKey * lightSpeed(const char * name, int par){return new KeyModeKey([name,par]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.lightSpeed(name,val);
			});}
		KeyModeKey * colorSpeed(const char * name, int par){return new KeyModeKey([name,par]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.colorSpeed(name,val);
			});}
		KeyModeKey * lightBig(const char * name, int par){return new KeyModeKey([name,par]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.lightBig(name,val);
			});}
		KeyModeKey * colorBig(const char * name, int par){return new KeyModeKey([name,par]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.colorBig(name,val);
			});}
	}
	KeyModeKey * mqtt_pub(const char* m, const char *top, const char * data, bool retined){return new KeyModeKey([m,top,data,retined]()
		{
			ledImp.mqttMessage(Tree::mosquitto_map[m], data, top, retined);
		});}
	KeyModeKey * cmd(const char * in){return new KeyModeKey([in]()
		{
		char val[12345];sprintf(val,"(%s)&",in);system(val);puts(val);fflush(stdout);
		});}
	KeyModeKey * goMode(const char * in){return new KeyModeKey([in]()
		{
			mode=in;
		});}
	KeyModeKey * undef = new KeyModeKey([](){});
	KeyModeKey * null = undef;
}
