#include <string>
#include <iostream>
#include <filesystem>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <chrono>
#include <thread>

#include "include.h"




LED ledImp;

//using Keymap = std::map<Key,std::function<void ()> >;

struct Grup;
Grup * inGrup[MAPH][MAPW];

Grup::Grup(){}
Grup::Grup(vector<pair<int,int>> in):keys(in)
{
	root_row = keys[0].first;
	root_col = keys[0].second;
	for(pair<int,int> it:keys) inGrup[it.first][it.second] = this;
}



ll activedAt[MAPH][MAPW];

const char * mode = "default";
Key::MOD modif=Key::MOD(0);



//Keymap * rootKeyMap;
//Keymap * actualKeyMap;
//Keymap * allModeKeyMap;

const int NUMSTRLEN=10;



#include "convert.cpp"



using namespace std;
#ifdef DEB
#define D if(1)
#else
#define D if(0)
#endif

Key::MOD mod;

const char * led_prefix = 0;

void setKeyboardLed(const char * suffix, bool state)
{
	if(!led_prefix) return;
	char fname[1234];
	sprintf(fname, "%s%s/brightness", led_prefix, suffix);
	FILE * f = fopen(fname, "w");
	if(!f) 
	{
		fprintf(stderr, "LED ERR: Cannot open %s.\n",fname);
		return;
	}
	fprintf(f,"%d", state);
	fclose(f);
}

void keyEvent(const char * dev)
{
    struct input_event ev;
    ssize_t n;
    int fd;

    for(int i=0;(fd = open(dev, O_RDONLY))<0;i++)
	{
		if (i==30) {
			fprintf(stderr, "KEYINPUT ERR: Cannot open %s: %s.\n", dev, strerror(errno));
			//sleep(1);
			return;
		}
		using namespace std::chrono_literals;
		std::this_thread::sleep_for(1s);
	}
	int rcode = ioctl(fd, EVIOCGRAB, 1);
	printf("GRAB %s\n", (rcode == 0) ? "SUCCESS" : "FAILURE");
	while (1)
	{
		{
			bool mode_default = !strcmp(mode, "default");
			bool mode_led = !strncmp(mode, "led-", 4);
			bool mode_led_set = mode_led && !strcmp(mode+strlen(mode)-4, "-SET");
			setKeyboardLed("numlock", (!mode_default && !mode_led) || mode_led_set);
			setKeyboardLed("capslock", mode_led);
		}
		n = read(fd, &ev, sizeof ev);
		if (n == (ssize_t)-1)
		{
			if (errno == EINTR)
				continue;
			else
				break;
		}
		else if (n != sizeof ev)
		{
			errno = EIO;
			break;
		}
		if (ev.type == EV_KEY && ev.value >= 0 && ev.value < 2)
		{
			Key::MOD thisMod = Key::NOMOD;
			if(ev.code==29 || ev.code==97) thisMod=Key::CTRL;
			if(ev.code==42 || ev.code==54) thisMod=Key::SHIFT;
			if(ev.code==125) thisMod=Key::SUPER;
			if(ev.code==56) thisMod=Key::ALT;
			printf("%s|%d: %s 0x%04x (%d)\n",mode, mod,"RELEASED\0PRESSED\0\0REPATED"+(ev.value*9), (int)ev.code, (int)ev.code);
			if(thisMod != Key::NOMOD)
			{
				if(ev.value) mod = Key::MOD(int(mod) | int(thisMod));
				else mod = Key::MOD(int(mod) & ~int(thisMod));
			}
			else
			if(0<= ev.code && ev.code < convertSize)
			{
				Key k = convert[ev.code];
				if(ev.value != 0)
				{
					printf("run %d %d\n", k.row, k.col);
					int t = time(NULL);
					if(inGrup[k.row][k.col])
					{
						Grup *g = inGrup[k.row][k.col];
						int activKeys = 0;
						for(pair<int,int> it: g->keys)
							if(activedAt[it.first][it.second] > t-5)
								activKeys++;
						if(activKeys==0)
						{
							printf("GRUP run %d %d\n", g->root_row, g->root_col);
							keyModes[{mode,mod}].map[g->root_row][g->root_col][0]();
						}
					}
					else
						if(keyModes.count({mode, mod}))
							keyModes[{mode,mod}].map[k.row][k.col][0]();
					activedAt[k.row][k.col] = t;
				}
				else
					activedAt[k.row][k.col] = 0;
			}
		}


		fflush(stdout);
    }
    fflush(stdout);
    fprintf(stderr, "KEYINPUT ERR: %s.\n", strerror(errno));
}

// ***************** MAIN *************************************
void usageExit(char * name)
{
#define PREPROC_XSTR(s) PREPROC_STR(s)
#define PREPROC_STR(s) #s
	printf(
			"%s: Configuration file is located at %s/../led/deviceTree.h (applying changes require recompilation)\n"
			,name,PREPROC_XSTR(BUILDPATH));
	exit(0);
}

int main(int argc, char ** argv)
{
	ledImp.loadTree(Tree::root);
	initConvert();
	for(int i=1;i<argc;i++)
	{
		if(!strcmp(argv[i],"-h") || !strcmp(argv[i],"--help")) usageExit(argv[0]);
	}
	if(argc<=1)  usageExit(argv[0]);
	if(argc>2) led_prefix = argv[2];
	keyEvent(argv[1]);
}
