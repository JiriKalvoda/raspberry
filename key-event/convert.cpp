const int convertSize=256*256*2;
Tree::Key convert[convertSize];

void initConvert()
{
vector<vector<int>> a =
{
{
1,	//ESC	
59,	//F1 	
60,	//F2 	
61,	//F3 	
62,	//F4 	
63,	//F5 	
64,	//F6 	
65,	//F7 	
66,	//F8 	
67,	//F9 	
68,	//F10	
87,	//F11	
88,	//F12	
},
{
41,	//`  	
2,	//1  	
3,	//2  	
4,	//3  	
5,	//4  	
6,	//5  	
7,	//6  	
8,	//7  	
9,	//8  	
10,	//9  	
11,	//0  	
12,	//-  	
13,	//=  	
14,	//Bac	
},
{
15,	//Tab	
16,	//Q  	
17,	//W  	
18,	//E  	
19,	//R  	
20,	//T  	
21,	//Y  	
22,	//U  	
23,	//I  	
24,	//O  	
25,	//P  	
26,	//[  	
27,	//]  	
28,	//Ent	
},
{
58,	//Cap	
30,	//A  	
31,	//S  	
32,	//D  	
33,	//F  	
34,	//G  	
35,	//H  	
36,	//J  	
37,	//K  	
38,	//L  	
39,	//;  	
40,	//'  	
43,	//backslash
},
{
42,	//Shi	
256,	//NUL	
44,	//Z  	
45,	//X  	
46,	//C  	
47,	//V  	
48,	//B  	
49,	//N  	
50,	//M  	
51,	//,  	
52,	//.  	
53,	///  	
54,	//Shi	
},
{
29,	//Ctr	
125,	//Win	
56,	//Alt	
57,	//Spa	
100,	//Alt	
126,	//Win	
127,	//Men			
97,	//Ctr	
},
{
	99,
	70,
	119,
},
{
110, //Insert	(E0F070)
102, //Home	(E0F06C)
104, //Page Up	(E0F07D)
},
{
111, //Delete	(E0F071)
107, //End		(E0F069)
109, //Page Dn	(E0F07A)
},
{
103, //Up Arr	(E0F075)
},
{
105, //Left Ar	(E0F06B)
108, //Down Ar	(E0F072)
106, //Right A	(E0F074)
},
{
69, //Num	    (F077)
98, ///       (E0F04A)
55, //*       (F07C)
74, //-       (F07B)
},
{
71, //7       (F06C)
72, //8       (F075)
73, //9       (F07D)
78, //+       (F079)
},
{
75, //4       (F06B)
76, //5       (F073)
77, //6       (F074)
},
{
79, //1       (F069)
80, //2       (F072)
81, //3       (F07A)
},
{
82, //0       (F070)
83, //.       (F071)
96, //Enter	(E0F05A)             
}
};
	for(int i=0;i<convertSize;i++) convert[i]=Tree::NULLKEY;
	for(int i=0;i<(int)a.size();i++)
		for(int j=0;j<(int)a[i].size();j++)
			convert[a[i][j]] = Tree::Key{i,j};
}
