#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <cstdint>
#include <cstring>
#include <vector>
#include<stdio.h>
#include <unistd.h>
#include <stdexcept>

#include <MQTTClient.h>

#include "../led/deviceTree.h"

Tree::Mosquitto * mosquitto;


using ll = long long;
using ld = long double;
using std::vector;
using std::max;
using std::min;

XImage* img;
int width,height;
int rwidth,rheight;
Display* display;
Window root;
vector<vector<ll>> prefixSum[3];
vector<vector<ll>> prefixCount[3];
int resize = 10;

MQTTClient connect()
{
	int rc;
	for(int i=0;i<2;i++)
	{
		printf("CONNECT\n");
		MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;

		std::string addres = mosquitto->getAddres();
		std::string username = mosquitto->getUsername("led-include");
		std::string passwd = mosquitto->getPasswd("led-include");

		MQTTClient mqtt = 0;
		MQTTClient_create(&mqtt,addres.c_str(), "monitor",
				MQTTCLIENT_PERSISTENCE_NONE, NULL);

		conn_opts.keepAliveInterval = 10000;
		conn_opts.cleansession = 1;
		conn_opts.username = username.c_str();
		conn_opts.password = passwd.c_str();

		if ((rc = MQTTClient_connect(mqtt, &conn_opts)) == MQTTCLIENT_SUCCESS)
		return mqtt;
		MQTTClient_destroy(&mqtt);
	}
	printf("Failed to connect MQTT, return code %d\n", rc);
	throw std::runtime_error("MQTT Connect error");
}
MQTTClient reconnect(MQTTClient client)
{
	MQTTClient_disconnect(client, 10000);
	MQTTClient_destroy(&client);
	return connect();
}

void init()
{

    display = XOpenDisplay(nullptr);
    root = DefaultRootWindow(display);

    XWindowAttributes attributes = {0};
    XGetWindowAttributes(display, root, &attributes);
    width = attributes.width;
    height = attributes.height;
    rwidth = width/resize-1;
    rheight = height/resize-1;
	for(int c=0;c<3;c++)
	    prefixCount[c]=prefixSum[c]=vector<vector<ll> >(rwidth+1,vector<ll>(rheight+1,0));
}

void get()
{

	if(img) {XDestroyImage(img);}
     img = XGetImage(display, root, 0, 0 , width, height, AllPlanes, ZPixmap);
    //ll pix = XGetPixel(img,1,1);
    //printf("%lld %lld %lld %lld\n",pix%256,pix/256%256,pix/256/256%256,pix/256/256/256);
     for(int c=0;c<3;c++)
    {
		for(int i=0;i<rwidth;i++)
		for(int j=0;j<rheight;j++)
		{
		prefixSum[c][i+1][j+1] = prefixSum[c][i][j+1] + prefixSum[c][i+1][j] - prefixSum[c][i][j]
			+ (XGetPixel(img,i*resize+resize/2,j*resize+resize/2)>>(16-8*c)) % 256;
		prefixCount[c][i+1][j+1] = prefixCount[c][i][j+1] + prefixCount[c][i+1][j] - prefixCount[c][i][j]
			+ 1;
		}

    }
}
char inarr[1234];

ll prefixGet(vector<vector<ll>>& in,int a,int b,int c,int d)
{
	return in[b][d]+in[a][c]-in[a][d]-in[b][c];
}

char out[12345];
const int lednum=15;

MQTTClient mqtt;
const int TIMEOUT = 2000;
void mqttMessage(const char * data)
{
	int rc;
	for(int i=0;;i++)
	{
		for(int i=0;i<2;i++)
		{
			MQTTClient_message pubmsg = MQTTClient_message_initializer;
			MQTTClient_deliveryToken token;
			pubmsg.payload = (void *)data;
			pubmsg.payloadlen = strlen(data);
			pubmsg.qos = 0;
			pubmsg.retained = 1;
			MQTTClient_publishMessage(mqtt,"monitor", &pubmsg, &token);
			rc = MQTTClient_waitForCompletion(mqtt, token, TIMEOUT);
			if(rc == MQTTCLIENT_SUCCESS) return;
		}
		if(i==1)
			throw std::runtime_error("MQTT Message faild");
		mqtt=reconnect(mqtt);
	}
}

int main(int argc, char ** argv)
{
	const char * mosquitto_name = argc>=2?argv[1]:"jug9";
	mosquitto = Tree::mosquitto_map[mosquitto_name];
	if(!mosquitto)
	{
		printf("Mosquitto %s not found\n", mosquitto_name);
		exit(2);
	}
	int sleepTime=1000;
	if(argc > 1) sleepTime=atoi(argv[1]);
	mqtt=connect();
	if(argc>1 && !strcmp(argv[1],"off"))
	{
		mqttMessage("");
		return 0;
	}
	int numOfSkip=0;
	init();
	vector<int>act;
	vector<int> last;
	for(int i=0;;i++)
	{

		get();

		act=vector<int>();

		for(int i=0;i<lednum;i++)
		{
			int A = 2560/lednum*i;
			int B = 2560/lednum*(i+1);
			int C = 0;
			int D = 1000;
			A/=resize;
			B/=resize;
			C/=resize;
			D/=resize;
			for(int c=0;c<3;c++)
			{
				ll sum = prefixGet(prefixSum[c],A,B,C,D);
				ll count =  prefixGet(prefixCount[c],A,B,C,D);
				//printf("%lld %lld  -> %lld\n",sum,count,out);
				if(count)
				{
					ll out = min(255ll,max(1ll,sum/count));
					out=min(255ll,max(0ll,out));
					act.push_back(out);
				}
				else
					act.push_back(255);
			}
		}
		char * outpt=out;
		ll diff=0;
		if(last.size() != act.size())
			diff=1000000;
		else
			for(int i=0;i<(int)act.size();i++)
			{
				diff += abs(last[i]-act[i]);
			}
		if(diff>10*lednum || numOfSkip>30)
		{
			for(auto it:act)
				outpt += sprintf(outpt,"%d ",it);
			printf("%2d -> %s\n",numOfSkip,out);
			try{mqttMessage(out);}
			catch (const std::exception& e)
			{
				printf("MQTT Sent error\n");
			}
			last=act;
			numOfSkip=0;
		}
		else numOfSkip++;
		usleep(sleepTime*1000);
	}


	return 0;
}
