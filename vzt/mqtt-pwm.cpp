#include <wiringPi.h>
#include <stdlib.h>
#include <stdbool.h>
#include <random>
#include <mutex>
#include <string.h>
#include <MQTTClient.h>
#include "../led/deviceTree.h"

#define fo(a,b) for(int a=0;a<(b);++a)
#define DMQTT if (0)

using Tree::FNAMELEN;

Tree::Mosquitto *mosquitto;
MQTTClient mqtt;

volatile float val;
const char * mqttPrefix;
int pin;
float frequency;
int period;



int msgarrvd_cb(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
	int i;
	DMQTT printf("Message arrived\n");
	DMQTT printf("     topic: %s\n", topicName);
	DMQTT printf("   message: ");
	char * payloadptr = (char *)message->payload;
	DMQTT for(i=0; i<message->payloadlen; i++)
	{
		putchar(payloadptr[i]);
	}
	DMQTT putchar('\n');
	char * in = new char [message->payloadlen+1];
	for(int i=0;i<message->payloadlen;i++)
		in[i]=payloadptr[i];
	in[message->payloadlen]=0;

	if(!strcmp(topicName,mqttPrefix))
	{
		val = atof(in);
	}

	delete in;
	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}

void connlost_cb(void *context, char *cause)
{
    printf("\nMQTT connection lost\n");
    printf("     cause: %s\n", cause);
    val = 0;
    delayMicroseconds(2'000'000);
	exit(1);
}

void initMqtt()
{
	for(int i=0;;i++)
	{
		MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
		int rc;
		std::string addres = mosquitto->getAddres();
		std::string username = mosquitto->getUsername("led-include");
		std::string passwd = mosquitto->getPasswd("led-include");
		static char mqttConName[1234] = "";
		std::random_device rd;
		if(!mqttConName[0]) sprintf(mqttConName,"motor-%d-%d",int(rd()),int(rd()));
		MQTTClient_create(&mqtt, addres.c_str(), mqttConName,
				MQTTCLIENT_PERSISTENCE_NONE, NULL);
		conn_opts.keepAliveInterval = 20;
		conn_opts.cleansession = 1;
		conn_opts.username = username.c_str();
		conn_opts.password = passwd.c_str();
		MQTTClient_setCallbacks(mqtt, NULL, connlost_cb, msgarrvd_cb, NULL);

		if ((rc = MQTTClient_connect(mqtt, &conn_opts)) != MQTTCLIENT_SUCCESS && i==10)
		{
			printf("Failed to connect MQTT, return code %d\n", rc);
			exit(-1);

		}
		if(rc == MQTTCLIENT_SUCCESS) break;
		delay(1000);
	}
	printf("CONECT OK\n");
	fflush(stdout);
}

void subscribeMqtt()
{
	MQTTClient_subscribe(mqtt, mqttPrefix, 1);
}

void usageExit(char *prname)
{
	fprintf(stderr, "Wrong argv\n");
	exit(1);
}


int main (int argc, char ** argv)
{
	wiringPiSetup();

	if(argc<=4) usageExit(argv[0]);
	mosquitto = Tree::mosquitto_map[argv[1]];
	mqttPrefix = argv[2];
	pin = atoi(argv[3]);
	frequency = atof(argv[3]);
	period = 1'000'000 / frequency;
	pinMode(pin, OUTPUT);
	digitalWrite(pin, 0);

	initMqtt();
	subscribeMqtt();


	while(true)
	{
		if(val<=0)
		{
			digitalWrite(pin, 0);
			delayMicroseconds(1'000'000);
		}
		else
		if(val>=1)
		{
			digitalWrite(pin, 1);
			delayMicroseconds(1'000'000);
		}
		else
		{
			digitalWrite(pin, 1);
			delayMicroseconds(val*period);
			digitalWrite(pin, 0);
			delayMicroseconds((1-val)*period);
		}
	}

	return 0 ;
}
