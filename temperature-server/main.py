#!/bin/python3
from influxdb import InfluxDBClient
import random
import time
from pathlib import Path
import time
import datetime

from datetime import datetime
from datetime import timezone

from paho.mqtt import client as mqtt_client
import time
from collections import defaultdict

import subprocess
import os, sys



client_id = f'sensors-mqtt-influx-{random.randint(0, 1000)}'

def connect_mqtt():
    broker = 'jug9'
    secret_file_prefix = f'{Path.home()}/.secret/mqtt/'
    addres = open(f'{secret_file_prefix}{broker}-all.addres',mode='r').read().strip()
    username = open(f'{secret_file_prefix}{broker}-temperature-server.user',mode='r').read().strip()
    passwd = open(f'{secret_file_prefix}{broker}-temperature-server.passwd',mode='r').read().strip()
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, passwd)
    client.on_connect = on_connect
    client.connect(addres)
    return client

def on_connect(mqtt, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    mqtt.subscribe("sensors/#")

def insert(timestamp, device_name, temperature):
    # print(timestamp, timestamp//time_window_size, device_name, temperature)
    time_windows[device_name][timestamp//time_window_size].add(timestamp, temperature)
    # print(time_windows[device_name][timestamp//time_window_size])

def on_message(mqtt, userdata, msg):
    topic = msg.topic.split('/')
    if topic[0]=="sensors" and topic[-1]=="all":
        device_name = '.'.join(topic[1:-1])
        if device_name.startswith("jug9."): device_name = "pokoj" + device_name[5:]
        pl = msg.payload.decode().split('\n')
        timestamp = int(pl[0])
        for line in pl[1:]:
            words = line.split(" ")
            if words[0] == "temperature":
                temperature = float(words[1])
                insert(timestamp, device_name, temperature)




class TimeWindow:
    def __init__(self):
        self.median = None
        self.data = []

    def add_without_calc(self, timestamp, data):
        self.data.append((timestamp, data))

    def calc(self):
        if len(self.data) >= 3:
            self.median = sorted(self.data, key=lambda x:x[1])[len(self.data)//2][1]
        else:
            self.median = None

    def add(self, timestamp, data):
        self.add_without_calc(timestamp, data)
        self.calc()

    def __str__(self):
        return f"{self.median} [{len(self.data)}]"
    __repr__ = __str__

time_window_size = 120

def actual_time_window():
    return int(time.time()) // time_window_size

time_windows = defaultdict(lambda: defaultdict(lambda: TimeWindow()))

def get_influx():
    from influxdb import InfluxDBClient
    dbh = InfluxDBClient(host='10.19.32.1',database='jug9')
    r = dbh.query('SELECT * FROM "sensor" WHERE time >= now() - 2h')
    for i in r:
        for j in i:
            t = int(
                    datetime.strptime(j["time"].split(".")[0].split("Z")[0],
                        "%Y-%m-%dT%H:%M:%S").replace(tzinfo=timezone.utc).timestamp())
            insert(t,j["device-id"], j["temperature"])

SENSOR1STUL = "pokoj1.stul"

def get_target():
    try:
        sp = subprocess.run([os.getenv("HOME")+"/.config/temperature/config",f"{time.time()}"], capture_output=True)
        t = float(sp.stdout)
        return t
        print("target:", target)
    except Exception as e:
        print("LOAD TARGET ERROR", e)
        return 21.5

def log(data):
    l = [f"{i} {j}" for i,j in data.items()]
    mqtt.publish("heating/jug9/1/log", "\n".join([f"{int(time.time())}"]+l))

def calc():
    target = get_target()
    ltw = actual_time_window() - 1
    tw = time_windows[SENSOR1STUL]
    if tw[ltw].median is None:
        print(f"PANIC ERROR NO DATA! ({ltw}) {tw[ltw]}")
        return 0.0
    else:
        def calc_integral(begin, end):
            integral_data = [tw[i].median for i in range(begin, end) if tw[i].median is not None]
            return None if len(integral_data) == 0 else sum(integral_data)/len(integral_data)
        proporcial = tw[ltw].median - target
        dt = 20*60
        integral = calc_integral(ltw-45*60//time_window_size, ltw+1) - target
        a = calc_integral(ltw-2*dt//time_window_size, ltw-dt//time_window_size)
        b = calc_integral(ltw-dt//time_window_size, ltw)
        derivate = 0 if a is None or b is None else (b - a)/dt*60*60
        print(f"p:{proporcial} i:{integral} d:{derivate}")
        suma = 1.2*proporcial + 1.2*integral + (derivate if derivate > 0 else 0)
        print("suma", suma, flush=True)
        r = 0.5-suma/2
        if r < 0: r = 0
        if r > 1: r = 1
        log({
            "proporcial": proporcial,
            "target": target,
            "integral": integral,
            "derivate": derivate,
            "suma": suma,
            "value": r,
            })
        return r
        

def set_motor(val):
    mqtt.publish("motor/heating/1/goto", f"{val}", retain=True)

get_influx()

mqtt = connect_mqtt()
mqtt.on_connect = on_connect
mqtt.on_message = on_message
mqtt.loop_start()
while True:
    #print(time_windows[SENSOR1STUL], actual_time_window())
    set_motor(calc())
    sys.stdout.flush()
    time.sleep(60)


