#!/bin/bash
cd "$(dirname "$0")"

chmod +x main.py
ln -sr main.py ~/bin/temperature-server

../init-service.sh "Temperature server" --user $HOME/bin/temperature-server ""  >  ~/.config/systemd/user/temperature.service

mkdir -p ~/.config/temperature
if [[ ! -f ~/.config/temperature/config ]]
then
	cp config ~/.config/temperature/config
fi
