#include <string>
#include <iostream>
#include <filesystem>
#include <string.h>
#include <random>
#include <map>
#include <functional>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <thread>
#include <chrono>
#include <thread>


#include "../led/led-withRead.h"
#include "../led/colors.h"
#ifdef DEB
#define D if(1)
#else
#define D if(0)
#endif
const int MAPH=16;
const int MAPW=14;

#define fo(a,b) for(int a=0;a<(b);++a)
using ll = long long;
using ld = double;

namespace fs = std::filesystem;

using Frame = int[MAPH][MAPW][3];
using Tree::Key;
using std::function;
using std::map;
using std::pair;
using std::string;
using std::thread;

LEDWithRead led("_");

const char * I3MODEFNAME = "jk/i3/mode";
const char * DISABLERAZERFNAME = "jk/razer/disable";

Key::MOD mod = Key::NOMOD;
std::string i3Mode;
bool disable=0;

void sem_all(sem_t * sem,int ms=-1)
{//wait and null semaphore
    if(ms==-1)
        sem_wait(sem);
    else
    {
        struct timespec ts;
        if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
        {
            perror("clock_gettime");
            exit(EXIT_FAILURE);
        }
        long long x = ms + (long long)ts.tv_nsec/1'000'000;
        ts.tv_sec +=  x/1000;
        ts.tv_nsec = x%1000*1'000'000;
        sem_timedwait(sem, &ts);
    }
    int semval;
    sem_getvalue(sem,&semval);
    for(;semval>0;semval--) sem_wait(sem);
}

sem_t runSem;

// ***************** RAZER *************************************
const char * path = "/sys/bus/hid/drivers/razerkbd/";
void razer(const char * name, const char * data, int len)
{
    for (const auto & entry : fs::directory_iterator(path))
    {
	    char fname[1234];
	    sprintf(fname,"%s/%s",entry.path().c_str(),name);
	    FILE * f = fopen(fname,"w");
	    if(f)
	    {
		    //puts(fname);
		    fwrite(data, 1, len, f);
		    fclose(f);
	    }
	    
    }

}


void razerCustom(Frame data)
{
	razer("matrix_effect_custom","1",1);
	char out[10*22*3+3+10];
	int outlen=0;
	for(int i=0;i<6;i++)
	{
		out[outlen++]=i;
		out[outlen++]=1;
		out[outlen++]=17;
		for(int j=0,p=0;j<=16;j++,p++)
		{
			if(j>=14 && i!=3)
			{
				for(int k=0;k<3;k++)
					out[outlen++]=data[i+6-(i>3)][i==4?0:j-14][k];
				continue;
			}
			if(i==2 && j==13)
				for(int k=0;k<3;k++)
					out[outlen++]=data[3][12][k];
			else
			if(i==3 && j==13)
				for(int k=0;k<3;k++)
					out[outlen++]=data[2][13][k];
			else
				for(int k=0;k<3;k++)
					out[outlen++]=data[i][p][k];
			if(i==0 && j== 1)  p--;
			if(i==5 && j==3 )  p--;
			if(i==5 && j==4 )  p--;
			if(i==5 && j==5 )  p--;
			if(i==5 && j==6 )  p--;
			if(i==5 && j==7 )  p--;
		}
	}
	razer("matrix_custom_frame",out,outlen);
}

// ***************** keymap *************************************
using KeyModeKey =  function<void (int*)>;
struct KeyMode
{
	KeyModeKey * map[MAPH][MAPW];
};
void set(int out[3],int a, int b,int c)
{
	out[0]=a;
	out[1]=b;
	out[2]=c;
}
namespace k
{
	namespace led
	{
		KeyModeKey * light(const char * name, int par, int maxVal){return new KeyModeKey([name,par,maxVal](int * c)
			{
				if(maxVal==0) return;
				int x = 20+235*pow(par/(double)maxVal,3);
				set(c,x,x,x);
			});}
		KeyModeKey * lightBoost(const char * name, int par, int maxVal, ld maxBoost){return new KeyModeKey([name,par,maxVal](int * c)
			{
				int x = 255;
				set(c,x,x,x);
			});}
		KeyModeKey * color(const char * name, int par, int maxVal){return new KeyModeKey([name,par,maxVal](int * c)
			{
				if(maxVal==0) return set(c,255,255,255);
				int x = 255*pow(1-par/(double)maxVal,3);
				set(c,255,x,x);
			});}
		KeyModeKey * swap(const char * name,const char * a,const char *b){return new KeyModeKey([name,a,b](int * c)
			{
				set(c,0,255,0);
			});}
		KeyModeKey * stop(const char * name){return new KeyModeKey([name](int * c)
			{
				set(c,0,255,0);
			});}
		KeyModeKey * lightSpeed(const char * name, int par){return new KeyModeKey([name,par](int * c)
			{
				set(c,0,255,0);
			});}
		KeyModeKey * colorSpeed(const char * name, int par){return new KeyModeKey([name,par](int * c)
			{
				set(c,0,255,0);
			});}
		KeyModeKey * lightBig(const char * name, int par){return new KeyModeKey([name,par](int * c)

			{
				int x = 255*par/12;
				set(c,x,x,x);
			});}
		KeyModeKey * colorBig(const char * name, int par){return new KeyModeKey([name,par](int * c)
			{
				set(c,colors(par).rgb[0],colors(par).rgb[1],colors(par).rgb[2]);
			});}
	}
	KeyModeKey * mqtt_pub(const char * a, const char *b, const char *c, bool r){return new KeyModeKey([](int * c)
		{
			set(c,255,0,255);
		});}
	KeyModeKey * cmd(const char * in){return new KeyModeKey([in](int * c)
		{
			set(c,255,0,255);
		});}
	KeyModeKey * goMode(const char * in){return new KeyModeKey([in](int * c)
		{
			set(c,0,0,255);
		});}
	KeyModeKey * rgb(ld r,ld g,ld b){return new KeyModeKey([r,g,b](int * c)
		{
			set(c,r*255,g*255,b*255);
		});}
	KeyModeKey * RGB(int r,int g,int b){return new KeyModeKey([r,g,b](int * c)
		{
			set(c,r,g,b);
		});}
	KeyModeKey * undef = new KeyModeKey([](int * c){c[0]=c[1]=c[2]=255;});
	KeyModeKey * null = new KeyModeKey([](int * c)
	{
		c[0]=c[1]=c[2]=0;
		if(i3Mode == "default" || !strncmp(i3Mode.c_str(),"LOCK-",4))
		{
			//printf("mod = %d\n",mod);
			if(mod == Key::NOMOD) c[0]=c[1]=c[2]=200;
			else if(mod == Key::SHIFT) c[0]=c[1]=c[2]=255;
			else if(mod == Key::CTRL) set(c, 0, 255, 0);
			else if(mod == (Key::CTRL | Key::SHIFT)) set(c, 100, 255, 100);
			else if(mod == Key::ALT) set(c, 0, 0, 255);
			else if(mod == (Key::ALT|Key::SHIFT)) set(c, 100, 100, 255);
			else if(mod == (Key::ALT|Key::CTRL)) set(c, 0, 255, 255);
			else if(mod == (Key::ALT|Key::CTRL|Key::SHIFT)) set(c, 100, 255, 255);
			else
			{
				if(i3Mode == "default") c[0]=c[1]=c[2]=0;
				else set(c,255,100,100);
			}
		}
	});
}
#include "keymap.cpp"


// ***************** MQTT *************************************
void delivered(void *,MQTTClient_deliveryToken dt)
{
    DMQTT printf("Message with token value %d delivery confirmed\n", dt);
}
int msgarrvd(void *,char *topicName, int topicLen, MQTTClient_message *message)
{
	int i;
	DMQTT printf("Message arrived\n");
	DMQTT printf("     topic: %s\n", topicName);
	DMQTT printf("   message: ");
	char * payloadptr = (char *)message->payload;
	DMQTT for(i=0; i<message->payloadlen; i++)
	{
		putchar(payloadptr[i]);
	}
	DMQTT putchar('\n');
	if(!strcmp(I3MODEFNAME,topicName)) i3Mode = payloadptr;
	if(!strcmp(DISABLERAZERFNAME,topicName))
	{
		if(!strcmp(payloadptr,"toggle"))
			disable^=1;
		else
			disable=!strcmp(payloadptr,"1");
	}
	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	sem_post(&runSem);
	return 1;
}
void connlost(void *,char *cause)
{
    printf("\nMQTT connection lost\n");
    printf("     cause: %s\n", cause);
}
MQTTClient connect(Tree::Mosquitto * mosquitto,bool cleansession=1)
{
	static char mqttConName[1234];
	int rc;
	for(int i=0;i<2;i++)
	{
		MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;

		std::string addres = mosquitto->getAddres();
		std::string username = mosquitto->getUsername("led-include");
		std::string passwd = mosquitto->getPasswd("led-include");

		MQTTClient mqtt = 0;
		 std::random_device rd;
		if(!mqttConName[0]) sprintf(mqttConName,"razer-%d-%d",int(rd()),int(rd()));
		MQTTClient_create(&mqtt,addres.c_str(), mqttConName,
				MQTTCLIENT_PERSISTENCE_NONE, NULL);

		MQTTClient_setCallbacks(mqtt, 0, connlost, msgarrvd, delivered);

		conn_opts.keepAliveInterval = 60;
		conn_opts.cleansession = cleansession;
		conn_opts.username = username.c_str();
		conn_opts.password = passwd.c_str();

		if ((rc = MQTTClient_connect(mqtt, &conn_opts)) == MQTTCLIENT_SUCCESS)
		{
			MQTTClient_subscribe(mqtt,I3MODEFNAME, 1);
			MQTTClient_subscribe(mqtt,DISABLERAZERFNAME, 1);
			return mqtt;
		}
		MQTTClient_destroy(&mqtt);
	}
	printf("Failed to connect MQTT, return code %d\n", rc);
	throw std::runtime_error("MQTT Connect error");
}

// ***************** KEY EVENT *************************************
void keyEvent(const char * dev)
{
    struct input_event ev;
    ssize_t n;
    int fd;

    for(int i=0;(fd = open(dev, O_RDONLY))<0;i++)
	{
		if (i==30) {
			fprintf(stderr, "KEYINPUT ERR: Cannot open %s: %s.\n", dev, strerror(errno));
			//sleep(1);
			return;
		}
		using namespace std::chrono_literals;
		std::this_thread::sleep_for(1s);
	}
	while (1)
	{
		n = read(fd, &ev, sizeof ev);
		if (n == (ssize_t)-1)
		{
			if (errno == EINTR)
				continue;
			else
				break;
		}
		else if (n != sizeof ev)
		{
			errno = EIO;
			break;
		}
		if (ev.type == EV_KEY && ev.value >= 0 && ev.value < 2)
		{
			Key::MOD thisMod = Key::NOMOD;
			if(ev.code==29 || ev.code==97) thisMod=Key::CTRL;
			if(ev.code==42 || ev.code==54) thisMod=Key::SHIFT;
			if(ev.code==125) thisMod=Key::SUPER;
			if(ev.code==56) thisMod=Key::ALT;
			//printf("%s 0x%04x (%d)\n","RELEASED\0PRESSED \0REPATED"+(ev.value*9), (int)ev.code, (int)ev.code);
			if(thisMod != Key::NOMOD)
			{
				if(ev.value) mod = Key::MOD(int(mod) | int(thisMod));
				else mod = Key::MOD(int(mod) & ~int(thisMod));
				sem_post(&runSem);
			}
		}

		fflush(stdout);
    }
    fflush(stdout);
    fprintf(stderr, "KEYINPUT ERR: %s.\n", strerror(errno));
}

// ***************** MAIN *************************************
void usageExit(char * name)
{
#define PREPROC_XSTR(s) PREPROC_STR(s)
#define PREPROC_STR(s) #s
	printf(
			"This is a program for showing keyboard layout mainly for led binds on a Razer keyboard.\n"
			"Start me as daemon by executing:\n"
			"\t%s [keyboard input]\n"
			"\n"
			"\tkeyboard input\t\tLinux file for reading key mod (for examle /dev/input/event5)\n"
			"\n"
			"Configuration file is located at %s/../led/deviceTree.h (applying changes require recompilation)\n"
			,name,PREPROC_XSTR(BUILDPATH));
	exit(0);
}

int main(int argc, char ** argv)
{
	for(int i=1;i<argc;i++)
	{
		if(!strcmp(argv[i],"-h") || !strcmp(argv[i],"--help")) usageExit(argv[0]);
	}
	//thread thKeyEvent(keyEvent, "/dev/input/by-path/platform-fd500000.pcie-pci-0000:01:00.0-usb-0:1.4:1.0-event-kbd");
	if(argc>1) 
		thread(keyEvent,argv[1]).detach();
	MQTTClient i3Mqtt = connect(Tree::mosquitto_map["rpi0"]);
	(void)i3Mqtt;
	led.loadTree(Tree::root);
	Frame fr;
	while(true)
	{
		sem_all(&runSem);
		printf("|%s| %d %d\n",i3Mode.c_str(),int(mod),int(disable));
		fflush(stdout);
		if(disable)
		{
			for(int i=0;i<MAPH;i++)
				for(int j=0;j<MAPW;j++)
				{
					fr[i][j][0]=0;
					fr[i][j][1]=0;
					fr[i][j][2]=0;
				}
		}
		else if(!keyModes.count({i3Mode,(int)mod}))
		{
			for(int i=0;i<MAPH;i++)
				for(int j=0;j<MAPW;j++)
					k::null[0](fr[i][j]);
		}
		else
		{
			KeyMode act = keyModes[{i3Mode,(int)mod}];
			for(int i=0;i<MAPH;i++)
				for(int j=0;j<MAPW;j++)
				{
					if((i==4 && (!j||j==12)) || (i==5 && j!=3))
						set(fr[i][j],255,255,255);
					else
						act.map[i][j][0](fr[i][j]);
				}
		}
		razerCustom(fr);
	}
}
