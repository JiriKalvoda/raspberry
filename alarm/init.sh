#!/bin/bash
cd "$(dirname "$0")"

chmod +x vialarm alarm-daemon
ln -sr vialarm ~/bin/
ln -sr alarm-daemon ~/bin/
ln -sr alarm-stop ~/bin/

sudo cp alarm.service /etc/systemd/system
sudo systemctl daemon-reload

../init-service.sh "Alarm" --user $HOME/bin/alarm-daemon ""  >  ~/.config/systemd/user/alarm.service


mkdir ~/.config/alarm -p
mkdir ~/.config/alarm/play -p
if [[ ! -f ~/.config/alarm/schedule ]]
then
	cp schedule ~/.config/alarm/schedule
fi
ln -rs schedule-include ~/.config/alarm/
