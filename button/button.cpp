#include <wiringPi.h>
#include<string> // system()

using namespace std;
#ifdef DEB
#define D if(1)
#else
#define D if(0)
#endif

#define fo(a,b) for(int a=0;a<(b);++a)
using ll = long long;

int main(int argc, char ** argv)
{
	if(argc != 3) exit(3);
	int pin=atoi(argv[1]);

	wiringPiSetup();			// Setup the library
	pinMode(pin, INPUT);		// Configure GPIO0 as an output
	pullUpDnControl(pin,PUD_UP);

	const int valOn = 0;
	while(1)
	{
		for(int i=0;i<10;i++, delay(10)) if(digitalRead(pin) != valOn) goto a;
		system(argv[2]);
		while(digitalRead(pin) == valOn) delay(10);
a:
		delay(100); 	// Delay 500ms

	}
	return 0;
}

