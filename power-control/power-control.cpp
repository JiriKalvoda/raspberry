//#include <vector>
#define USING_POST_HOOK
#include <string.h>
#include "../lib/gpio.h"
#include <math.h>
#include <unistd.h>
#include <mutex>

#include <MQTTClient.h>
#include <semaphore.h>


#include "../led/deviceTree.h"
#include "../led/postHooks.h"

const int TIMEOUT = 300; // s
const int AFTERTIME = 1; // s
const int PUB_PERIOD = 300; // s
const int START_TIME = 500'000; // us


using Tree::FNAMELEN;
using Tree::FLEN;

using Tree::MQTTPREFIX;

using ld=double;
using ll=long long;

using Gpio::p;
using Gpio::pinModeOutput;
using Gpio::digitalWrite;


const Tree::PowerControl * maschine;

MQTTClient mqtt;
std::unordered_map<std::string,char *> mqttMap;
std::mutex mqttMapMutex;
bool mqttConnLost=0;
sem_t runSem;

#define DMQTT if(0)

int pin;

void sem_all(sem_t * sem,int ms=-1)
{//wait and null semaphore
	if(ms==-1)
		sem_wait(sem);
	else
	{
		struct timespec ts;
		if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
		{
			perror("clock_gettime");
			exit(EXIT_FAILURE);
		}
		long long x = ms + (long long)ts.tv_nsec/1'000'000;
		ts.tv_sec +=  x/1000;
		ts.tv_nsec = x%1000*1'000'000;
		sem_timedwait(sem, &ts);
	}
	int semval;
	sem_getvalue(sem,&semval);
	for(;semval>0;semval--) sem_wait(sem);
}

void init()
{
	Gpio::init();
	pinModeOutput(p(pin), 1);
}


void delivered(void *context, MQTTClient_deliveryToken dt)
{
}
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    int i;
    DMQTT printf("Message arrived\n");
    DMQTT printf("     topic: %s\n", topicName);
    DMQTT printf("   message: ");
    char * payloadptr = (char *)message->payload;
    DMQTT for(i=0; i<message->payloadlen; i++)
    {
        putchar(payloadptr[i]);
    }
    DMQTT putchar('\n');
    char * in = new char [message->payloadlen+1];
    for(int i=0;i<message->payloadlen;i++)
	    in[i]=payloadptr[i];
    in[message->payloadlen]=0;
    char * old;
    std::string topic = std::string(topicName);
    {
	std::lock_guard<std::mutex> guard(mqttMapMutex);
	    old = mqttMap[topic];
	    mqttMap[topic]=in;
    }
    if(old) delete [] old;
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    sem_post(&runSem);
    return 1;
}
void connlost(void *context, char *cause)
{
    printf("\nMQTT connection lost\n");
    printf("     cause: %s\n", cause);
	mqttConnLost=1;
    sem_post(&runSem);
	exit(1);
}

void initMqtt()
{
	for(int i=0;;i++)
	{
		MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
		int rc;

		auto mosquitto = maschine->mosquitto;

		std::string addres = mosquitto->getAddres();
		std::string username = mosquitto->getUsername("led-daemon");
		std::string passwd = mosquitto->getPasswd("led-daemon");
		MQTTClient_create(&mqtt, addres.c_str(), "power-control",
				MQTTCLIENT_PERSISTENCE_NONE, NULL);
		conn_opts.keepAliveInterval = 20;
		conn_opts.cleansession = 1;
		conn_opts.username = username.c_str();
		conn_opts.password = passwd.c_str();
		//conn_opts.password = "fnnvjfdnvjfnjkvnsiuuredvuj";
		MQTTClient_setCallbacks(mqtt, NULL, connlost, msgarrvd, delivered);

		if ((rc = MQTTClient_connect(mqtt, &conn_opts)) != MQTTCLIENT_SUCCESS && i==10)
		{
			printf("Failed to connect MQTT, return code %d\n", rc);
			exit(-1);

		}
		if(rc == MQTTCLIENT_SUCCESS) break;
		usleep(1000 * 1000);
	}
	printf("CONECT OK\n");
	fflush(stdout);
}


void usageExit(const char * name)
{
#define PREPROC_XSTR(s) PREPROC_STR(s)
#define PREPROC_STR(s) #s
	printf(
			"Start me as daemon by executing:\n"
			"\n"
			"	%s <device_name>\n"
			"\n"
			"Configuration file is located at %s/../led/deviceTree.h (apply charges need recompilation)\n"
			,name,PREPROC_XSTR(BUILDPATH));
	exit(0);
#undef PREPROC_XSTR
#undef PREPROC_STR
}

char prefix[Tree::FNAMELEN];

char last_pub_state[Tree::FLEN];
ll last_pub_time = 0;
void pub_state(const char * state)
{
	if(!strcmp(last_pub_state, state) && last_pub_time + PUB_PERIOD > time(0)) return;
	strcpy(last_pub_state, state);
	last_pub_time = time(0);
	char fname[Tree::FNAMELEN];
	sprintf(fname, "%s/state", prefix);
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;
	pubmsg.payload = (void *)state;
	pubmsg.payloadlen = strlen(state);
	pubmsg.qos = 0;
	pubmsg.retained = 1;
	MQTTClient_publishMessage(mqtt, fname, &pubmsg, &token);
	printf("STATE: %s\n", state);
	fflush(stdout);
}

int main(int argc,char ** argv)
{
	if(argc!=2) usageExit(argv[0]);
	maschine = Tree::power_control_map[argv[1]];
	printf("%s %s\n", maschine->power_control_name, maschine->mosquitto->name);
	sprintf(prefix,"power-control/%s",maschine->power_control_name);
	pin = maschine->pin;
	sem_init(&runSem, 0, 1);
	for(int i=1;i<argc;i++)
	{
		if(!strcmp(argv[i],"-h") || !strcmp(argv[i],"--help")) usageExit(argv[0]);
	}
	initMqtt();
	{
		char tmp[Tree::FNAMELEN];
		sprintf(tmp, "%s/control/#", prefix);
		MQTTClient_subscribe(mqtt, tmp, 1);

	}
	init();
	ll last_on_time = 0;
	ll last_off_time = 0;
	bool last_on_or_idle = 0;
	for(int i=0;;i++)
	{
		bool on = 0;
		{
			std::lock_guard<std::mutex> guard(mqttMapMutex);
			for(const auto &[name, value] : mqttMap)
			{
				//printf("%s %s\n", name.c_str(), value);
				if(!strncmp(name.c_str(), prefix, strlen(prefix)))
				{
					if(atoi(value) > time(NULL)-TIMEOUT) on = 1;
				}
			}

		}
		if(on) last_on_time = time(NULL);
		else last_off_time = time(NULL);
		bool on_or_idle = last_on_time > time(NULL)-AFTERTIME;
		if(on)
		{
			if(!last_on_or_idle)
			{
				digitalWrite(p(pin), !(1));
				pub_state("starting");
				usleep(START_TIME);
				
			}
			pub_state("on");
		}
		else
		{
			if(on_or_idle)
				pub_state("idle");
			else
				pub_state("off");
		}

		digitalWrite(p(pin), !(on_or_idle));
		sem_all(&runSem, 1'010);
		if(on) last_on_time = time(NULL);
		else last_off_time = time(NULL);
		last_on_or_idle = on_or_idle;
	}
	return 0;
}
