#include <string.h>
#include <unistd.h>
#include <signal.h>

#include "../led/led-include.h"

LED led;

void usageExit(char * name)
{
	exit(1);
}


volatile bool in_send = 0;
volatile bool end_now = 0;

Tree::Mosquitto * mosquitto;
bool retained = 1;
const char * topic;
char data[1000];

void end()
{
	sprintf(data, "0");
	led.mqttMessage(mosquitto,data,topic,retained);
	exit(0);
}

void handler(int)
{
	if(in_send) end_now=1;
	else end();
}


int main(int argc,char ** argv)
{
	signal(34, handler);
	int argit = 1;

	if(argit >= argc) usageExit(argv[0]);
	char * name = argv[argit++];
	char exe[1234];
	sprintf(exe,"osdc --duration=10000 --min-duration=5000 'už je' '%s'\\!\\!\\!", name);
	system(exe);

	if(argit >= argc) usageExit(argv[0]);
	if(!Tree::mosquitto_map.count(argv[argit])) usageExit(argv[0]);
	mosquitto = Tree::mosquitto_map[argv[argit++]];

	if(argit >= argc) usageExit(argv[0]);
	topic = argv[argit++];

	while(true)
	{
		sprintf(data, "%ld", time(NULL));
		in_send = 1;
		led.mqttMessage(mosquitto,data,topic,retained);
		in_send = 0;
		if(end_now) end();
		sleep(1);
	}
	return 0;
}

