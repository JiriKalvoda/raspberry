#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include<fstream>
using namespace std;
#define MAXTIMINGS	85
using ld = long double;
using ll = long long;
using ui = unsigned int;

int a=0,b=0;

const int CPIN=5;
const int DPIN=4;

ui lastTime=0;

void interuptNeg(void)
{
	ui actTime = micros();
	ui timeChange = actTime-lastTime;
	if(timeChange < 4) return;
	lastTime = actTime;
	printf("<%d>",timeChange);
}
void interupt(void)
{
	{
			return;
			ui actTime = micros();
			ui timeChange = actTime-lastTime;
			lastTime = actTime;
			printf(" -> %d\n",int(timeChange));
	}
	int c = digitalRead(CPIN);
	int r = digitalRead(DPIN);
	ui actTime = micros();
	ui timeChange = actTime-lastTime;
	if(!c)
	{
		printf("<%d>",timeChange);
		lastTime = actTime;
	}
	else
	{
		printf("[%d]",timeChange);
		if(timeChange < 4) return;
		lastTime = actTime;
		static int in;
		static int i;
		if(i>=11 || timeChange>100000)
		{
			in=i=0;
			printf("\n");
		}
		printf("%d",r);
		fflush(stdout);
		in |= r<<i++;
		if(i==1 && in==1) in=i=0;
		if(i==11)
		{
			printf("-> %d\n",in);
			if((in&1)!=0) {fprintf(stderr,"Start bit err\n");return;}
			if((in>>10&1)!=1) {fprintf(stderr,"Stop bit err\n");return;}
			in>>=1;
			in&=255;
			int par=0;
			for(int i=0;i<9;i++) par^=in>>i;
			par&=1;
			if(!par) {fprintf(stderr,"Parity bit err\n");return;}
			in&=127;
			printf("-> %d\n",in);
		}
	}
}
void init()
{
	if ( wiringPiSetup() < 0)
		exit( 10 );
	pinMode( CPIN, INPUT );
	pinMode( DPIN, INPUT );
	//if ( wiringPiISR (CPIN, INT_EDGE_BOTH, &interupt) < 0 ) {exit(11);}


}
 
int len=0;
int data=0;
int main(int argc,char ** argv)
{
	init();
	int last=-10;
	for(;;)
	{
		printf("B %d\n",waitForInterrupt(CPIN, 1000) );
		for(int i=0;i<1000;i++)
		{
			int c = digitalRead(CPIN);
			if(c!=last)
			{
				i=0;
				if(len>=11) data=len=0;
				ui actTime = micros();
				ui timeChange = actTime-lastTime;
				if(timeChange>1000 && len)
				{
					printf("[%d] %d\n",len,data);
					data=len=0;
				}
				lastTime = actTime;
				int d = digitalRead(DPIN);
				//printf("%d %d -> %d\n",int(c),int(d),int(timeChange));
				if(c) data+=d<<=len++;
				if(len>=11)
				{
					//printf("-> %d\n",data);
					if((data&1)!=0) {fprintf(stderr,"Start bit err\n");continue;}
					if((data>>10&1)!=1) {fprintf(stderr,"Stop bit err\n");continue;}
					data>>=1;
					data&=(1<<9)-1;
					int par=0;
					for(int i=0;i<9;i++) par^=data>>i;
					par&=1;
					if(!par) {fprintf(stderr,"Parity bit err\n");continue;}
					data&=(1<<8)-1;
					printf("%d\n",data);
				}
			}
			last=c;
		}
		printf("E\n");
	}
	return(0);
}
