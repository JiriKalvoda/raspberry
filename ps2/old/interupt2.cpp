#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include<fstream>
#include <mutex>
using namespace std;
#define MAXTIMINGS	85
using ld = long double;
using ll = long long;
using ui = unsigned int;

int a=0,b=0;

const int CPIN=5;
const int DPIN=4;

const int QLEN=1000;

std::mutex mut;
std::lock_guard<std::mutex> * loc;

struct In
{
	ui micros;
	int d,c;
};
In q[QLEN];
int qbegin,qend;

volatile bool isSendToKeyboard=0;
ui lastInteruptMicros=0;
void interupt(void)
{
	if(isSendToKeyboard) return;
	q[qbegin]={lastInteruptMicros=micros(),digitalRead(DPIN),digitalRead(CPIN)};
	qbegin++;
	qbegin%=QLEN;
	mut.unlock();

}
void init()
{
	if ( wiringPiSetup() < 0)
		exit( 10 );
	pinMode( CPIN, INPUT );
	pinMode( DPIN, INPUT );
	if ( wiringPiISR(CPIN, INT_EDGE_RISING, &interupt) < 0 ) {exit(11);}


}
 
int len=0;
int data=0;

bool sendToKeyboard(int in)
{
	fprintf(stderr,"SENT START %d\n",in);
	while (micros() - lastInteruptMicros < 200 || !digitalRead(DPIN) || !digitalRead(CPIN));
	isSendToKeyboard = 1;

	ui start = micros();
#define  whileOn {while (!digitalRead(CPIN)) if(micros()-start>50000) goto errSend;};
#define whileOff {while (!!digitalRead(CPIN)) if(micros()-start>50000) goto errSend;};

	pinMode(DPIN, OUTPUT);
	digitalWrite(DPIN, 0);
	int cs = 1 ;
	for (int i = 0; i < 8; i++)
	{
		whileOn;
		whileOff;
		digitalWrite(DPIN, (in >> i) & 1);
		cs += (in >> i) & 1;
	}
	whileOn;
	whileOff;
	digitalWrite(DPIN, cs & 1);
	whileOn;
	whileOff;
	digitalWrite(DPIN, 1);
	whileOn;
	whileOff;
	delayMicroseconds(10);
	pinMode(DPIN, INPUT);
	whileOn;
	delayMicroseconds(10);
	delayMicroseconds(99);
	delayMicroseconds(30);
	isSendToKeyboard = 0;
	fprintf(stderr,"SENT END\n",in);
	return 0;
errSend:
	digitalWrite(DPIN, 1);
	pinMode(DPIN, INPUT);
	isSendToKeyboard = 0;
	fprintf(stderr,"SENT END ERR!!\n",in);
	return 1;
}


void print(char const * prefix)
{
	fprintf(stderr,"%s%5d [",prefix,data);
	for(int i=0;i<len;i++) 
		fprintf(stderr,"%d",data>>i&1);
	fprintf(stderr,"]\n");
}
int main(int argc,char ** argv)
{
	init();
	delay(100);
	//sendToKeyboard(0xFF);
	//sendToKeyboard(0xFE);
	for(int i=0;i<8;i++)
	{
	//delay(1000);
	//sendToKeyboard(0xFF);
	//continue;
	//sendToKeyboard(0xED);
	//delayMicroseconds(1000);
	//sendToKeyboard(i%8);
	}
	int lastMicros=0;
	int lastTime=0;
	int last=-1;
	int lastData=-1;
	for(;;)
	{
		mut.lock();
		if(qend!=qbegin)
			fprintf(stderr,"START %d \n",int(micros()-q[qend].micros));
		else
			fprintf(stderr,"START NO\n");
		bool isFirstAftertInterupt=1;
		for(int i=0;i<2000000;i++)
		{
			if((qbegin!=qend?q[qend].micros:micros())-lastTime>250 && len)
			{
				if(len>4)
				{
				print("\t\t");
				if(len==11)// || !isFirstAftertInterupt)
                                {
                                        data>>=len-10;
                                        data<<=1;
                                        if((data&1)!=0) {fprintf(stderr,"Start bit err\n");goto err;}
                                        if((data>>10&1)!=1) {fprintf(stderr,"Stop bit err\n");goto err;}
                                        data>>=1;
                                        data&=(1<<9)-1;
                                        int par=0;
                                        for(int i=0;i<9;i++) par^=data>>i;
                                        par&=1;
                                        if(!par) {fprintf(stderr,"Parity bit err\n");goto err;}
                                        data&=(1<<8)-1;
					if(data == 0xFE) {fprintf(stderr,"0xFE err\n");goto errFE;}
					/*
					if(data != lastData) 
					{
						lastData=data;
						fprintf(stderr,"Next try %d\n",data);goto err;
					}
					else*/
					{
						lastData=-1;
						printf("%d\n",data);
					}
				}
				else
				{
					err:
					for(int i=0;i<15;i++)
						delayMicroseconds(99);
					sendToKeyboard(0xFE);
					errFE:
					isFirstAftertInterupt=0;
				}
				}
				data=len=0;
			}
		if(qbegin!=qend)
		{
			i=0;
			In act = q[qend];
			//printf("%d %d %d\n",act.c,act.d,int(act.micros-lastMicros));
			lastMicros = act.micros;
			qend++;
			qend%=QLEN;
			int c = act.c;
			//if(c!=last)
			{
				last=c;
				ui actTime = act.micros;
				ui timeChange = actTime-lastTime;
				/*if(timeChange>1000 && len)
				{
					print("TLE ");
					data=len=0;
				}*/
				lastTime = actTime;
				int d = act.d;
				//printf("%d %d -> %d\n",int(c),int(d),int(timeChange));
				if(c)
					data+=d<<len++;
				//if(data && len==1) {print("STE ");data=len=0;}
			}
			last=c;
		}
		/*
		if(len>=10 || len==10 && qbegin == qend && micros()-lastMicros>100)
		{
			{
				print(" -> ");
				if((data&1)!=0) {fprintf(stderr,"Start bit err\n");goto err;}
				//if((data>>10&1)!=1) {fprintf(stderr,"Stop bit err\n");continue;}
				data>>=1;
				data&=(1<<9)-1;
				int par=0;
				for(int i=0;i<9;i++) par^=data>>i;
				par&=1;
				if(!par) {fprintf(stderr,"Parity bit err\n");goto err;}
				data&=(1<<8)-1;
				printf("%d\n",data);
			}
			err:
			data=len=0;

		}
		*/
		}
		fprintf(stderr,"END\n");
	}
	/*
	int last=-10;
	for(;;)
	{
		printf("B %d\n",waitForInterrupt(CPIN, 1000) );
		for(int i=0;i<1000;i++)
		{
		}
		printf("E\n");
	}
	*/
	return(0);
}
