const int convertSize=256*256*2;
Tree::Key convert[convertSize];

void initConvert()
{
vector<vector<int>> a =
{
{
0x76,	//ESC	
0x05,	//F1 	
0x06,	//F2 	
0x04,	//F3 	
0x0C,	//F4 	
0x03,	//F5 	
0x0B,	//F6 	
0x83,	//F7 	
0x0A,	//F8 	
0x01,	//F9 	
0x09,	//F10	
0x78,	//F11	
0x07,	//F12	
},
{
0x0E,	//`  	
0x16,	//1  	
0x1E,	//2  	
0x26,	//3  	
0x25,	//4  	
0x2E,	//5  	
0x36,	//6  	
0x3D,	//7  	
0x3E,	//8  	
0x46,	//9  	
0x45,	//0  	
0x4E,	//-  	
0x55,	//=  	
0x66,	//Bac	
},
{
0x0D,	//Tab	
0x15,	//Q  	
0x1D,	//W  	
0x24,	//E  	
0x2D,	//R  	
0x2C,	//T  	
0x35,	//Y  	
0x3C,	//U  	
0x43,	//I  	
0x44,	//O  	
0x4D,	//P  	
0x54,	//[  	
0x5B,	//]  	
0x5A,	//Ent	
},
{
0x58,	//Cap	
0x1C,	//A  	
0x1B,	//S  	
0x23,	//D  	
0x2B,	//F  	
0x34,	//G  	
0x33,	//H  	
0x3B,	//J  	
0x42,	//K  	
0x4B,	//L  	
0x4C,	//;  	
0x52,	//'  	
0x5D,	//backslash
},
{
0x12,	//Shi	
0xFFFF,	//NUL	
0x1A,	//Z  	
0x22,	//X  	
0x21,	//C  	
0x2A,	//V  	
0x32,	//B  	
0x31,	//N  	
0x3A,	//M  	
0x41,	//,  	
0x49,	//.  	
0x4A,	///  	
0x59,	//Shi	
},
{
0x14,	//Ctr	
0xE01F,	//Win	
0x11,	//Alt	
0x29,	//Spa	
0xE011,	//Alt	
0xE027,	//Win	
0xE02F,	//Men			
0xE014,	//Ctr	
},
{
	0,
	0x7e,
	0xe012,
},
{
0xE070, //Insert	(E0F070)
0xE06C, //Home	(E0F06C)
0xE07D, //Page Up	(E0F07D)
},
{
0xE071, //Delete	(E0F071)
0xE069, //End		(E0F069)
0xE07A, //Page Dn	(E0F07A)
},
{
0xE075, //Up Arr	(E0F075)
},
{
0xE06B, //Left Ar	(E0F06B)
0xE072, //Down Ar	(E0F072)
0xE074, //Right A	(E0F074)
},
{
0x77  , //Num	    (F077)
0xE04A, ///       (E0F04A)
0x7C  , //*       (F07C)
0x7B  , //-       (F07B)
},
{
0x6C  , //7       (F06C)
0x75  , //8       (F075)
0x7D  , //9       (F07D)
0x79  , //+       (F079)
},
{
0x6B  , //4       (F06B)
0x73  , //5       (F073)
0x74  , //6       (F074)
},
{
0x69  , //1       (F069)
0x72  , //2       (F072)
0x7A  , //3       (F07A)
},
{
0x70  , //0       (F070)
0x71  , //.       (F071)
0xE05A, //Enter	(E0F05A)             
}
};
	for(int i=0;i<convertSize;i++) convert[i]=Tree::NULLKEY;
	for(int i=0;i<(int)a.size();i++)
		for(int j=0;j<(int)a[i].size();j++)
			convert[a[i][j]] = Tree::Key{i,j};
}
