#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include<fstream>
#include <mutex>
using namespace std;
#define MAXTIMINGS	85
using ld = long double;
using ll = long long;
using ui = unsigned int;

mutex mut;

int a=0,b=0;

const int CPIN=4;
const int DPIN=5;

const int QLEN=1000;

struct In
{
	ui micros;
	int d,c;
};
In q[QLEN];
int qbegin,qend;

void interupt(void)
{
	q[qbegin]={micros(),digitalRead(DPIN),digitalRead(CPIN)};
	qbegin++;
	qbegin%=QLEN;
	mut.unlock();

}
void init()
{
	if ( wiringPiSetup() < 0)
		exit( 10 );
	pinMode( CPIN, INPUT );
	pinMode( DPIN, INPUT );
	if ( wiringPiISR(CPIN, INT_EDGE_RISING, &interupt) < 0 ) {exit(11);}


}
 
int len=0;
int data=0;

void print(char const * prefix)
{
					fprintf(stderr,"%s%5d [",prefix,data);
					for(int i=0;i<len;i++) 
						fprintf(stderr,"%d",data>>i&1);
					fprintf(stderr,"]\n");
}


int main(int argc,char ** argv)
{
	init();
	int lastMicros=0;
	int lastTime=0;
	//int last=-1;
	for(;;)
	{
		mut.lock();
		for(int i=0;i<100000000;i++)
		{
			if(qbegin!=qend)
			{
				In act = q[qend];
				//printf("%d %d %d\n",act.c,act.d,int(act.micros-lastMicros));
				lastMicros = act.micros;
				qend++;
				qend%=QLEN;
				int c = act.c;
				//if(c!=last)
				{
					//last=c;
					ui actTime = act.micros;
					ui timeChange = actTime-lastTime;
					if(timeChange>1000 && len)
					{
						print("TLE ");
						data=len=0;
					}
					lastTime = actTime;
					int d = act.d;
					//printf("%d %d -> %d\n",int(c),int(d),int(timeChange));
					if(c)
						data+=d<<len++;
					if(data && len==1) {print("STE ");data=len=0;}
				}
				//last=c;
			}
			if(len>=10 || (len==10 && qbegin == qend && micros()-lastMicros>100))
			{
				{
					print(" -> ");
					if((data&1)!=0) {fprintf(stderr,"Start bit err\n");goto err;}
					//if((data>>10&1)!=1) {fprintf(stderr,"Stop bit err\n");continue;}
					data>>=1;
					data&=(1<<9)-1;
					int par=0;
					for(int i=0;i<9;i++) par^=data>>i;
					par&=1;
					if(!par) {fprintf(stderr,"Parity bit err\n");goto err;}
					data&=(1<<8)-1;
					fprintf(stderr,"%d\n",data);
					printf("%d\n",data);
					fflush(stdout);
				}
				err:
				data=len=0;

			}
		}
	}
	/*
	int last=-10;
	for(;;)
	{
		printf("B %d\n",waitForInterrupt(CPIN, 1000) );
		for(int i=0;i<1000;i++)
		{
		}
		printf("E\n");
	}
	*/
	return(0);
}
