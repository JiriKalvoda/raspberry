#include "../led/led-include.h"
#include <map>
#include <functional>

const int MAPH=16;
const int MAPW=14;

using namespace std;
using ld = long double;
using ll = long long;
using ui = unsigned int;
using Tree::Key;


LED ledImp;

using Keymap = std::map<Key,std::function<void ()> >;

const char * mode = "default";
Key::MOD modif=Key::MOD(0);

// ***************** keymap *************************************
using KeyModeKey =  function<void ()>;
struct KeyMode
{
	KeyModeKey * map[MAPH][MAPW];
};
namespace k
{
	namespace led
	{
		const int NUMSTRLEN=10;
		KeyModeKey * light(const char * name, int par){return new KeyModeKey([name,par]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.light(name,val);
			});}
		KeyModeKey * color(const char * name, int par){return new KeyModeKey([name,par]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.color(name,val);
			});}
		KeyModeKey * swap(const char * name,const char * a,const char *b){return new KeyModeKey([name,a,b]()
			{
				ledImp.swap(name);
			});}
		KeyModeKey * stop(const char * name){return new KeyModeKey([name]()
			{
				ledImp.f(name,"-1","-1","-1","-1");
			});}
		KeyModeKey * lightSpeed(const char * name, int par){return new KeyModeKey([name,par]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.lightSpeed(name,val);
			});}
		KeyModeKey * colorSpeed(const char * name, int par){return new KeyModeKey([name,par]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.colorSpeed(name,val);
			});}
		KeyModeKey * lightBig(const char * name, int par){return new KeyModeKey([name,par]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.lightBig(name,val);
			});}
		KeyModeKey * colorBig(const char * name, int par){return new KeyModeKey([name,par]()
			{
				char val[NUMSTRLEN];sprintf(val,"%d",par);ledImp.colorBig(name,val);
			});}
	}
	KeyModeKey * cmd(const char * in){return new KeyModeKey([in]()
		{
		char val[12345];sprintf(val,"(%s)&",in);system(val);puts(val);fflush(stdout);
		});}
	KeyModeKey * goMode(const char * in){return new KeyModeKey([in]()
		{
			mode=in;
		});}
	KeyModeKey * undef = new KeyModeKey([](){});
	KeyModeKey * null = undef;
}
#include "keymap.cpp"


//Keymap * rootKeyMap;
//Keymap * actualKeyMap;
//Keymap * allModeKeyMap;

const int NUMSTRLEN=10;



#include "convert.cpp"



int main()
{
	ledImp.loadTree(Tree::root);
	//allModeKeyMap = new Keymap;
	//rootKeyMap = new Keymap;
	//for(auto it:led.partByName["pokojik"]->dev)
		//f(it,rootKeyMap);
	//for(auto it:led.partByName["pokojik"]->subPart)
		//f(it,rootKeyMap);
	//actualKeyMap=rootKeyMap;
	//allModeKeyMap[0][Key{0,0}] = [=]{actualKeyMap=rootKeyMap;};
	initConvert();
	int data;
	bool lastEndMark=0;
	bool lastE0 = 0;
	int ignoreextN=0;
	while(scanf("%d",&data)==1)
	{
		if(lastEndMark)
		{
			if(ignoreextN) ignoreextN--;
			else
			{
				Key k = convert[data+(lastE0?0xe000:0)];
				if(k.null)
					printf("NULL\n");
				else
				{
					fflush(stdout);
					if(k == Key{4,0}) modif = Key::MOD(modif | Key::SHIFT);
					else
					if(k == Key{5,0}) modif = Key::MOD(modif | Key::CTRL);
					else
					{
						k.mod = modif;
						modif = Key::MOD(0);
						//if(allModeKeyMap->count(k))
						//{
							//allModeKeyMap[0][k]();
						//}
						//if(actualKeyMap->count(k))
						//{
							//actualKeyMap[0][k]();
						//}
						if(keyModes.count({mode,k.mod}))
						{
							keyModes[{mode,k.mod}].map[k.row][k.col][0]();
						}
					}
				}
			}
			lastEndMark=0;
			lastE0=0;
		}
		if(data==0xe1)
		{
			ignoreextN=2;
			{
				Key k = {6,2};
				k.mod = modif;
				modif = Key::MOD(0);
				printf("R%d C%d\n",k.row,k.col);
				//if(allModeKeyMap->count(k))
				//{
					//allModeKeyMap[0][k]();
				//}
				//if(actualKeyMap->count(k))
				//{
					//actualKeyMap[0][k]();
				//}
						if(keyModes.count({mode,modif}))
						{
							keyModes[{mode,modif}].map[k.row][k.col][0]();
						}
			}
		}
		if(data==0xe0) lastE0=1;
		else
		if(data==240) lastEndMark=1;
		else
			lastE0=0;
	}

}
