#!/bin/python3
import time
import random
from paho.mqtt import client as mqtt_client
import os, sys
from pathlib import Path



night_mode_mqtt_topic = "jk/led-hook/night/enable"


client_id = f'raspberry-powr-led-{random.randint(0, 1000)}'

def connect_mqtt():
    broker = 'jug9'
    secret_file_prefix = f'{Path.home()}/.secret/mqtt/'
    addres = open(f'{secret_file_prefix}{broker}-all.addres',mode='r').read().strip()
    username = open(f'{secret_file_prefix}{broker}-power-led.user',mode='r').read().strip()
    passwd = open(f'{secret_file_prefix}{broker}-power-led.passwd',mode='r').read().strip()
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, passwd)
    client.on_connect = on_connect
    client.connect(addres)
    return client

def on_connect(mqtt, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    mqtt.subscribe(night_mode_mqtt_topic)

night_mode = False

def set_led(state: bool):
    f = open("/sys/devices/platform/leds/leds/led1/brightness", "w")
    f.write('1' if state else '0')
    f.close()
    f = open("/sys/devices/platform/leds/leds/led0/trigger", "w")
    f.write('mmc0' if state else 'none')
    f.close()
    print("SET", state, flush=True)

def on_message(mqtt, userdata, msg):
    if msg.topic == night_mode_mqtt_topic:
        global night_mode
        night_mode = msg.payload.decode().split('\n')[0] != '0'
        set_led(not night_mode)




mqtt = connect_mqtt()
mqtt.on_connect = on_connect
mqtt.on_message = on_message
mqtt.loop_forever()
