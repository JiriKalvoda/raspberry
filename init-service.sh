#!/bin/bash
name="$1"
user="$2"
exe="$3"
par="$4"
opt="$5"
installToUser=false

if [ -z "$name" ] || [ -z "$user" ] || [ -z "$exe" ]
then 
	echo "Script for creating systemd deamon"
	echo "Usage:"
	echo "	$0 name [user/'--user'] exec [cmd parametrs] [daemon options]"
	exit 100
fi

if [[ "$user" == "--user" ]]
then
	user="$USER"
	installToUser=true
	# loginctl enable-linger USERNAME
fi

if $installToUser
then
	systemctlUserArg="--user"
fi

if id "$user" &>/dev/null; then  true ; else
    echo "user $user not found"
	exit 1
fi

home="$(getent passwd "$user" | cut -d: -f6)"
path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/sbin:/usr/bin:/bin:$home/bin"
if [[ "$exe" == "/"* ]]
then
	exePath="$exe"
else
	exePath="/usr/bin/$exe"
fi

[ -z "$exePath" ] && exit 2


if $installToUser
then
lineUser=""
else
lineUser="User=$user"
fi

if $installToUser
then
lineWantedBy="WantedBy=default.target"
else
lineWantedBy="WantedBy=multi-user.target"
fi

cat <<EOF
[Unit]
Description=$name
After=network.target
StartLimitIntervalSec=10

[Service]
$lineUser
Environment=HOME=$home
Environment=PATH=$path
ExecStart=$exePath $par
Restart=always
RestartSec=10
$opt

[Install]
$lineWantedBy
EOF
