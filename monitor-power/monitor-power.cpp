//#include <vector>
#define USING_POST_HOOK
#include <unistd.h>
#include <cstring>
#include <random>
#include <sys/types.h>
#include <sys/stat.h>
#include <grp.h>
#include <cstdio>


#define fo(a, b) for(int a=0;a<(b); a++)


using ll=long long;

bool state;
char * on_start;
char * on_stop;

void do_on_start_stop()
{
	if(state)
		system(on_start);
	else
		system(on_stop);
}

void write();

#ifdef OUT_RELAY
#include "../lib/gpio.h"
using Gpio::getLine;
using Gpio::digitalWrite;
using Gpio::pinModeOutput;
using Gpio::Line;
int pin; //(GPIO numbers)
Line line;
void init()
{
	Gpio::init();
	line = getLine(pin);
	pinModeOutput(line, !(state));
}
void write_hw()
{
	digitalWrite(line, !state);
}
void usageExit(const char * name)
{
	printf(
			"Start me as daemon by executing:\n"
			"\n"
			"	%s <pin (GPIO numbers)> <fifo file place> <default_state(0/1)>\n"
			,name);
	exit(0);
}
#endif

#ifdef OUT_FILE
char * write_fname;
void write_hw()
{
	FILE *out =  fopen(write_fname, "w");
	fprintf(out, "%d\n", !state);
	fclose(out);
}
void init() {write();}
void usageExit(const char * name)
{
	printf(
			"Start me as daemon by executing:\n"
			"\n"
			"	%s <sys file> <fifo file place> <default_state(0/1)>\n"
			,name);
	exit(0);
}
#endif

void write()
{
	write_hw();
	do_on_start_stop();
}

int main(int argc,char ** argv)
{
	if(argc!=6) usageExit(argv[0]);

#ifdef OUT_RELAY
	pin = atoi(argv[1]);
#endif
#ifdef OUT_FILE
	write_fname =argv[1];
#endif
	char * path = argv[2];
	state = atoi(argv[3]);
	on_start = argv[4];
	on_stop = argv[5];

	unlink(path);
	if(mkfifo(path, 0620))
	{
		printf("Oh dear, something went wrong with mkfifo()! %s\n", strerror(errno));
		exit(1);
	}
	struct group *g = getgrnam("monitor-control");
	if(!g)
	{
		printf("No sutch group!\n");
		exit(1);
	}
	if(chown(path, -1, g->gr_gid))
	{
		printf("Oh dear, something went wrong with chmod()! %s\n", strerror(errno));
		exit(1);
	}
	if(chmod(path, 0620))
	{
		printf("Oh dear, something went wrong with chmod()! %s\n", strerror(errno));
		exit(1);
	}

	init();

	printf("Waiting for commands...'\n");
	fflush(stdout);
	FILE *in = fopen(path, "r");
	FILE *hold_open = fopen(path, "w");

	while(true)
	{
		char s[1234];
		fscanf(in, "%100s", s);
		printf("Input: '%s'\n", s);
		if(!strcmp(s, "0") || !strcmp(s, "off"))
			state = 0;
		else
		if(!strcmp(s, "1") || !strcmp(s, "on"))
			state = 1;
		else
		if(!strcmp(s, "!") || !strcmp(s, "toggle"))
			state = !state;
		else
			printf("Wrong command!\n");
		printf("State set to %d\n", state);
		fflush(stdout);
		write();
	}
	return 0;
}
