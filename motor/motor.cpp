#include "../lib/gpio.h"
#include <stdlib.h>
#include <stdbool.h>
#include <random>
#include <mutex>
#include <string.h>
#include <MQTTClient.h>
#include <unistd.h>
#include "../led/deviceTree.h"

#define fo(a,b) for(int a=0;a<(b);++a)
#define DMQTT if (0)

using Tree::FNAMELEN;

using Gpio::p;
using Gpio::digitalWrite;
using Gpio::pinModeOutput;

Tree::Mosquitto *mosquitto;
MQTTClient mqtt;
std::mutex mqttMutex;

char * mqttPrefix;
int maxPlace;
int calibratePlace;
int go_over=0;
double duplicit_calibration=0;
int sleep_time_us = 2'000;

volatile bool calibrate=true;
volatile int actPlace=0;
volatile int gotoPlace=0;
volatile int tmpGotoPlace=0;
volatile bool enable=0;


void set_gotoPlace(int x)
{
	if(maxPlace*x<0) x=0;
	if(maxPlace*x>maxPlace*maxPlace) x=maxPlace;
	gotoPlace = x;
	tmpGotoPlace = x;
	if(actPlace < x) tmpGotoPlace+=go_over;
	if(actPlace > x) tmpGotoPlace-=go_over;
	if(maxPlace*tmpGotoPlace<0) tmpGotoPlace=0;
	if(maxPlace*tmpGotoPlace>maxPlace*maxPlace) tmpGotoPlace=maxPlace;
}



int msgarrvd_cb(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
	int i;
	DMQTT printf("Message arrived\n");
	DMQTT printf("     topic: %s\n", topicName);
	DMQTT printf("   message: ");
	char * payloadptr = (char *)message->payload;
	DMQTT for(i=0; i<message->payloadlen; i++)
	{
		putchar(payloadptr[i]);
	}
	DMQTT putchar('\n');
	char * in = new char [message->payloadlen+1];
	for(int i=0;i<message->payloadlen;i++)
		in[i]=payloadptr[i];
	in[message->payloadlen]=0;

	std::lock_guard<std::mutex> guard(mqttMutex);

	if(!strncmp(topicName,mqttPrefix,strlen(mqttPrefix)))
	{
		char *topicNameWP = topicName+strlen(mqttPrefix);
		if(!strcmp(topicNameWP, "goto"))
		{
			double val = strtod(in, NULL);
			if(val < 0) val = 0;
			if(val > 1) val = 1;
			if(val == 0 && actPlace==0)
				actPlace += duplicit_calibration * maxPlace;
			if(val == 1 && actPlace==maxPlace)
				actPlace -= duplicit_calibration * maxPlace;
			set_gotoPlace(maxPlace*val);
			enable = 1;
		}
		if(!strcmp(topicNameWP, "calibrate")) calibrate = 1, enable = 1;
	}

	delete in;
	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}

void connlost_cb(void *context, char *cause)
{
    printf("\nMQTT connection lost\n");
    printf("     cause: %s\n", cause);
	exit(1);
}

void initMqtt()
{
	for(int i=0;;i++)
	{
		MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
		int rc;
		std::string addres = mosquitto->getAddres();
		std::string username = mosquitto->getUsername("led-include");
		std::string passwd = mosquitto->getPasswd("led-include");
		static char mqttConName[1234] = "";
		std::random_device rd;
		if(!mqttConName[0]) sprintf(mqttConName,"motor-%d-%d",int(rd()),int(rd()));
		MQTTClient_create(&mqtt, addres.c_str(), mqttConName,
				MQTTCLIENT_PERSISTENCE_NONE, NULL);
		conn_opts.keepAliveInterval = 20;
		conn_opts.cleansession = 1;
		conn_opts.username = username.c_str();
		conn_opts.password = passwd.c_str();
		MQTTClient_setCallbacks(mqtt, NULL, connlost_cb, msgarrvd_cb, NULL);

		if ((rc = MQTTClient_connect(mqtt, &conn_opts)) != MQTTCLIENT_SUCCESS && i==10)
		{
			printf("Failed to connect MQTT, return code %d\n", rc);
			exit(-1);

		}
		if(rc == MQTTCLIENT_SUCCESS) break;
		usleep(1000 * 1000);
	}
	printf("CONECT OK\n");
	fflush(stdout);
}

void subscribeMqtt()
{
	char fname[FNAMELEN];
	sprintf(fname,"%s#",mqttPrefix);
	MQTTClient_subscribe(mqtt, fname, 1);
}

void usageExit(char *prname)
{
	fprintf(stderr, "Wrong argv\n");
	exit(1);
}

int pins[4];

void set(int p)
{
	p%=8;
	p+=8;
	p%=8;
	bool out[4];
	fo(i,4) out[i]=0;
	out[p/2]=1;
	out[(p+1)/2%4]=1;
    fo(i,4) digitalWrite(Gpio::p(pins[i]), out[i]);
}
void set_off()
{
	fo(i,4) digitalWrite(Gpio::p(pins[i]), 0);
}
int main (int argc, char ** argv)
{
	Gpio::init();

	if(argc<=1+3+4+1) usageExit(argv[0]);
	mosquitto = Tree::mosquitto_map[argv[1]];
	mqttPrefix = argv[2];
	maxPlace = atoi(argv[3]);
	fo(i,4) pins[i] = atoi(argv[4+i]);
	fo(i,4) pinModeOutput(p(pins[i]));
	if(argc>8 && argv[8][0])
		calibratePlace = atoi(argv[8]);
	else calibratePlace = 2 * maxPlace;
	if(argc>9 && argv[9][0])
		go_over = atoi(argv[9]);
	duplicit_calibration = atof(argv[10]);
	if(argc>11 && argv[11][0])
		sleep_time_us = atoi(argv[11]);

	initMqtt();
	subscribeMqtt();


	while(true)
	{
		if(!enable)
		{
			usleep(1'000'000);
			continue;
		}
		if(calibrate)
		{
			calibrate = 0;
			printf("CALIBRATION BEGIN\n");
			fflush(stdout);
			actPlace = calibratePlace;
			while(actPlace)
			{
				if(0 < actPlace) actPlace--;
				else
					actPlace++;
				set(actPlace);
				usleep(sleep_time_us);
			}
			printf("CALIBRATION END\n");
			fflush(stdout);
			set_gotoPlace(gotoPlace);
			continue;
		}
		if(actPlace == tmpGotoPlace) tmpGotoPlace = gotoPlace;
		if(actPlace != tmpGotoPlace)
		{
			if(tmpGotoPlace < actPlace) actPlace--;
			else
				actPlace++;
			if(actPlace%100 == 0) printf("actual place %lf\n", actPlace/(double)maxPlace);
			fflush(stdout);
			set(actPlace);
			usleep(sleep_time_us);
		}
		else
		{
			usleep(100'000);
			set_off();
		}
	}


	/*
	int x = atoi(argv[1]);
	wiringPiSetup () ;
	fo(i,4) pinMode (pins[i], OUTPUT) ;
	for (int i=0;x<0?i>x:i<x;x<0?i--:i++)
	{
		set(i);
		usleep(1500);
	}
	set_off();
	*/
	return 0 ;
}
