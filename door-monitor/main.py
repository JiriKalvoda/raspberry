#!/usr/bin/python3
import gpiod
import time
import random
import sys
from pathlib import Path
from paho.mqtt import client as mqtt_client

pins = []
mqtt_topics = []

for i in range(len(sys.argv)//2):
    pins.append(int(sys.argv[i+1]))
for i in range(len(sys.argv)//2):
    mqtt_topics.append(sys.argv[i+1+len(sys.argv)//2])


chip = gpiod.Chip("gpiochip0")

client_id = f'door-monitor-{random.randint(0, 1000)}'
lines = [chip.get_line(x) for x in  pins]

def connect_mqtt():
    broker = 'jug9'
    secret_file_prefix = f'{Path.home()}/.secret/mqtt/'
    addres = open(f'{secret_file_prefix}{broker}-all.addres',mode='r').read().strip()
    username = open(f'{secret_file_prefix}{broker}-door-monitor.user',mode='r').read().strip()
    passwd = open(f'{secret_file_prefix}{broker}-door-monitor.passwd',mode='r').read().strip()
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, passwd)
    client.on_connect = on_connect
    client.connect(addres)
    return client

mqtt = connect_mqtt()
mqtt.loop_start()


for line in lines:
    line.request(consumer="User", type=gpiod.LINE_REQ_DIR_IN, flags=gpiod.LINE_REQ_FLAG_BIAS_PULL_UP)

while True:
    last_data = [-1 for _ in pins]
    for i in range(500):
        data = [line.get_value() for line in lines]
        if data != last_data:
            print(data, flush=True)
        for dato, topic, last_dato in zip(data, mqtt_topics, last_data):
            if last_dato != dato:
                mqtt.publish(topic, f"{dato}", retain=True)
        last_data = data
        time.sleep(0.2)
