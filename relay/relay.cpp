//#include <vector>
#define USING_POST_HOOK
#include "../lib/gpio.h"
#include <unistd.h>
#include <cstring>
#include <random>

#include <MQTTClient.h>


#include "../led/deviceTree.h"

#define fo(a, b) for(int a=0;a<(b); a++)

using Tree::FNAMELEN;
using Tree::FLEN;

using Gpio::getLine;
using Gpio::digitalWrite;
using Gpio::pinModeOutput;
using Gpio::Line;

using ll=long long;

MQTTClient mqtt;
Tree::Mosquitto* mosquitto;

struct Output
{
	const char * mqtt_path;
	const char * excepted_val;
	int pin; //(GPIO numbers)
	Line line;
} outputs[100];

int outputsLen=0;

#define DMQTT if(0)

void init()
{
	Gpio::init();
	fo(i, outputsLen)
	{
		outputs[i].line = getLine(outputs[i].pin);
		pinModeOutput(outputs[i].line, !(0));
	}
}


void delivered(void *context, MQTTClient_deliveryToken dt)
{
}

int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    int i;
    DMQTT printf("Message arrived\n");
    DMQTT printf("     topic: %s\n", topicName);
    DMQTT printf("   message: ");
    char * payloadptr = (char *)message->payload;
    DMQTT for(i=0; i<message->payloadlen; i++)
    {
        putchar(payloadptr[i]);
    }
    DMQTT putchar('\n');

    for(int i=strlen(payloadptr)-1;i>=0 && (payloadptr[i]==' ' || payloadptr[i]=='\n');i--)
	    payloadptr[i]='\0';

    fo(i, outputsLen)
	    if(!strcmp(topicName, outputs[i].mqtt_path))
	    {
		    bool val = !strcmp(payloadptr, outputs[i].excepted_val);
		    digitalWrite(outputs[i].line, !val);
		    printf("%s %d ('%s' arrived to %s)\n",val?"Starting":"Stoping", outputs[i].pin, payloadptr, topicName);
		    fflush(stdout);
	    }

    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}
void connlost(void *context, char *cause)
{
    printf("\nMQTT connection lost\n");
    printf("     cause: %s\n", cause);
    fo(i, outputsLen) digitalWrite(outputs[i].line, !0);
    exit(1);
}

char mqttConName[1234];
void initMqtt()
{
	for(int i=0;;i++)
	{
		MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
		int rc;

		std::string addres = mosquitto->getAddres();
		std::string username = mosquitto->getUsername("relay");
		std::string passwd = mosquitto->getPasswd("relay");
		std::random_device rd;
		if(!mqttConName[0]) sprintf(mqttConName, "relay-%d-%d",int(rd()),int(rd()));
		MQTTClient_create(&mqtt, addres.c_str(), mqttConName,
				MQTTCLIENT_PERSISTENCE_NONE, NULL);
		conn_opts.keepAliveInterval = 20;
		conn_opts.cleansession = 1;
		conn_opts.username = username.c_str();
		conn_opts.password = passwd.c_str();
		MQTTClient_setCallbacks(mqtt, NULL, connlost, msgarrvd, delivered);

		if ((rc = MQTTClient_connect(mqtt, &conn_opts)) != MQTTCLIENT_SUCCESS && i==10)
		{
			printf("Failed to connect MQTT, return code %d\n", rc);
			exit(-1);

		}
		if(rc == MQTTCLIENT_SUCCESS) break;
		usleep(1000 * 1000);
	}
	printf("CONECT OK\n");
	fflush(stdout);
}


void usageExit(const char * name)
{
#define PREPROC_XSTR(s) PREPROC_STR(s)
#define PREPROC_STR(s) #s
	printf(
			"Start me as daemon by executing:\n"
			"\n"
			"	%s <mqtt_broker_name> <mqtt_topic> [=<shoult be val] <pin (GPIO numbers)> ...\n"
			"\n"
			"Configuration file is located at %s/../led/deviceTree.h (apply charges need recompilation)\n"
			,name,PREPROC_XSTR(BUILDPATH));
	exit(0);
#undef PREPROC_XSTR
#undef PREPROC_STR
}

int main(int argc,char ** argv)
{
	if(argc<4) usageExit(argv[0]);
	mosquitto = Tree::mosquitto_map[argv[1]];
	if(!mosquitto) usageExit(argv[0]);
	for(int i=2; i<argc; i++)
	{
		outputs[outputsLen].mqtt_path = argv[i];
		if(++i >= argc) usageExit(argv[0]);
		if(argv[i][0] == '=')
		{
			outputs[outputsLen].excepted_val = argv[i]+1;
			if(++i >= argc) usageExit(argv[0]);
		}
		else
			outputs[outputsLen].excepted_val = "1";
		outputs[outputsLen].pin = atoi(argv[i]);
		outputsLen++;
	}
	fo(i, outputsLen)
		printf("CONFIGURED %d %s %s\n", outputs[i].pin, outputs[i].mqtt_path, outputs[i].excepted_val);
	init();
	initMqtt();
	fo(i, outputsLen)
		MQTTClient_subscribe(mqtt, outputs[i].mqtt_path, 1);
	while(true) sleep(1000);
	return 0;
}
