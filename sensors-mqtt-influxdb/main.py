from influxdb import InfluxDBClient
import random
import time
from pathlib import Path

from datetime import datetime

from paho.mqtt import client as mqtt_client

class Db():
    def __init__(self):
        self.dbh = InfluxDBClient(host='10.19.32.1',database='jug9')

    def insert(self, measurement, name, timestamp, data):
        timestamp = int(timestamp * 1000000000)
        point = {
                "measurement": measurement,
                "time": timestamp,
                "tags": {
                    "device-id": name,
                    },
                "fields": data,
                }
        self.dbh.write_points([point])

dbh = Db()

client_id = f'sensors-mqtt-influx-{random.randint(0, 1000)}'

def connect_mqtt():
    broker = 'jug9'
    secret_file_prefix = f'{Path.home()}/.secret/mqtt/'
    addres = open(f'{secret_file_prefix}{broker}-all.addres',mode='r').read().strip()
    username = open(f'{secret_file_prefix}{broker}-sensors-mqtt-influx.user',mode='r').read().strip()
    passwd = open(f'{secret_file_prefix}{broker}-sensors-mqtt-influx.passwd',mode='r').read().strip()
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!", flush=True)
            mqtt.subscribe("sensors/#")
            mqtt.subscribe("heating/#")
        else:
            print("Failed to connect, return code %d\n", rc, flush=True)
    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, passwd)
    client.on_connect = on_connect
    client.connect(addres)
    return client

mqtt = connect_mqtt()

def parse_and_insert(measurement, device_name, pl):
        timestamp = int(pl.split("\n")[0])
        data = { i.split(" ")[0]: float(i.split(" ")[1]) for i in pl.split("\n")[1:] }
        print(measurement, device_name, timestamp, data, flush=True)
        dbh.insert(measurement, device_name, timestamp, data)

def on_message(mqtt, userdata, msg):
    try:
        topic = msg.topic.split('/')
        pl = msg.payload.decode()
        if topic[0]=="sensors" and topic[-1]=="all":
            device_name = '.'.join(topic[1:-1])
            if device_name.startswith("jug9."): device_name = "pokoj" + device_name[5:]
            parse_and_insert("sensor", device_name, pl)
        if topic[0]=="heating" and topic[-1]=="log":
            device_name = '.'.join(topic[1:-1])
            if device_name.startswith("jug9."): device_name = "pokoj" + device_name[5:]
            parse_and_insert("heating", device_name, pl)
    except BaseException as err:
        print(f"on_message error {err} ({type(err)})", flush=True)



mqtt.on_message = on_message
mqtt.loop_forever()

while True:

    dbh.insert_sensor("sensor", 0, {})

    time.sleep(5)
