#include "deviceTree.h"
#include "colors.h"
#include "postHooks.h"

//Empty API for post hoks
#include <MQTTClient.h>
#include <mutex>
MQTTClient mqtt;
std::unordered_map<std::string,char *> mqttMap;
std::mutex mqttMapMutex;


#define fo(a,b) for(int a=0;a<(b);++a)
using ll = long long;

void usageExit(char * name)
{
	fprintf(stderr, "USAGE:\n\t%s HELP_NAME\n\n",name);
	fprintf(stderr, "Available names:\n");
	for(auto it : Tree::hooksForDoc) fprintf(stderr, "\t%s\n",it.first.c_str());
	exit(1);
}

int main(int argc, char ** argv)
{
	if(argc!=2) usageExit(argv[0]);
	char * docname = argv[1];
	if(!Tree::hooksForDoc.count(docname)) usageExit(argv[0]);
	auto hooks = Tree::hooksForDoc[docname];
	printf("\
\\input ucwmac2\n\
\\ucwmodule{verb}\n\
\\ucwmodule{ofs}\n\
\\chyph\n\
\\parskip 3pt\n\
\\parindent 0pt\n\
\\raggedbottom\n\
\n\
\\font\\hhdfont csbx17\n\
\\font\\hdfont csbx12\n\
\\font\\smallboldfont csbx10 at 7.5pt\n\
\\def\\hd#1{\\bigskip\\leftline{\\hfill\\hhdfont #1\\hfill}\\interlinepenalty 10000}\n\
\\def\\colordot#1{\
\\hskip 0pt\\hbox to 0pt{\\settextsize{20}\
\\hbox to 0pt{{\\hfil\\colorlocal{\\rgb{#1}}$\\bullet$\\hfil}\\hskip -20pt}\
\\hbox to 0pt{{\\hfil\\colorlocal{\\rgb{0 0 0}}$\\circ$\\hfil}\\hskip -20pt}\
}\\hskip 20pt\
}\n\
\n\
\\def\\columnbox{\\leftline{\\pagebody}}\n\
\\def\\fullline{\\hbox to \\fullhsize}\n\
\\hoffset=-.5in\n\
\\newdimen\\fullhsize\n\
\\fullhsize=7.5in \\hsize=3.6in\n\
\\def\fullline{\\hbox to \\fullhsize}\n\
\\let\\lr=L \\newbox\\leftcolumn\n\
\\output={\\if L\\lr\n\
\\global\\setbox\\leftcolumn=\\columnbox \\global\\let\\lr=R\n\
\\else \\doubleformat \\global\\let\\lr=L\\fi\n\
\\ifnum\\outputpenalty>-20000 \\else\\dosupereject\\fi}\n\
\\def\\doubleformat{\\shipout\\vbox{\n\
\\fullline{\\box\\leftcolumn\\hfil\\vrule\\hfil\\leftline{\\pagebody}}\n\
}\n\
}\n\
\n\
\\def\\\\\{\\vskip 0pt}\n\
"				);


	for(Tree::Hook* it:hooks) it->printTeXDoc(stdout);
	printf("\\bye\n");
	return 0;
}

