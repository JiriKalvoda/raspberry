#include <string.h>

#include "led-include.h"
#include "colors.h"

LED led;

void hint(const char * world,int parnum,int argc,const char ** argv,FILE * out=stdout)
{
	if(!strcmp(world,"--")) world="";
	if(parnum==0)
	{
		for(const char * it:led.allNames)
		{
			for(int i=0;;i++)
			{
				if(!world[i]) {fprintf(out,"%s\n",it);break;}
				if(it[i]!=world[i]) break;
			}
		}

	}
	if(parnum==1)
	{
		for(const char * it:{"l","c","C","s","S","ch"})
		{
			for(int i=0;;i++)
			{
				if(!world[i]) {fprintf(out,"%s\n",it);break;}
				if(it[i]!=world[i]) break;
			}
		}
	}
}

void usageExit(char * name)
{
	printf("USAGE:\n"
			"\t%s NAME LIGHT [R G B] [LIGHT_SPEED] [COLOR_SPEED]\n"
			"\t\tWrite '_' as any parameters mean that it will not be changed.\n"
			"\t\tLIGHT, R, G and B are two dimensional array of float in range 0-1 or special value -1 (stop changing).\n"
			"\t\t\tFirst dimension is separated by ' ' (space). It will split strip on parts with the same length.\n"
			"\t\t\tSecond dimension is separated by ':'. This array will be periodically repeat in part in LED strip.\n"
			"\t%s NAME ch [<list of channels>]\n"
			"\t%s NAME l LIGHT_KEY<int in range 0-3>\n"
			"\t%s NAME lb LIGHT_KEY<int> KEY_COUNT<int> [MAX_BOOST<double>]\n"
			"\t%s NAME c COLOR_KEY<int in range 0-3>\n"
			"\t%s NAME L LIGHT_LONG_KEY<int in range 0-11>\n"
			"\t%s NAME C COLOR_LONG_KEY<int in range 0-%d>\n"
			"\t%s NAME s LIGHT_SPEED_KEY<int in range 0-MAX_KEY> [MAX_KEY<int default 3>]\n"
			"\t%s NAME S COLOR_SPEED_KEY<int in range 0-MAX_KEY> [MAX_KEY<int default 3>]\n"
			"NAME specify device or set of devices (part). Use ' ' (SPACE) to apply changes on multiple devices/parts.\n"
			,name,name,name,name,name,name,name,colorsLen-1,name,name);
	printf("\nAvailable names:\n");
	for(const char * it : led.allNames)
		printf("\t%s\n",it);
}


int main(int argc,char ** argv)
{
	led.loadTree(Tree::root);
	for(int i=1;i<argc;i++)
	{
		if(!strcmp(argv[i],"-h") || !strcmp(argv[i],"--help")) usageExit(argv[0]);
	}
	if(argc>1 && !strcmp(argv[1],"_")) argv[1][0]=0;

	if(argc-1>=3 && !strcmp(argv[2],"ch")) led.channels(argv[1],(const char **)argv+3,argc-3);
	else
	if((argc-1==3 || argc-1==4) && !strcmp(argv[2],"l")) led.light(argv[1],argv[3],argc-1>=4?atoi(argv[4]):3);
	else
	if((argc-1==4 || argc-1==5) && !strcmp(argv[2],"lb")) led.lightBoost(argv[1],argv[3],atoi(argv[4]), argc-1>=5?strtod(argv[5],NULL):-1);
	else
	if((argc-1==3 || argc-1==4) && !strcmp(argv[2],"c")) led.color(argv[1],argv[3],argc-1>=4?atoi(argv[4]):3);
	else
	if(argc-1>=3 && !strcmp(argv[2],"H")) hint(argv[1],atoi(argv[3])-1,argc-4,(const char **)argv+4);
	else
	if(argc-1==3 && !strcmp(argv[2],"L")) led.lightBig(argv[1],argv[3]);
	else
	if(argc-1==3 && !strcmp(argv[2],"C")) led.colorBig(argv[1],argv[3]);
	else
	if(argc-1==3 && !strcmp(argv[2],"s")) led.lightSpeed(argv[1],argv[3]);
	else
	if(argc-1==3 && !strcmp(argv[2],"S")) led.colorSpeed(argv[1],argv[3]);
	else
	if(argc-1==2) led.f(argv[1],argv[2],"_","_","_","_","_");
	else
	if(argc-1==5) led.f(argv[1],argv[2],argv[3],argv[4],argv[5],"_","_");
	else
	if(argc-1==6) led.f(argv[1],argv[2],argv[3],argv[4],argv[5],argv[6],"_");
	else
	if(argc-1==7) led.f(argv[1],argv[2],argv[3],argv[4],argv[5],argv[6],argv[7]);
	else
		usageExit(argv[0]);
	return 0;
}

