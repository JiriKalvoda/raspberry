#ifndef POSTHOOKS_H_INCLUDE
#define POSTHOOKS_H_INCLUDE

#include "deviceTree.h"
#include <mutex>

namespace Tree
{
	struct Hook
	{
		std::vector<const char *> neededMqtt;
		virtual int beforeRun()
		{
			return 1<<30;
		}
		virtual int afterRun()
		{
			return 1<<30;
		}
		virtual void printTeXDoc(FILE * f)
		{
		}
	};
	struct PostHook:public Hook
	{
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			return 1<<30;
		}
	};
	struct PreHook:public Hook
	{
		virtual int run(const DeviceReal * device,InputArr & data, void  * context)
		{
			return 1<<30;
		}
	};




}

#endif
