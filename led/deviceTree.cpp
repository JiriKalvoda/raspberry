#include "deviceTree.h"

#define CONSTRUCT(t, a...)  (new t(Abs(a)))
#define PART(a...) CONSTRUCT(Part, a)
#define DEV(a...)  CONSTRUCT(Device, a)
#define LINK(a...)  CONSTRUCT(Link, a)
#define DEV_VIRTUAL(a...)  CONSTRUCT(Device, a)
#define DEV_NEOPIXEL(a...)  CONSTRUCT(DeviceNeoPixel, a)
#define DEV_DMX(a...)  CONSTRUCT(DeviceDMX, a)
#define FOURKEY(a...) {Key({a}),Key({a})+1,Key({a})+2,Key({a})+3}
#ifdef USING_POST_HOOK
	#define PH(a...) ->addPostHook(a)
	#define PRH(a...) ->addPreHook(a)
	#define IFPH(a...) a
	#include "postHooks.cpp"
#else
	#define PH(a...) 
	#define PRH(a...) 
	#define IFPH(a...) 
#endif


namespace Tree
{
	const char * MQTTPREFIX = "led/";
	const char * MQTTMESSAGE = "led-message";

	IFPH(
	PostHookPowerLimit mainPower((30*4+25)*3*255);
	PostHookPowerLimit secondPower((15+16+45-13)*3*255);

#define JK_MAIL_DIRS \
			{  0,255,255,{"","instantmessaging"}}, \
			{  0,255,  0,{"seznam"}}, \
			{  0,  0,255,{"ksp-git","ksp"}}, \
			{255,  0,  0,{"*"}},
	PostHookMail postHookMail({JK_MAIL_DIRS});
	PostHookMail2 postHookMail2({JK_MAIL_DIRS});

	PostHookOSD postHookOSDArch("jk/osd/arch","jk/led-hook/osd/arch/disable");
	PostHookOSD postHookOSDArzen("jk/osd/arzen","jk/led-hook/osd/arzen/disable");
	PostHookOSD postHookOSDAcid("jk/osd/acid","jk/led-hook/osd/acid/disable");

	PostHookTimer postHookTimerArzen("jk/led-hook/timer/arzen");

	PostHookNightMode postHookNightMode("jk/led-hook/night/enable");

	PostHookSensor::Configuration postHookSensor_indor_temperature{"Vnitřní teplota",{
		{20,ColorFloat(1,1,1)},
		{21,ColorFloat(0.5,0.5,1)},
		{21.5,ColorFloat(0,0,1)},
		{22,ColorFloat(0,0.5,1)},
		{22.5,ColorFloat(0,1,1)},
		{23,ColorFloat(0,1,0.5)},
		{23.5,ColorFloat(0,1,0)},
		{24,ColorFloat(0.5,1,0)},
		{24.5,ColorFloat(1,1,0)},
		{25,ColorFloat(1,0.5,0)},
		{26,ColorFloat(1,0,0)},
		{27,ColorFloat(1,0.5,0.5)},
		{0,ColorFloat(1,1,1)},
	}};
	PostHookSensor::Configuration postHookSensor_outdor_temperature{"Venkovní teplota",{
		{-10,ColorFloat(1,1,1)},
		{0,ColorFloat(0.5,0.5,1)},
		{5,ColorFloat(0,0,1)},
		{10,ColorFloat(0,0.5,1)},
		{15,ColorFloat(0,1,1)},
		{17.5,ColorFloat(0,1,0.5)},
		{20,ColorFloat(0,1,0)},
		{22.5,ColorFloat(0.5,1,0)},
		{25,ColorFloat(1,1,0)},
		{27.5,ColorFloat(1,0.5,0)},
		{30,ColorFloat(1,0,0)},
		{35,ColorFloat(1,0.5,0.5)},
		{0,ColorFloat(1,1,1)},
	}};
	PostHookSensor::Configuration postHookSensor_humidity{"Vlhkost",{
		{20,ColorFloat(1,1,1)},
		{30,ColorFloat(1,1,0)},
		{40,ColorFloat(0,1,0)},
		{50,ColorFloat(0,1,0.5)},
		{60,ColorFloat(0,1,1)},
		{70,ColorFloat(0,0.5,1)},
		{100,ColorFloat(0,0,1)},
	}};
	)


	namespace mosquittos
	{
		Mosquitto rpi4   ("rpi4");
		Mosquitto blatto ("blatto");
	}

	namespace maschines
	{
		Maschine rpi7_DMX0("rpi7_DMX0", &mosquittos::blatto,-1,-1,-1,-1,"/dev/ttyAMA4");
		Maschine rpi9_DMX0("rpi9_DMX0", &mosquittos::blatto,-1,-1,-1,-1,"/dev/ttyS2");
		Maschine npi_chodba_DMX0("npi-chodba_DMX0", &mosquittos::blatto,-1,-1,-1,-1,"/dev/ttyS2");
		Maschine rpi4 ("rpi4", &mosquittos::rpi4);
	}

	namespace power_controls
	{
		PowerControl rpi7("rpi7", &mosquittos::blatto, 23);
	}


	Part *root = PART("")
	->add(PART("blatto")
		->add(PART("sp")
			->add(DEV_DMX("zachod")->set(&maschines::npi_chodba_DMX0)
				->add_channel(1,0.54,0.1 ,1,3)
				->set_rgbToChannel(DeviceReal::alwaysOn_rgbToChannel)
			)
			->add(DEV_DMX("koupelna")->set(&maschines::npi_chodba_DMX0)
				->add_channel(1,1,1 ,1,0)
				->set_rgbToChannel(DeviceReal::alwaysOn_rgbToChannel)
			)
			->add(DEV_DMX("kuchyn")->set(&maschines::npi_chodba_DMX0)
				->add_channel(1,0.54,0.1 ,1,2)
				->set_rgbToChannel(DeviceReal::alwaysOn_rgbToChannel)
			)
			->add(DEV_DMX("chodba")->set(&maschines::npi_chodba_DMX0)
				->add_channel(1,0.54,0.1 ,1,1)
				->set_rgbToChannel(DeviceReal::alwaysOn_rgbToChannel)
			)
		)
		->add(PART("1")
			->add(DEV_DMX("podpatro")->set(&maschines::rpi7_DMX0)
				->add_channel(1,0.65,0.31 ,1,1)
				->set_rgbToChannel(DeviceReal::oneChannel_rgbToChannel)
				PH(new PostHookPowerControl(&power_controls::rpi7))
			)
			->add(DEV_DMX("stul")->set(&maschines::rpi7_DMX0)
				->add_channel(1,0.54,0.1 ,1,3)
				->add_channel(1,1   ,1   ,1,2)
				->set_rgbToChannel(DeviceReal::linerarSpaceChannels_rgbToChannel)
				PH(new PostHookPowerControl(&power_controls::rpi7))
			)
			->add(DEV_DMX("liska_stul")->set(&maschines::rpi9_DMX0)
				->add_channel(1,0.65,0.31 ,1,0)
				->set_rgbToChannel(DeviceReal::oneChannel_rgbToChannel)
				PH(new PostHookPowerControl(&power_controls::rpi7))
			)
			->add(PART("okno")
				->add(DEV_DMX("leve")->set(&maschines::rpi9_DMX0)
					->add_channel(1,0.54,0.1 ,1,1)
					->add_channel(1,0.65,0.31 ,1,3)
					->add_channel(1,1,1 ,1,2)
					->set_rgbToChannel(DeviceReal::linerarSpaceChannels_rgbToChannel)
					PH(new PostHookPowerControl(&power_controls::rpi7))
				)
			)
		)
	)
	/*->add(
		PART("jug9")
		->add(DEV_VIRTUAL("",NULLKEY,{},{},{},NULLKEY,
				{
					new ActionVztFan({},&mosquittos::jug9, "vzt/fan/in"),
					new ActionVztFan({},&mosquittos::jug9, "vzt/fan/out"),
				}))
		->add(PART("1",{4,6,Key::SUPER},{},{},{},{4,6,Key::MOD(Key::SUPER|Key::CTRL)})
			->add(DEV_VIRTUAL("",{0,8},rowKeys(Key{0,1}, 4),{Key{0,5}},FOURKEY(0,9),{5,3},
					{
						new ActionKeyCmd({0,7},"movingssh -x jiri@rpi0 -Y \"alarm-stop && osdc 'Alarm stop'\""),
						(new ActionOnOffCmd({6,2,Key::SHIFT},{6,2,Key::CTRL},"movingssh -x rpi0 -Y \"rpi-backlight -p %s && (echo -n 'rpi backlight '; rpi-backlight --get-power) | osdc -\""))
							->setOnOffMsq("on","off")->setToggle({6,2}),
						(new ActionOnOffMqtt({6,1,Key::CTRL},{6,1,Key::SHIFT},&mosquittos::rpi0,"jk/razer/disable"))
							->setToggle({6,1}),
						new ActionOnOffMqtt({8,0}, {7,0},&mosquittos::jug9, "led-hook/wardrobe/1/disable"),
						new ActionOnOffMqtt({8,2}, {7,2},&mosquittos::jug9, "jk/led-hook/night/enable"),
						(new ActionOnOffCmd({9,0}, {10,1},"movingssh -x root@rpi0 -Y \" systemctl %s led-daemon led-daemon led-daemon-DMX && osdc 'LED %s'\""))
							->setOnOffMsq("start", "stop"),
						new ActionMotor(rowKeys(Key{1,8},4),&mosquittos::jug9, "motor/curtain/1/goto"),
					}))
			->add(DEV_NEOPIXEL("nadstul",{1,6},rowKeys(Key{1,1},4),{{1,5}},{})->set(&maschines::rpi5,2,31)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_begin_read)
					PH(&postHookTimerArzen)
					PH(&postHookMail,0,15)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_end)
			)
			->add(DEV_NEOPIXEL("stul",{2,6},rowKeys(Key{2,1},4),{{2,5}},{})->set(&maschines::rpi5,1,31)
				PH(new PostHookSet({0,0,0}),21,21)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_begin_read)
					PH(&postHookTimerArzen)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_end)
			)
			->add(DEV_NEOPIXEL("monitor",{2,12},rowKeys(Key{2,7},4),{{2,11}},{})->set(&maschines::rpi5,3,15)
				PH(new PostHookMqttInputMul("monitor"))
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_begin_read)
					PH(&postHookOSDArch)PH(&postHookOSDArzen)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_end)
			)
			->add(DEV_DMX("podpatro",{3,6},FOURKEY(3,1),{},rowKeys(Key{3,5},1))->set(&maschines::rpi0_DMX)
				->add_channel(1,0.65,0.31 ,1,3)
				->set_rgbToChannel(DeviceReal::oneChannel_rgbToChannel)
				PRH(new PreHookWardrobe({
						"door/1/wardrobe/0/0/open",
						"door/1/wardrobe/0/1/open",
						"door/1/wardrobe/1/open",
						"door/1/wardrobe/2/0/open",
						"door/1/wardrobe/2/1/open",
						},"led-hook/wardrobe/1/disable",0.66))
			)
			->add(DEV_DMX("okno",{4,11},rowKeys(Key{4,2},4),{Key{4,6}},{{4,7},{4,8},{4,9},{4,10},NULLKEY,NULLKEY,NULLKEY})->set(&maschines::rpi0_DMX)
				->add_channel(1,0.54,0.1 ,1,0)
				->add_channel(1,0.65,0.31 ,1,1)
				->add_channel(1,1,1 ,1,2)
				->set_rgbToChannel(DeviceReal::linerarSpaceChannels_rgbToChannel)
			)
			->add(PART("rgb", {3,11},{},{},rowKeys({3,7},4))->setControlAsDevice()
				->add(LINK("nadstul")->setPath("../nadstul"))
				->add(LINK("stul")   ->setPath("../stul"))
				->add(LINK("monitor")->setPath("../monitor"))
			 )
		)
	)*/
	->add(
		PART("pokojik",{4,6,Key::MOD(Key::SUPER|Key::SHIFT)},{},{},{},{4,6,Key::MOD(Key::SUPER|Key::SHIFT|Key::CTRL)})
		->add(DEV_VIRTUAL("",{0,8},FOURKEY(0,1),{Key{0,5}},FOURKEY(0,9),{5,3},
				{
					new ActionKeyCmd({0,7},"movingssh -x jiri@rpi4 -Y \"alarm-stop && osdc 'Alarm stop'\""),
					new ActionKeyCmd({6,2},"movingssh -x rpi4 -Y \"rpi-backlight -p toggle && (echo -n 'rpi backlight '; rpi-backlight --get-power) | osdc -\""),
					new ActionKeyCmd({6,2,Key::SHIFT},"movingssh -x rpi4 -Y \"rpi-backlight -p on && (echo -n 'rpi backlight '; rpi-backlight --get-power) | osdc -"),
					new ActionKeyCmd({6,2,Key::CTRL},"movingssh -x rpi4 -Y \"rpi-backlight -p off && (echo -n 'rpi backlight '; rpi-backlight --get-power) | osdc -\""),
					new ActionKeyCmd({6,1},"mqtt-pub rpi4 razer/disable toggle"),
					new ActionKeyCmd({6,1,Key::SHIFT},"mqtt-pub -r rpi4 razer/disable 0"),
					new ActionKeyCmd({6,1,Key::CTRL},"mqtt-pub -r rpi4 razer/disable 1"),
					new ActionKeyCmd({7,0},"mqtt-pub -r rpi4 jk/led-hook/mail/disable 0"),
					new ActionKeyCmd({8,0},"mqtt-pub -r rpi4 jk/led-hook/mail/disable 1"),
					new ActionKeyCmd({7,1},"mqtt-pub -r rpi4 jk/led-hook/osd/arch/disable 0"),
					new ActionKeyCmd({8,1},"mqtt-pub -r rpi4 jk/led-hook/osd/arch/disable 1"),
					new ActionKeyCmd({7,2},"mqtt-pub -r rpi4 jk/led-hook/night/enable 0"),
					new ActionKeyCmd({8,2},"mqtt-pub -r rpi4 jk/led-hook/night/enable 1"),
					new ActionKeyCmd({9,0},"movingssh -x root@rpi4 -Y \" systemctl start led-mosquitto led-daemon && osdc 'LED start'\""),
					new ActionKeyCmd({10,1},"movingssh -x root@rpi4 -Y \" systemctl stop led-mosquitto led-daemon && osdc 'LED stop'\""),
					new ActionKeyCmd({10,2},"movingssh -x root@rpi4 -Y \" systemctl start ultrasonic-pokojik && osdc 'Ultrasonic start'\""),
					new ActionKeyCmd({10,0},"movingssh -x root@rpi4 -Y \" systemctl stop ultrasonic-pokojik && osdc 'Ultrasonic stop'\""),
				}))
		->add(DEV_NEOPIXEL("stul"   ,{4,11},FOURKEY(4,2),{Key{4,6}},FOURKEY(4,7))->set(&maschines::rpi4,1,16,0)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_begin_read)
					PH(&postHookTimerArzen)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_end)
				PH(&secondPower)PH(&mainPower))
		->add(PART("postel")
			->add(DEV_VIRTUAL("",{2,7}))
			->add(DEV_NEOPIXEL("policka",{3,6},FOURKEY(3,1),{{3,5}},FOURKEY(3,7))->set(&maschines::rpi4,3,28,0)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_begin_read)
					PH(&postHookMail,27,15)PH(new PostHookSet({0,255,0}),0,0)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_end)
				PH(&mainPower))
			->add(DEV_NEOPIXEL("polstar",{2,6},FOURKEY(2,1),{{2,5}},FOURKEY(2,8))->set(&maschines::rpi4,4,16,1)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_begin_read)
					PH(&postHookOSDArch)PH(&postHookOSDArzen)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_end)
				PH(&mainPower))
		)
		->add(PART("strop")
			->add(DEV_VIRTUAL("",{1,8},FOURKEY(1,1),{{1,5}},FOURKEY(1,9)))
			->add(DEV_NEOPIXEL("policka",{1,6},FOURKEY(1,1,Key::SHIFT),{{1,5,Key::SHIFT}},FOURKEY(1,9,Key::SHIFT))->set(&maschines::rpi4,2,45,0)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_begin_read)
					PH(&postHookMail,0,30)
				PH(&postHookNightMode,0,-1,1,&PostHookNightMode_end)
				PH(&secondPower)PH(&mainPower))
			->add(DEV_NEOPIXEL("skrin"  ,{1,7},FOURKEY(1,1,Key::CTRL ),{{1,5,Key::CTRL}},FOURKEY(1,9,Key::CTRL ))->set(&maschines::rpi4,5,45,0)
				PH(&mainPower))
		)
	)
	->add(
		PART("tech")->guiHide()
		/*
		->add(
			PART("jug9")
			->add(
				PART("1")
				->add(DEV_NEOPIXEL("not")->set(&maschines::rpi5,0,36)
					PH(&postHookNightMode,0,-1,1,&PostHookNightMode_begin_read)
						PH(&postHookMail2,35,16)
						PH(new PostHookLowCostEnergy(),1,1)
						PH(new PostHookHeatingMotor("motor/heating/1/goto"),2,2)
						PH(new PostHookSensor("sensors/jug9/1/stul/temperature",postHookSensor_indor_temperature),3,3)
						PH(new PostHookSensor("sensors/jug9/1/patro/temperature",postHookSensor_indor_temperature),4,4)
						PH(new PostHookSensor("sensors/jug9/1/stul/humidity",postHookSensor_humidity),6,6)
						PH(new PostHookNextAlarm("alarm/jug9/1/next"),8,8)
					PH(&postHookNightMode,0,-1,1,&PostHookNightMode_end)
					PH(new PostHookScale(3,255))
				)
			)
		)*/
	)
	->calc();


	IFPH(
	std::map<std::string, std::vector<Hook*>> hooksForDoc =
	{
		{"jug9-1",{
			&postHookMail2,
			new PostHookLowCostEnergy(),
			new PostHookHeatingMotor(""),
			new PostHookSensor("", postHookSensor_indor_temperature),
			new PostHookSensor("", postHookSensor_outdor_temperature),
			new PostHookSensor("", postHookSensor_humidity),
			new PostHookNextAlarm(""),
		}},
	};)
}
