#include <string.h>
#include <MQTTClient.h>
#include <mutex>
#include <thread>
#include <functional>
#include <set>

#include "deviceTree.h"
#include "colors.h"

using Tree::FNAMELEN;
using Tree::FLEN;

using cch = const char;
using Tree::ld;

Tree::Mosquitto * myMosuitto;

#define DMQTT if(0)
#define D if(1)

#define fo(a,b) for(int a=0;a<(b);++a)

int ledMessage(char * in);

MQTTClient mqtt;
std::unordered_map<std::string,std::string> mqttMap;
std::mutex mqttMapMutex;

void mqttWrite(const char * fname, const char * data)
{
	{
		std::lock_guard<std::mutex> guard(mqttMapMutex);
		if(data[0]) mqttMap[fname]=data;
		else mqttMap.erase(fname);
	}
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;
	pubmsg.payload = (void *)data;
	pubmsg.payloadlen = strlen(data);
	pubmsg.qos = 0;
	pubmsg.retained = 1;
	MQTTClient_publishMessage(mqtt, fname, &pubmsg, &token);
}

void delivered(void *context, MQTTClient_deliveryToken dt)
{
}
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
	int i;
	DMQTT printf("Message arrived\n");
	DMQTT printf("     topic: %s\n", topicName);
	DMQTT printf("   message: ");
	char * payloadptr = (char *)message->payload;
	DMQTT for(i=0; i<message->payloadlen; i++)
	{
		putchar(payloadptr[i]);
	}
	DMQTT putchar('\n');
	char * in = new char [message->payloadlen+1];
	for(int i=0;i<message->payloadlen;i++)
		in[i]=payloadptr[i];
	in[message->payloadlen]=0;
	if(!strcmp(topicName,Tree::MQTTMESSAGE))
	{
		ledMessage(in);
	}
	else
	{
		std::string topic = std::string(topicName);
		{
			std::lock_guard<std::mutex> guard(mqttMapMutex);
			if(topic[0])
				mqttMap[topic]=in;
			else mqttMap.erase(topic);

		}
	}
	delete in;
	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}
void connlost(void *context, char *cause)
{
    printf("\nMQTT connection lost\n");
    printf("     cause: %s\n", cause);
}

void initMqtt()
{
	for(int i=0;;i++)
	{
		MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
		int rc;

		std::string addres = myMosuitto->getAddres();
		std::string username = myMosuitto->getUsername("led-mosquitto");
		std::string passwd = myMosuitto->getPasswd("led-mosquitto");

		MQTTClient_create(&mqtt, addres.c_str(), "led-mosquitto",
				MQTTCLIENT_PERSISTENCE_NONE, NULL);

		conn_opts.keepAliveInterval = 20;
		conn_opts.cleansession = 1;
		conn_opts.username = username.c_str();
		conn_opts.password = passwd.c_str();
		MQTTClient_setCallbacks(mqtt, NULL, connlost, msgarrvd, delivered);

		if ((rc = MQTTClient_connect(mqtt, &conn_opts)) != MQTTCLIENT_SUCCESS && i==10)
		{
			printf("Failed to connect MQTT, return code %d\n", rc);
			exit(-1);

		}
		if(rc == MQTTCLIENT_SUCCESS) break;
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
	printf("CONECT OK\n");
	fflush(stdout);
}

std::unordered_map<std::string,Tree::Abs *> byName;
void loadTree(Tree::Part * in)
{
	if(in->mosquitto.count(myMosuitto)==0) return;
	if(in->allName[0]==0) 
		if(!byName.count("_")) byName["_"]=in;
	if(!byName.count(in->allName)) byName[in->allName]=in;
	for(auto & it:in->content) if(it->name[0])
	{
		if(it->type & Tree::TYPE_Part)
			loadTree((Tree::Part*) it);
		else
			if(!byName.count(it->allName)) byName[it->allName]=it;
	}
}

void write(const char * a,const char *b,const char * data)
{
	if(!strcmp(data,"_")) return;
	char fname[FLEN];
	sprintf(fname,"%s%s/%s/%s",Tree::MQTTPREFIX,"master",a,b);
	mqttWrite(fname,data);
}

int gcd(int a, int b) { return b ? gcd(b, a%b) : a; }
using Tree::ld2d;
using ld1d = std::vector<ld>;
void expandShape1D(ld1d * a, int l)
{
	int shape=1;
	fo(i,l) shape = shape*a[i].size()/gcd(shape, a[i].size());
	fo(i,l)
	{
		ld1d b;
		if(!a[i].size()) {printf("EXPAND ERROR: size 0\n");exit(1);}
		fo(x,int(shape/a[i].size())) for(auto &it : a[i])  b.push_back(it);
		a[i] = b;
	}
}
void expandShape2D(ld2d * a, int l)
{
	int shape=1;
	fo(i,l) shape = shape*a[i].size()/gcd(shape, a[i].size());
	fo(i,l)
	{
		ld2d b;
		if(!a[i].size()) {printf("EXPAND ERROR: size 0\n");exit(1);}
		for(auto &it : a[i]) fo(x,int(shape/a[i].size())) b.push_back(it);
		a[i] = b;
	}
	fo(i,shape)
	{
		ld1d b[l];
		fo(j,l) b[j]=a[j][i];
		expandShape1D(b,l);
		fo(j,l) a[j][i]=b[j];
	}
}

void transform2D(ld2d *in, int in_len, ld2d *out, int out_len, std::function<void (double *, double*)> f)
{
	expandShape2D(in, in_len);
	fo(i, out_len) out[i]=in[0];
	fo(i, (int)out[0].size())
		fo(j, (int)out[0][i].size())
		{
			ld a_in[in_len];
			ld a_out[out_len];
			fo(k, in_len) a_in[k] = in[k][i][j];
			f(a_in, a_out);
			fo(k, out_len) out[k][i][j] = a_out[k];
		}
}
void doChToRgb(Tree::DeviceReal *dev)
{
	ld2d channels[dev->channels_len];
	for(int i=0;i<dev->channels_len;i++)
	{
		std::lock_guard<std::mutex> guard(mqttMapMutex);
		char fname[FNAMELEN];
		sprintf(fname,"%s%s/%s/ch%d",Tree::MQTTPREFIX,"master",dev->allName,i);
		if(!mqttMap.count(fname)) return;
		channels[i] = Tree::parse2D(mqttMap[fname].c_str());
	}
	ld2d rgb[3];
	transform2D(channels, dev->channels_len, rgb, 3, [dev](double *a, double *b){dev->channelToRgb(a,b);});
	fo(i, 3)
	{
		char out[FLEN];
		Tree::createStrFrom2D(rgb[i], out);
		char fileName[FNAMELEN];
		sprintf(fileName, "%c","rgb"[i]);
		write(dev->allName, fileName, out);
	}
}
void doRgbToCh(Tree::DeviceReal *dev)
{
	ld2d rgb[3];
	for(int i=0;i<3;i++)
	{
		std::lock_guard<std::mutex> guard(mqttMapMutex);
		char fname[FNAMELEN];
		sprintf(fname,"%s%s/%s/%c",Tree::MQTTPREFIX,"master",dev->allName,"rgb"[i]);
		if(!mqttMap.count(fname)) return;
		rgb[i] = Tree::parse2D(mqttMap[fname].c_str());
	}
	ld2d channels[dev->channels_len];
	transform2D(rgb, 3, channels, dev->channels_len, [dev](double *a, double *b){dev->rgbToChannel(a,b);});
	fo(i, dev->channels_len)
	{
		char out[FLEN];
		Tree::createStrFrom2D(channels[i], out);
		char fileName[FNAMELEN];
		sprintf(fileName, "ch%d",i);
		write(dev->allName, fileName, out);
	}
}

using NdSet = std::set<Tree::Abs*>;


#define CREATE_ABS_HEADER(func) void func(Tree::Abs *, NdSet & doneSet, func ## _PARAMS);

#define CREATE_PART(func) \
void func(Tree::Part * part, NdSet & doneSet, func ## _PARAMS) \
{ \
	if(part->mosquitto.count(myMosuitto)==0) return; \
	for(auto it : part->content) \
		func(it, doneSet,func ## _EXPAND); \
} 

#define CREATE_LINK(func) \
void func(Tree::Link * link, NdSet & doneSet, func ## _PARAMS) \
{ \
	if(link->mosquitto.count(myMosuitto)==0) return; \
	func(link->target, doneSet,func ## _EXPAND); \
} 

#define CREATE_ABS(func) \
void func(Tree::Abs * tree_a, NdSet & doneSet, func ## _PARAMS_NODEFAULT) \
{ \
	if(doneSet.count(tree_a)) return; \
	doneSet.insert(tree_a); \
	if(tree_a->type & Tree::TYPE_DeviceReal) func((Tree::DeviceReal*) tree_a, func ## _EXPAND); \
	if(tree_a->type & Tree::TYPE_Part)       func((Tree::Part*) tree_a,  doneSet, func ## _EXPAND); \
	if(tree_a->type & Tree::TYPE_Link)       func((Tree::Link*) tree_a,  doneSet,func ## _EXPAND); \
}

#define CREATE_STR(func) \
void func(cch * in,func ## _PARAMS) \
{ \
	NdSet doneSet;\
	char name[FLEN]; \
	for(int i=0,j=0;;i++,j++) \
	{ \
		if(in[i]==0||in[i]==' '||in[i]==',') \
		{ \
			name[j]=0; \
			Tree::Abs * tree_a = Tree::root->evalRelativePath(name); \
			if(tree_a) \
			{ \
				func(tree_a, doneSet, func ## _EXPAND); \
			} \
			j=-1; \
		} \
		else name[j]=in[i]; \
		if(in[i]==0) break; \
	} \
}

#define CREATE_BASE(func) CREATE_ABS_HEADER(func) CREATE_PART(func) CREATE_LINK(func) CREATE_ABS(func) CREATE_STR(func)

#define f_PARAMS           cch * light,cch * r="_",cch * g="_",cch * b="_",cch * lightSpeed="_",cch * colorSpeed="_"
#define f_PARAMS_NODEFAULT cch * light,cch * r    ,cch * g    ,cch * b    ,cch * lightSpeed    ,cch * colorSpeed
#define f_EXPAND                 light,      r,          g,          b,           lightSpeed,          colorSpeed

void f(Tree::Device * dev, f_PARAMS)
{
	if(dev->mosquitto.count(myMosuitto)==0) return;
	write(dev->allName,"lightSpeed",lightSpeed);
	write(dev->allName,"colorSpeed",colorSpeed);
	write(dev->allName,"r",r);
	write(dev->allName,"g",g);
	write(dev->allName,"b",b);
	write(dev->allName,"light",light);
	if(dev->type & Tree::TYPE_DeviceReal)
	{
		auto dev_real = (Tree::DeviceReal*) dev;
		if(strcmp(r,"_") || strcmp(g,"_") || strcmp(b,"_"))
			doRgbToCh(dev_real);
	}
}
CREATE_BASE(f)

#define getMaxLightBoost_PARAMS ld * out
#define getMaxLightBoost_PARAMS_NODEFAULT getMaxLightBoost_PARAMS
#define getMaxLightBoost_EXPAND    out
void getMaxLightBoost(Tree::Device * dev, getMaxLightBoost_PARAMS)
{
	*out = std::max(*out, dev->maxLightBoost);
}
CREATE_BASE(getMaxLightBoost)

#define channels_PARAMS           cch ** val,int val_len
#define channels_PARAMS_NODEFAULT cch ** val,int val_len
#define channels_EXPAND                 val,    val_len


void channels(Tree::Device * dev, channels_PARAMS)
{
	if(dev->mosquitto.count(myMosuitto)==0) return;
	if(dev->type & Tree::TYPE_DeviceReal)
	{
		auto dev_real = (Tree::DeviceReal*) dev;
		for(int ch=0;ch<val_len&&ch<dev_real->channels_len;ch++)
		{
			char fname[100];
			sprintf(fname, "ch%d", ch);
			write(dev->allName,fname, val[ch]);
		}
		doChToRgb(dev_real);
	}
}
CREATE_BASE(channels)


void light(cch * name, cch * val, int maxVal)
{
	int v = atoi(val);
	char buf[10];
	sprintf(buf,"%f",colorButtonLight(v,maxVal));
	f(name,buf,"_","_","_","_","_");
}
void lightBoost(cch * name, cch * val, int maxVal, double maxLightBoost)
{
	if(!maxVal) return;
	int v = atoi(val);
	if(maxLightBoost < 0)
	{
		maxLightBoost = 1;
		getMaxLightBoost(name, &maxLightBoost);
	}
	char buf[10];
	sprintf(buf,"%f",1+(maxLightBoost-1)*v/maxVal);
	f(name,buf,"_","_","_","_","_");
}
void lightBig(cch * name, cch * val)
{
	light(name, val, 11);
}
void colorSpeed(cch * name, cch * val)
{
	if(!strcmp(val,"0")) f(name,"_","_","_","_","_","0");
	if(!strcmp(val,"1")) f(name,"_","_","_","_","_","1");
	if(!strcmp(val,"2")) f(name,"_","_","_","_","_","0.5");
	if(!strcmp(val,"3")) f(name,"_","_","_","_","_","0.15");
}
void lightSpeed(cch * name, cch * val)
{
	if(!strcmp(val,"0")) f(name,"_","_","_","_","0","_");
	if(!strcmp(val,"1")) f(name,"_","_","_","_","2","_");
	if(!strcmp(val,"2")) f(name,"_","_","_","_","1","_");
	if(!strcmp(val,"3")) f(name,"_","_","_","_","0.3","_");
}
void colorDoubleRGB(cch * name, double rgb[3])
{
	char r[10],g[10],b[10];
	sprintf(r,"%f",rgb[0]);
	sprintf(g,"%f",rgb[1]);
	sprintf(b,"%f",rgb[2]);
	f(name,"_",r,g,b);
}
void colorRGB(cch * name, int rgb[3])
{
	char r[10],g[10],b[10];
	sprintf(r,"%f",rgb[0]/255.0);
	sprintf(g,"%f",rgb[1]/255.0);
	sprintf(b,"%f",rgb[2]/255.0);
	f(name,"_",r,g,b);
}
void color(cch * name, cch * val, int maxVal)
{
	ld rgb[3];
	colorButtonRedScale(atoi(val),maxVal, rgb);
	colorDoubleRGB(name,rgb);
}
void colorBig(cch * name, cch * val)
{
	Color c = colors(atoi(val));
	colorRGB(name,c.rgb);
}

int ledMessage(int argc,char ** argv)
{
	auto parsingError = []{D printf("PARSING ERROR\n");};
	D fo(i,argc) printf("'%s' ",argv[i]);
	D printf("\n");
	std::vector<std::pair<char *,char *>> vars={{argv[0],0}};
	for(int i=0;argv[0][i];i++)
	{
		if(argv[0][i]==' ')
		{
			argv[0][i]=0;
			vars.push_back({argv[0]+i+1,0});
		}
		if(argv[0][i]=='=' && !vars.rbegin()[0].second)
		{
			argv[0][i]=0;
			vars.rbegin()[0].second=argv[0]+i+1;
		}

	}

	if(!strcmp(vars[0].first,"led"))
	{
		/*for(auto it:vars)
		{
			// if(!strcmp(it.first,"LED_WIF") && it.second) sscanf(it.second,"%d",(int*)&wif);
			// if(!strcmp(it.first,"LED_SLOT") && it.second) slot=it.second;
		}*/

		if(argc-1>=3 && !strcmp(argv[2],"ch")) channels(argv[1],(cch**)argv+3,argc-3);
		else
		if((argc-1==3 || argc-1==4) && !strcmp(argv[2],"l")) light(argv[1],argv[3],argc-1>=4?atoi(argv[4]):4);
		else
		if((argc-1==5) && !strcmp(argv[2],"lb")) lightBoost(argv[1],argv[3],atoi(argv[4]), strtod(argv[5],NULL));
		else
		if((argc-1==3 || argc-1==4) && !strcmp(argv[2],"c")) color(argv[1],argv[3],argc-1>=4?atoi(argv[4]):4);
		else
		if(argc-1==3 && !strcmp(argv[2],"L")) lightBig(argv[1],argv[3]);
		else
		if(argc-1==3 && !strcmp(argv[2],"C")) colorBig(argv[1],argv[3]);
		else
		if(argc-1==3 && !strcmp(argv[2],"s")) lightSpeed(argv[1],argv[3]);
		else
		if(argc-1==3 && !strcmp(argv[2],"S")) colorSpeed(argv[1],argv[3]);
		else
		if(argc-1==2) f(argv[1],argv[2],"_","_","_","_","_");
		else
		if(argc-1==5) f(argv[1],argv[2],argv[3],argv[4],argv[5],"_","_");
		else
		if(argc-1==6) f(argv[1],argv[2],argv[3],argv[4],argv[5],argv[6],"_");
		else
		if(argc-1==7) f(argv[1],argv[2],argv[3],argv[4],argv[5],argv[6],argv[7]);
		else
			parsingError();
	}
	else
		parsingError();
	return 0;
}

int ledMessage(char * in)
{
	std::vector<char *> argv={in};
	for(int i=0;in[i];i++)
	{
		if(in[i]=='|')
		{
			in[i]=0;
			argv.push_back(in+i+1);
		}
	}
	return ledMessage(argv.size(),argv.data());
}

void usageExit(char * name)
{
#define PREPROC_XSTR(s) PREPROC_STR(s)
#define PREPROC_STR(s) #s
	printf(
			"This is program for applying MQTT commands.\n"
			"Start me as daemon by executing:\n"
			"\n"
			"	%s <device_name>\n"
			"\n"
			"\n"
			"Configuration file is located at %s/deviceTree.h (apply charges need recompilation)\n"
			,name,PREPROC_XSTR(BUILDPATH));
	exit(0);
#undef PREPROC_XSTR
#undef PREPROC_STR
}

int main(int argc,char ** argv)
{
	if(argc!=2) usageExit(argv[0]);
	myMosuitto = Tree::mosquitto_map[argv[1]];
	if(!myMosuitto)
	{
		printf("Mosquitto not found.\n");
		usageExit(argv[0]);
	}
	for(int i=1;i<argc;i++)
	{
		if(!strcmp(argv[i],"-h") || !strcmp(argv[i],"--help")) usageExit(argv[0]);
	}
	loadTree(Tree::root);
	initMqtt();
	char fname[FNAMELEN];
	sprintf(fname,"%s#",Tree::MQTTPREFIX);
	MQTTClient_subscribe(mqtt, fname, 1);
	MQTTClient_subscribe(mqtt, Tree::MQTTMESSAGE, 1);

	while(1)
	{
		using namespace std::this_thread; // sleep_for, sleep_until
		using namespace std::chrono; // nanoseconds, system_clock, seconds

		sleep_for(seconds(10));
	}
	return 0;
}
