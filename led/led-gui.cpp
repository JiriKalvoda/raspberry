#include<bits/stdc++.h>
#include <unistd.h>
#include <termios.h>
#include <sys/ioctl.h>
#include<sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <thread>
#include "screen.h"
#include "colors.h"
#include "led-withRead.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <semaphore.h> 
#include <poll.h> 
using namespace std;
#ifdef DEB
#define D if(1)
#else
#define D if(0)
#endif

LEDWithRead ledCls;

#define fo(a,b) for(int a=0;a<(b);++a)
using ll = long long;
using ld = double;
using cch = const char;

volatile ll stdinLen=0;
ll stdinByteRead=0;
int stdinpipe[2];
int inputThreadInstructionPipe[2];
// l - lock input
// u - unlock input

int termX=0,termY=0;


const int ESCLEN=100;
char escIn[ESCLEN];
int escInLen=0;
Box * rootBox;
Box * mouseBox;

sem_t drawSem;
bool disableDraw=0;
bool drawRedraw=1;
void redraw(int nope=0)
{
	drawRedraw=1;
	sem_post(&drawSem);
}

void stop(int s=0);

void led(cch * in,cch * light,cch * r="_",cch * g="_",cch * b="_",cch * lightSpeed="_",cch * colorSpeed="_")
{
	try
	{
		ledCls.f(in,light,r,g,b,lightSpeed,colorSpeed);
	}
	catch (const runtime_error& error)
	{
		resetTermios();
		throw error;
	}
}

void ledChannel(cch * in, cch * val, int index)
{
	try
	{
		const char * val_arr[index+1];
		fo(i, index) val_arr[i]=0;
		val_arr[index]=val;
		ledCls.channels(in,val_arr, index+1);
	}
	catch (const runtime_error& error)
	{
		resetTermios();
		throw error;
	}
}

void mqttMessage(Tree::Mosquitto * mosquitto, const char * data, const char * topic=Tree::MQTTMESSAGE, bool retained=0)
{
	try
	{
		ledCls.mqttMessage(mosquitto, data, topic, retained);
	}
	catch (const runtime_error& error)
	{
		resetTermios();
		throw error;
	}
}

void systemCleanTerm(const char * cmd)
{
	write(inputThreadInstructionPipe[1],"l",1);

	disableDraw=1;
	//loadScreenSize();
	resetTermios();

	printf("$ %s\n", cmd);

	system(cmd);

	disableDraw=0;
	write(inputThreadInstructionPipe[1],"u",1);
	initTermios(0);
	redraw();
}



struct BoxDevice:public Box
{
	Tree::Abs * device;
	LEDWithRead::Read * ledRead=0;
	BoxDevice(Tree::Abs *d)
	{
		device=d;
		if(device) if(ledCls.readByName.count(device->allName)) ledRead=ledCls.readByName[device->allName];
		//fprintf(stderr,"%s -> %s | %s\n",device->allName,ledRead->dev?ledRead->dev->allName:"---",ledRead->part?ledRead->part->allName:"---");
	}
	int ledReadDataVersion=-1;
	virtual SizePrior minH()
	{
		SizePrior r;
		r[1]+=1;
		return r;
	}
	virtual SizePrior minW()
	{
		SizePrior r;
		r[1]+=1;
		return r;
	}
	virtual void drawSoft()
	{
		if(!ledRead) draw();
		else
		if(ledRead->dataVersion != ledReadDataVersion)
		{
			ledReadDataVersion = ledRead->dataVersion;
			draw();
		}
	}
};
struct BoxBar:public BoxDevice
{
	ld scaleBegin=0;
	ld scaleZoom=1;
	BoxBar(Tree::Abs *d):BoxDevice(d){}
	virtual void scaleToRgb(ld scale,int rgb[3])=0;
	virtual void click(ld scale)=0;
	const char * textColor="";
	virtual SizePrior minW()
	{
		SizePrior r;
		r[1]+=10;
		return r;
	}
	virtual int marginLen()
	{
		return w/10/scaleZoom;
	}
	virtual ld pozToScale(ld Y)
	{
		Y/=scaleZoom;
		Y+=w*scaleBegin;
		return min(ld(1),max(ld(0),ld(Y-marginLen())/(w-2*marginLen())));
	}
	virtual void readLedFunc(int * read)=0;
	virtual const char* getName(){ return device->allName;}
	virtual void draw()
	{
		const char * name = getName();
		int nameLen=strlen(name);
		int nameStart = w/2-nameLen/2;
		printf("%s",textColor);
		int read[3];
		readLedFunc(read);
		int minReadRgbDiff=1<<30;
		for(int j=0;j<w;j++)
		{
			int rgb[3];
			ld val = pozToScale(j);
			scaleToRgb(val,rgb);
			int readRgbDiff=0;
			fo(i,3) readRgbDiff += abs(read[i]-rgb[i]);
			minReadRgbDiff=std::min(minReadRgbDiff,readRgbDiff);
		}
		for(int i=0;i<h;i++)
		{
			printf("\e[%d;%dH",x+i,y);
			for(int j=0;j<w;j++)
			{
				int rgb[3];
				ld val = pozToScale(j);
				scaleToRgb(val,rgb);
				char ch=' ';
				if(i==h/2 && nameStart<=j && j<nameStart+nameLen)
					ch=name[j-nameStart];
				int readRgbDiff=0;
				fo(i,3) readRgbDiff += abs(read[i]-rgb[i]);
				if(readRgbDiff<=minReadRgbDiff+3 && readRgbDiff<10) ch='#';
				if(val==0 || val==1) rgb[0]=rgb[1]=rgb[2]=128;
				printf("\e[48;2;%d;%d;%dm%c",rgb[0],rgb[1],rgb[2],ch);
			}
		}
		printf("\e[0m");
	}
	virtual void mouse(int t,int X,int Y)
	{
		if(t>=32 && mouseBox!=this) return;
		mouseBox=this;
		ld val = pozToScale(Y-y);
		click(val);
	}
};
struct BoxLightBar:public BoxBar
{
	BoxLightBar(Tree::Abs *d):BoxBar(d){textColor="\e[31m";}
	virtual void scaleToRgb(ld scale,int rgb[3])
	{
		rgb[0]=rgb[1]=rgb[2]=255*scale;
	}
	virtual void click(ld scale)
	{
		char out[100];
		sprintf(out,"%lF",scale);
		led(device->allName,out);
	}
	virtual void readLedFunc(int * read)
	{
		read[0]=read[1]=read[2]=ledRead->lightNum*255;
	}
};
struct BoxLightBoostBar:public BoxBar
{
	char name[20];
	ld maxLightBoost;
	BoxLightBoostBar(Tree::Abs *d):BoxBar(d){textColor="\e[31m";
		maxLightBoost = device->name[0]?device->maxLightBoost: device->up?device->up->maxLightBoost: 1; // HACK
		sprintf(name, "BOOST (%.2lf)", maxLightBoost);
	}
	virtual const char* getName(){ return name;}
	virtual void scaleToRgb(ld scale,int rgb[3])
	{
		rgb[0]=rgb[1]=rgb[2]=255*scale;
	}
	virtual void click(ld scale)
	{
		char out[100];
		sprintf(out,"%lF",1+(scale*(maxLightBoost-1)));
		led(device->allName,out);
	}
	virtual void readLedFunc(int * read)
	{
		read[0]=read[1]=read[2]=(ledRead->lightNum-1)/(maxLightBoost-1)*255;
	}
};
struct BoxChannelBar:public BoxBar
{
	Tree::DeviceReal *devReal;
	int channel_id;
	Tree::Channel *channel;
	BoxChannelBar(Tree::DeviceReal *d, int id):BoxBar(d),devReal(d),channel_id(id),channel(d->channels[id]){textColor="\e[39m";}
	virtual void scaleToRgb(ld scale,int rgb[3])
	{
		fo(i,3) rgb[i]=255*scale*channel->rgb[i];
	}
	virtual void click(ld scale)
	{
		char out[100];
		sprintf(out,"%lF",scale);
		ledChannel(device->allName,out, channel_id);
	}
	virtual void readLedFunc(int * read)
	{
		fo(i,3) read[i]=255*ledRead->channelsNum[channel_id]*channel->rgb[i];
	}
};
struct BoxRgbBar:public BoxBar
{
	int rgb_id;
	BoxRgbBar(Tree::Abs *d, int id):BoxBar(d),rgb_id(id){textColor="\e[39m";}
	virtual void scaleToRgb(ld scale,int rgb[3])
	{
		fo(i,3) rgb[i]=i==rgb_id?255*scale:0;
	}
	virtual void click(ld scale)
	{
		char out[100];
		sprintf(out,"%lF",scale);
		led(device->allName,"_"
				,rgb_id==0?out:"_"
				,rgb_id==1?out:"_"
				,rgb_id==2?out:"_"
				);
	}
	virtual void readLedFunc(int * read)
	{
		fo(i,3) read[i]=i==rgb_id?255* (*ledRead->byIdNum[1+rgb_id]) :0;
	}
};
struct BoxColorBar:public BoxBar
{
	BoxColorBar(Tree::Abs *d):BoxBar(d){textColor="\e[34m";}
	virtual void scaleToRgb(ld scale,int rgb[3])
	{
		colorRedScale(scale,rgb);
	}
	virtual void click(ld scale)
	{
		int rgb[3];
		scaleToRgb(scale,rgb);
		char r[10],g[10],b[10];
		sprintf(r,"%f",rgb[0]/255.0);
		sprintf(g,"%f",rgb[1]/255.0);
		sprintf(b,"%f",rgb[2]/255.0);
		led(device->allName,"_",r,g,b);
	}
	virtual void readLedFunc(int * read)
	{
		read[0]=ledRead->rNum*255;
		read[1]=ledRead->gNum*255;
		read[2]=ledRead->bNum*255;
	}
};
struct BoxColorBarWhiteYellow:public BoxColorBar
{
	BoxColorBarWhiteYellow(Tree::Abs *d):BoxColorBar(d){textColor="\e[34m";}
	virtual void scaleToRgb(ld scale,int rgb[3])
	{
		colorRedScaleWhiteYellow(scale,rgb);
	}
};
struct BoxColorBarYellowRed:public BoxColorBar
{
	BoxColorBarYellowRed(Tree::Abs *d):BoxColorBar(d){textColor="\e[34m";}
	virtual void scaleToRgb(ld scale,int rgb[3])
	{
		colorRedScaleYellowRed(scale,rgb);
	}
};
struct BoxRGBColor:public BoxDevice
{
	BoxRGBColor(Tree::Abs *d):BoxDevice(d){}
	virtual SizePrior minH()
	{
		SizePrior r;
		r[1]+=1;
		r[2]+=5;
		r[3]+=5;
		r[4]+=5;
		r[5]+=5;
		return r;
	}
	virtual SizePrior minW()
	{
		SizePrior r;
		r[1]+=1;
		r[2]+=5;
		r[3]+=5;
		r[4]+=5;
		r[5]+=5;
		return r;
	}
	virtual void scaleToRgbPart(ld a,ld b,int *x,int * y,int * z)
	{
		*x=255*(1-abs(a));
		*y=255*(1-abs(b));
		*z=255;
	}
	virtual void scaleToRgb(ld X,ld Y,int rgb[3])
	{
		X=min(ld(1),max(ld(-1),2.2*(X-0.5)));
		Y=min(ld(1),max(ld(-1),2.2*(Y-0.5)));
		if(X<=0)
		{
			if(Y<=0)
				scaleToRgbPart(X,Y,rgb+1,rgb+2,rgb+0);
			else
			{ rgb[0]=rgb[2]=255;rgb[1]=255*max(ld(0),1-max(abs(X),abs(Y)));}
		}
		else
		{
			if(Y<=0)
				scaleToRgbPart(X,Y,rgb+0,rgb+2,rgb+1);
			else
				scaleToRgbPart(X,Y,rgb+0,rgb+1,rgb+2);
		}
	}
	virtual void printFilter(int rgb[3])
	{
	}
	virtual void draw()
	{
		int minReadRgbDiff=1<<30;
		int read[3];
		read[0]=ledRead->rNum*255;
		read[1]=ledRead->gNum*255;
		read[2]=ledRead->bNum*255;
		for(int i=0;i<h;i++)
		{
			for(int j=0;j<w;j++)
			{
				int rgb[3];
				scaleToRgb(ld(i)/h,ld(j)/w,rgb);
				int readRgbDiff=0;
				fo(i,3) readRgbDiff += abs(read[i]-rgb[i]);
				minReadRgbDiff=std::min(minReadRgbDiff,readRgbDiff);
			}
		}
		for(int i=0;i<h;i++)
		{
			printf("\e[%d;%dH\e[30m",x+i,y);
			for(int j=0;j<w;j++)
			{
				int rgb[3];
				scaleToRgb(ld(i)/h,ld(j)/w,rgb);
				int rgbPrint[3];
				fo(i,3) rgbPrint[i]=rgb[i];
				printFilter(rgbPrint);
				char ch=' ';
				int readRgbDiff=0;
				fo(i,3) readRgbDiff += abs(read[i]-rgb[i]);
				if(readRgbDiff<=minReadRgbDiff+15 && readRgbDiff<20) ch='#';
				fo(i,3) if(rgbPrint[i]<0) rgbPrint[i]=0, ch='L';
				fo(i,3) if(rgbPrint[i]>255) rgbPrint[i]=255, ch='H';
				if(!rgb[0] || !rgb[1] || !rgb[2] ) rgbPrint[0]=rgbPrint[1]=rgbPrint[2]=128;
				printf("\e[48;2;%d;%d;%dm%c",rgbPrint[0],rgbPrint[1],rgbPrint[2],ch);
			}
		}
		printf("\e[0m");
	}
	virtual void mouse(int t,int X,int Y)
	{
		if(t>=32 && mouseBox!=this) return;
		mouseBox=this;
		int rgb[3];
		scaleToRgb(ld(X-x)/h,ld(Y-y)/w,rgb);
		char r[10],g[10],b[10];
		sprintf(r,"%f",rgb[0]/255.0);
		sprintf(g,"%f",rgb[1]/255.0);
		sprintf(b,"%f",rgb[2]/255.0);
		led(device->allName,"_",r,g,b);
	}
};

struct BoxRGBChannelsSumColor: public BoxRGBColor
{
	Tree::DeviceReal *dev_real;
	int channelsMask;
	BoxRGBChannelsSumColor(Tree::DeviceReal *DEV, int MASK):
		BoxRGBColor(DEV), dev_real(DEV), channelsMask(MASK)
	{

	}
	virtual void printFilter(int rgb[3])
	{
		ld rgb_ld[3];
		ld channels[dev_real->channels_len];
		fo(i,3) rgb_ld[i]=rgb[i]/255.0;
		dev_real->rgbToChannel(rgb_ld, channels);
		fo(i, dev_real->channels_len) if(!((channelsMask>>i)&1)) channels[i]=0;
		fo(i,3) rgb_ld[i]=0;
		dev_real->channelToRgb(channels, rgb_ld);
		fo(i,3) rgb[i]=rgb_ld[i]*255;
	}
};

struct BoxListColor:public BoxDevice
{
	BoxListColor(Tree::Abs *d):BoxDevice(d){}
	virtual SizePrior minH()
	{
		SizePrior r;
		r[1]+=1;
		r[2]+=5;
		r[3]+=5;
		r[4]+=5;
		return r;
	}
	virtual SizePrior minW()
	{
		SizePrior r;
		r[1]+=1;
		r[2]+=5;
		r[3]+=5;
		r[4]+=5;
		return r;
	}
	virtual void scaleToRgb(ld X,ld Y,int rgb[3])
	{
		int a=8;
		int b=(colorsLen-1)/a+1;
		int c=b*int(X*a)+int(Y*b);
		if(c<colorsLen)
			for(int i=0;i<3;i++) rgb[i]=colors(c).rgb[i];
		else rgb[0]=rgb[1]=rgb[2]=255;
		
	}
	virtual void draw()
	{
		int read[3];
		read[0]=ledRead->rNum*255;
		read[1]=ledRead->gNum*255;
		read[2]=ledRead->bNum*255;
		for(int i=0;i<h;i++)
		{
			printf("\e[%d;%dH\e[30m",x+i,y);
			for(int j=0;j<w;j++)
			{
				int rgb[3];
				scaleToRgb(ld(i)/h,ld(j)/w,rgb);
				char ch=' ';
				int readRgbDiff=0;
				fo(i,3) readRgbDiff += abs(read[i]-rgb[i]);
				if(readRgbDiff<=3) ch='#';
				printf("\e[48;2;%d;%d;%dm%c",rgb[0],rgb[1],rgb[2],ch);
			}
		}
		printf("\e[0m");
	}
	virtual void mouse(int t,int X,int Y)
	{
		if(t>=32 && mouseBox!=this) return;
		mouseBox=this;
		int rgb[3];
		scaleToRgb(ld(X-x)/h,ld(Y-y)/w,rgb);
		char r[10],g[10],b[10];
		sprintf(r,"%f",rgb[0]/255.0);
		sprintf(g,"%f",rgb[1]/255.0);
		sprintf(b,"%f",rgb[2]/255.0);
		led(device->allName,"_",r,g,b);
	}
};
struct BoxDeviceSpeed:public BoxDevice
{
	const char *val;
	const char *name;
	virtual SizePrior minH()
	{
		SizePrior r;
		r.r+=1;
		r[5]+=1;
		return r;
	}
	virtual SizePrior minW()
	{
		return minH();
	}
	virtual ld ledReadFunc(){return 0;}
	BoxDeviceSpeed(Tree::Abs *d,const char * Val):BoxDevice(d),val(Val)
	{
	}
	virtual void mouse(int t,int X,int Y)=0;
	virtual void draw()
	{
		int nameLen=strlen(val);
		int nameStart = w/2-nameLen/2;
		ld valNum=-10;
		ld read = ledReadFunc();
		sscanf(val,"%lF",&valNum);
		if(valNum<=read+0.01 && valNum>=read-0.01)
		{
			printf("\e[42m");
		}
		else
		{
			printf("\e[49m");
		}
		for(int i=0;i<h;i++)
		{
			printf("\e[%d;%dH",x+i,y);
			for(int j=0;j<w;j++)
			{
				char ch=' ';
				if(i==h/2 && nameStart<=j && j<nameStart+nameLen)
					ch=val[j-nameStart];
				putchar(ch);
			}
		}
		printf("\e[0m");

	};

};
struct BoxDeviceColorSpeed:public BoxDeviceSpeed
{
	virtual ld ledReadFunc(){return ledRead->colorSpeedNum;}
	BoxDeviceColorSpeed(Tree::Abs *d,const char * Val):BoxDeviceSpeed(d,Val){}
	virtual void mouse(int t,int X,int Y)
	{
		led(device->allName,"_","_","_","_","_",val);
	}
};
struct BoxDeviceLightSpeed:public BoxDeviceSpeed
{
	virtual ld ledReadFunc(){return ledRead->lightSpeedNum;}
	BoxDeviceLightSpeed(Tree::Abs *d,const char * Val):BoxDeviceSpeed(d,Val){}
	virtual void mouse(int t,int X,int Y)
	{
		led(device->allName,"_","_","_","_",val,"_");
	}
};
struct BoxDeviceText:public BoxDevice
{
	const int FLEN=10000;
	int parNum;
	BoxDeviceText(Tree::Abs *d,int ParNum):BoxDevice(d),parNum(ParNum){}
	virtual SizePrior minH()
	{
		SizePrior r;
		r[1]+=1;
		return r;
	}
	virtual SizePrior minW()
	{
		SizePrior r;
		r[1]+=1;
		return r;
	}
	virtual void draw()
	{
		printf("\e[0m");
		const char * str=ledRead->byId[parNum];
		for(int i=0;i<h;i++)
		{
			printf("\e[%d;%dH",x+i,y);
			for(int j=0;j<w;j++)
			{
				if(*str)
				{
					if(*str>=' ' && *str<128)
						printf("%c",*str);
					else
						printf("\e[31m?\e[0m");
					str++;
				}
				else
					printf(" ");
			}
		}
		printf("\e[0m");
	}
	virtual void mouse(int t,int X,int Y)
	{
		char tmpFName[Tree::FNAMELEN]="/tmp/led-gui-XXXXXX";
#ifdef OS_PHONE
		sprintf(tmpFName,"%s/tmp-led-gui-XXXXXXX",getenv("HOME"));
#endif

		int tmpf = mkstemp(tmpFName);
		//printf("TMP%sPTM\n",tmpFName);
		const char* p[6];
		char in[FLEN];
		char sys[Tree::FNAMELEN];
		{
			FILE * f = fopen(tmpFName,"w");
			if(!f) exit(100);
			fprintf(f,"%s",ledRead->byId[parNum]);
			fclose(f);
		}
		sprintf(sys,"vim %s",tmpFName);

		systemCleanTerm(sys);

		int f = open(tmpFName,O_RDONLY);
		if(f>=0)
		{
			int r = read(f,in,FLEN-1);
			in[r]=0;
			close(f);
			int numOfValidChar=0;
			for(int i=0;in[i];i++) if(in[i]==10) in[i]=0;
			for(int i=0;in[i];i++) numOfValidChar+=in[i]>' ';
			if(parNum<6)
			{
				for(int i=0;i<6;i++)
					p[i]= i==parNum?in:"_";
				if(numOfValidChar)
					led(device->allName,p[0],p[1],p[2],p[3],p[4],p[5]);
			}
			else
			{
				ledChannel(device->allName, in, parNum-6);
			}
		}
		close(tmpf);
		unlink(tmpFName);
	}
};
struct BoxSetRoot:public BBox
{
	Box * rootToSet;
	BoxSetRoot(const char * str,Box * b):BBox(str),rootToSet(b){}
	virtual void mouse(int t,int X,int Y)
	{
		if(t<32)
		{
		rootBox->hide();
		rootBox=rootToSet;
		redraw();
		}
	}
};

struct BoxSmoothMqtt:public BoxBar
{
	Tree::ActionSmoothMqtt *action;
	ld shownVal=-100;
	virtual const char* getName(){ return action->topic;}
	ld getVal()
	{
		auto data = ledCls.readTopic(action->mosquitto, action->topic);
		if(data==0 || data[0]==0) return -1;
		return atof(data);
	}
	virtual void drawSoft()
	{
		if(shownVal != getVal())
			draw();
	}
	BoxSmoothMqtt(Tree::ActionSmoothMqtt *m):BoxBar(0),action(m)
	{
		textColor="\e[31m";
		ledCls.subscribe(action->mosquitto, action->topic);
	}
	virtual void scaleToRgb(ld scale,int rgb[3])
	{
		rgb[2]=255*scale;
		rgb[0]=rgb[1]=rgb[2]/2;
	}
	virtual void click(ld scale)
	{
		char val[10];
		sprintf(val,"%f",scale);
		mqttMessage(action->mosquitto, val, action->topic, 1);
	}
	virtual void readLedFunc(int * read)
	{
		shownVal = getVal();
		scaleToRgb(shownVal, read);
	}
};

struct MqttBBox:public BBox
{
	Tree::Mosquitto *mosquitto;
	const char * topic;
	const char * val;
	bool show;
	bool retained;
	MqttBBox(const char * NAME, Tree::Mosquitto *MOSQUITTO, const char * TOPIC, const char * VAL, bool SHOW, bool RETAINED):
		BBox(NAME),mosquitto(MOSQUITTO),topic(TOPIC),val(VAL),show(SHOW),retained(RETAINED)
	{
		if(show) ledCls.subscribe(mosquitto, topic);
	}
	virtual void mouse(int t,int X,int Y)
	{
		mqttMessage(mosquitto, val, topic, retained);
	}
	virtual char getEmptyCh()
	{
		if(!show) return ' ';
		auto data = ledCls.readTopic(mosquitto, topic);
		return (data&&!strcmp(data,val))?'#':' ';
	}
	virtual void drawSoft()
	{
		draw();
	}
};

Box * initRgbBarsBox(Tree::Abs * p)
{
	VBox * v = new VBox;
	fo(i, 3)
	{
		auto cbar = new BoxRgbBar(p, i);
		v->in.push_back(cbar);
	}
	return v;
}
Box * initChannelsBarBox(Tree::DeviceReal * p)
{
	VBox * v = new VBox;
	fo(i, p->channels_len)
	{
		auto cbar = new BoxChannelBar(p, i);
		v->in.push_back(cbar);
	}
	return v;
}
Box * initChannelsTextsBox(Tree::DeviceReal * p)
{
	HBox * text = new HBox;
	for(int i=0;i<p->channels_len;i++)
		text->in.push_back(new BoxDeviceText(p,6+i));
	return text;
}
Box * initBasicTextsBox(Tree::Abs * p)
{
	HBox * text = new HBox;
	for(int i=0;i<6;i++)
		text->in.push_back(new BoxDeviceText(p,i));
	return text;
}
Box * initLightBarsBox(Tree::Abs *p)
{
	BoxLightBar * l1 = new BoxLightBar(p);
	BoxLightBar * l2 = new BoxLightBar(p);
	VBox * l = new VBox;
	l1->scaleZoom=2;
	l2->scaleZoom=2;
	l2->scaleBegin=0.5;
	l->in.push_back(l1);
	l->in.push_back(l2);
	{
		BoxLightBoostBar * l3 = new BoxLightBoostBar(p);
		l->in.push_back(l3);
	}
	return l;
}
Box * initRedshiftBarsBox(Tree::Abs *p)
{
	BoxColorBar * c1 = new BoxColorBarWhiteYellow(p);
	BoxColorBar * c2 = new BoxColorBarYellowRed(p);
	VBox * c = new VBox;
	c->in.push_back(c1);
	c->in.push_back(c2);
	return c;
}

Box * initSpeedsButtonsBox(Tree::Abs *p)
{
	HBox * h = new HBox(0);

	HBox * lightSpeed=new HBox;
	h->in.push_back(lightSpeed);
	for(const char * it : {"0","6","2","1","0.3"})
		lightSpeed->in.push_back(new BoxDeviceLightSpeed(p,it));
	HBox * colorSpeed=new HBox;
	h->in.push_back(colorSpeed);
	for(const char * it : {"0","3","1","0.5","0.15","0.03"})
		colorSpeed->in.push_back(new BoxDeviceColorSpeed(p,it));
	return h;
}

Box * initMoreBox(Tree::Abs * p,Box * root)
{
	VBox * a = new VBox;
	VBox * b = new VBox;
	VBox * c = new VBox;
	VBox * d = new VBox;
	a->border=0;
	b->border=0;
	c->border=0;
	d->border=0;
	HBox * menu = new HBox;

	// MENU
	{
		menu->border=0;
		if(root) menu->in.push_back(new BoxSetRoot("Back",root));
		else     menu->in.push_back(new BBox("Back",[](int,int,int){stop();}));
		BoxSetRoot *A = new BoxSetRoot("Main control",a);
		BoxSetRoot *B = new BoxSetRoot("RGB",b);
		BoxSetRoot *C = new BoxSetRoot("Colors",c);
		BoxSetRoot *D_ = new BoxSetRoot("Channels RGB",d);

		A->color="\e[45;97m";
		B->color="\e[44;97m";
		C->color="\e[45;97m";
		D_->color="\e[44;97m";

		menu->in.push_back(A);
		menu->in.push_back(B);
		menu->in.push_back(C);
		if(p->type & Tree::TYPE_DeviceReal)
			menu->in.push_back(D_);

		a->in.push_back(menu);
		b->in.push_back(menu);
		c->in.push_back(menu);
		d->in.push_back(menu);
	}

	// Main
	{
		a->in.push_back(initLightBarsBox(p));
		a->in.push_back(initRedshiftBarsBox(p));
		if(p->type & Tree::TYPE_DeviceReal)
			a->in.push_back(initChannelsBarBox((Tree::DeviceReal*)p));
		else
			a->in.push_back(initRgbBarsBox((Tree::DeviceReal*)p));
		a->in.push_back(initSpeedsButtonsBox(p));
		if(p->type & Tree::TYPE_DeviceReal)
			a->in.push_back(initChannelsTextsBox((Tree::DeviceReal*)p));
		a->in.push_back(initBasicTextsBox(p));
	}

	// RGB
	{
		b->in.push_back(initLightBarsBox(p));
		HBox * rgb = new HBox();
		b->in.push_back(rgb);
		rgb->in.push_back(new BoxRGBColor(p));
	}

	// Colors
	{
		c->in.push_back(initLightBarsBox(p));
		HBox * list=new HBox;
		c->in.push_back(list);
		list->in.push_back(new BoxListColor(p));
	}

	// Channels RGB
	if(p->type & Tree::TYPE_DeviceReal)
	{
		auto p_real = (Tree::DeviceReal*)p;
		int counts = p_real->channels_len+1;
		for(int i=0;i<counts;)
		{
			HBox * row=new HBox(0);
			d->in.push_back(row);
			for(int j=0;j<2&&i<counts;i++,j++)
			{
				HBox * rgb = new HBox();
				row->in.push_back(rgb);
				if(i==p_real->channels_len)
					rgb->in.push_back(new BoxRGBChannelsSumColor(p_real,-1));
				else
					rgb->in.push_back(new BoxRGBChannelsSumColor(p_real, 1<<i));
			}
		}
	}

	return a;
}

Box * initActionsBox(Tree::Abs * p,Box * root)
{
	VBox * a = new VBox;
	a->border=0;
	HBox * menu = new HBox;

	// MENU
	{
		menu->border=0;
		if(root) menu->in.push_back(new BoxSetRoot("Back",root));
		else     menu->in.push_back(new BBox("Back",[](int,int,int){stop();}));
	}

	VBox * b = new VBox;
	a->in.push_back(menu);
	a->in.push_back(b);
	for(Tree::Action* it: p->actions)
	{
		if(it->type & Tree::TYPE_ActionKeyCmd)
		{
			auto action = (Tree::ActionKeyCmd *)it;
			BBox *buton = new  BBox(action->cmd,[=](int,int,int){
							systemCleanTerm(action->cmd);
						});
			buton->color="\e[45m";
			b->in.push_back(buton);
		}
		if(it->type & Tree::TYPE_ActionOnOffMqtt)
		{
			auto action = (Tree::ActionOnOffMqtt *)it;
			HBox* hbox = new HBox(0);
			MqttBBox *butonOff = new  MqttBBox("OFF", action->mosquitto, action->topic, action->off, 1, 1);
			butonOff->color="\e[45m";
			hbox->in.push_back(butonOff);
			if(action->toggle)
			{
				MqttBBox *butonToggle = new  MqttBBox(action->topic, action->mosquitto, action->topic, action->toggle, 0, 0);
				butonToggle->color="\e[45m";
				hbox->in.push_back(butonToggle);
			}
			else
			{
				BBox *butonNope = new BBox(action->topic);
				butonNope->color="\e[40m";
				hbox->in.push_back(butonNope);
			}
			MqttBBox *butonOn = new  MqttBBox("ON", action->mosquitto, action->topic, action->on, 1, 1);
			butonOn->color="\e[45m";
			hbox->in.push_back(butonOn);
			b->in.push_back(hbox);
		}
		if(it->type & Tree::TYPE_ActionOnOffCmd)
		{
			auto action = (Tree::ActionOnOffCmd*)it;
			char * off_cmd = action->getCmd(action->off);
			HBox* hbox = new HBox(0);
			BBox *butonOff = new  BBox("OFF",[=](int,int,int){systemCleanTerm(off_cmd);});
			butonOff->color="\e[45m";
			hbox->in.push_back(butonOff);
			if(action->toggle)
			{
				char * toggle_cmd = action->getCmd(action->toggle);
				BBox *butonToggle = new  BBox(action->cmd,[=](int,int,int){systemCleanTerm(toggle_cmd);});
				butonToggle->color="\e[45m";
				hbox->in.push_back(butonToggle);
			}
			else
			{
				BBox *butonNope = new BBox(action->cmd);
				butonNope->color="\e[40m";
				hbox->in.push_back(butonNope);
			}
			char * on_cmd = action->getCmd(action->on);
			BBox *butonOn = new  BBox("ON",[=](int,int,int){systemCleanTerm(on_cmd);});
			butonOn->color="\e[45m";
			hbox->in.push_back(butonOn);
			b->in.push_back(hbox);
		}
		if(it->type & Tree::TYPE_ActionSmoothMqtt)
		{
			auto action = (Tree::ActionSmoothMqtt *)it;
			BoxSmoothMqtt *box = new  BoxSmoothMqtt(action);
			b->in.push_back(box);
		}
	}
	return a;
}

Box * initRootBox(Tree::Part * p,Box * root=0);
void initBox(Box * b,Tree::Part * p,Box * root=0)
{
	for(auto it_no_delink:p->content)
	{
		Tree::Abs* it = it_no_delink;
		while(it->type & Tree::TYPE_Link) it = ((Tree::Link *)it)->target;
		if(it->type & Tree::TYPE_Device || it->controlAsDevice) 
		{
			if(it->guiHidden) continue;
			HBox * a = new HBox(0);
			a->in.push_back(new BoxLightBar (it));
			a->in.push_back(new NoBox(SizePrior(),SizePrior()(0,3)));
			if(it->actions.size())
			{
				BBox *act = new BoxSetRoot("ACT",initActionsBox(it,root));
				act->color="\e[45m";
				a->in.push_back(act);
				a->in.push_back(new NoBox(SizePrior(),SizePrior()(0,3)));
			}
			a->in.push_back(new BoxSetRoot("MORE",initMoreBox(it,root)));
			b->in.push_back(a);
		}
		else
		if(it->type & Tree::TYPE_Part)
		{
			if(it->guiHidden) continue;
			if(!it->key.null)
			{
				Box * a = new BoxSetRoot(it->allName,initRootBox((Tree::Part *)it,root));
				b->in.push_back(a);
			}
			else
			{
				VBox * a = new VBox;
				b->in.push_back(a);
				initBox(a,(Tree::Part*) it,root);
			}
		}
	}
}
Box * initRootBox(Tree::Part * p,Box * root)
{
	VBox * a = new VBox;
	HBox * h = new HBox;
	a->border=0;
	h->border=0;
	if(root) h->in.push_back(new BoxSetRoot("Back",root));
	else     h->in.push_back(new BBox("Back",[](int,int,int){stop();}));
	a->in.push_back(h);
	VBox * d = new VBox;
	d->in.push_back(new BoxColorBar(p));
	a->in.push_back(d);
	VBox * c = new VBox;
	a->in.push_back(c);
	initBox(c,p,a);
	return a;
}
void init(const char * name)
{
	if(ledCls.partByName.count(name))
	{
		Tree::Part   * part = ledCls.partByName[name];
		ledCls.loadReadTree(part);
		ledCls.connectTree(part,1);
		rootBox=initRootBox(part,0);
	}
	else
	if(ledCls.deviceByName.count(name))
	{
		Tree::Device * dev = ledCls.deviceByName[name];
		ledCls.loadReadTree(dev);
		ledCls.connectTree(dev,1);
		rootBox=initMoreBox(dev,0);
	}
	else
	{
		printf("Devidce/part %s not exist.\n\nAvailable devices and parts:\n",name);
		for(const char * it : ledCls.allNames)
			printf("\t%s\n",it);
		exit(1);
	}
}
int mouseX=-1,mouseY=-1;
void mouseUp(int t,int x,int y)
{
	mouseX=x;mouseY=y;
	//printf("U %d %d %d\n",x,y,t);
}
void mouseDown(int t,int x,int y)
{
	mouseBox=0;
	//printf("D %d %d %d\n",t,x,y);
	mouseX=x;mouseY=y;
	rootBox->mouse(t,x,y);
}
void mouseMove(int t,int x,int y)
{
	if(x==mouseX&&y==mouseY) return;
	//printf("m %d %d %d\n",x,y,t);
	mouseX=x;mouseY=y;
}
void mouseMoveDown(int t,int x,int y)
{
	//if(x==mouseX&&y==mouseY) return;
	//printf("M %d %d %d\n",x,y,t);
	//printf("%lld %lld %lld\n",stdinLen,stdinByteRead,stdinLen-stdinByteRead);
	if(stdinLen!=stdinByteRead) return;
	mouseX=x;mouseY=y;
	rootBox->mouse(64|t,x,y);
}

void drawThread()
{
	while(true)
	{
		sem_wait(&drawSem);
		int semval;
		sem_getvalue(&drawSem,&semval);
		for(;semval>0;semval--) sem_wait(&drawSem);
		if(disableDraw) continue;
		if(termX && termY)
		{
			if(drawRedraw)
			{
				drawRedraw=0;
				printf("\e[2J\e[H");
				rootBox->calcPlace(1,1,termX,termY);
				rootBox->draw();
				fflush(stdout);
			}
			else
			{
				rootBox->drawSoft();
				fflush(stdout);
			}
		}
	}
}

void inputThread()
{
	char buf [12345];
	bool lockInput=false;
	bool readAll=true;
	while(true)
	{
		struct pollfd fds[2];
		fds[0].fd = 0;
		fds[0].events = lockInput?0:POLLIN;
		fds[1].fd = inputThreadInstructionPipe[0];
		fds[1].events = POLLIN;
		int ret = poll(fds, sizeof(fds)/sizeof(fds[0]), readAll?0:-1);
		if (ret > 0) {
			if(fds[0].revents & POLLIN)
			{
				int l=read(0,buf,12340);
				if(!readAll)
				{
					write(stdinpipe[1],buf,l);
					stdinLen+=l;
				}
			}
			if(fds[1].revents & POLLIN)
			{
				char in='-';
				read(inputThreadInstructionPipe[0],&in,1);
				if(in=='l') lockInput=true;
				if(in=='u') lockInput=false, readAll=true;
			}
		}
		else readAll=false;
	}
}

void stop(int s)
{
	resetTermios();
	sem_destroy(&drawSem);
	exit(1);
}

int main(int argc, char **argv)
{
	sem_init(&drawSem, 0, 1);
	ledCls.loadTree(Tree::root);
	for(int i=1;i<argc;i++)
	{
		if(!strcmp(argv[i],"-h") || !strcmp(argv[i],"--help"))
		{
			printf("%s: Graphic access to LED strips configuration\n\n"
					"USAGE:\n"
					"\t%s [PART/DEVICE - Default: \"_\" = <root>]\n\n"
					"Available devices and parts:\n"
					,argv[0],argv[0]);
			for(const char * it : ledCls.allNames)
				printf("\t%s\n",it);
			exit(0);
		}
	}
	init(argc>1?argv[1]:"");
	signal (SIGINT,stop);
	signal(SIGWINCH, loadScreenSize);
    initTermios(0);
	//printf("\e[?1000h");
	//printf("\e[?1000;1003;1006;1015h");
	pipe(inputThreadInstructionPipe);
	pipe(stdinpipe);
	FILE * stdinpipef=fdopen(stdinpipe[0],"r");
	std::thread(inputThread).detach();
	std::thread(drawThread).detach();
	ledCls.everyNew.push_back([]{sem_post(&drawSem);});
	while(1)
	{
		char ch = getc(stdinpipef);
		stdinByteRead++;
		if(ch==27)
		{
			if(escInLen)
			{
				break;
			}
			escIn[(escInLen=0)++]=ch;
		}
		else if(escInLen && escInLen<ESCLEN-1)
		{
			//ioctl(0, FIONBIO, &stdInBytesToRead);
			escIn[escInLen++]=ch;
			if(('a'<=ch&&ch<='z') || ('A'<=ch&&ch<='Z'))
			{
				escIn[escInLen]=0;
				int a,b,c;
				//printf("%s\n",escIn+1);
				switch(ch)
				{
					case 'R':
						if(sscanf(escIn,"\e[%d;%dR",&a,&b)==2)
						{
							static bool forFirst=1;
							if(termX!=a || termY!=b)
							{
								termX=a;
								termY=b;
								if(forFirst) printf("\e[%dS",termX);
								forFirst=0;
								redraw();
							}
						}
						break;
					case 'M':
					case 'm':
						if(sscanf(escIn,"\e[<%d;%d;%d",&a,&c,&b)==3)
						{
							if(ch=='M' && 0<=a&&a<=3) mouseDown(a,b,c);
							if(ch=='m' && 0<=a&&a<=3) mouseUp(a,b,c);
							if(ch=='M' && a==35) mouseMove(0,b,c);
							if(ch=='M' && a==32) mouseMoveDown(0,b,c);

						}
						if(sscanf(escIn,"\e[%d;%d;%d",&a,&c,&b)==3)
						{
							if(32<=a&&a<=34) mouseDown(a-32,b,c);
							if(35<=a&&a<=37) mouseUp(a-35,b,c);
							if(67==a) mouseMove(0,b,c);
							if(64<=a && a<=66) mouseMoveDown(0,b,c);
						}
						break;

				}
				escInLen=0;
			}
		}
		else
		{
			//if(ch=='`') 
				//printf("\e[999;999H\e[6n\e[H"); // print end screen coordinate
			//if(ch>32||ch==10)
				//printf("%c",ch);
			//else 
				//printf("<%d>",int(ch));
			fflush(stdout);
		}
	}
	resetTermios();
	fflush(stdout);
	sem_destroy(&drawSem);
	return 0;
}
