#include "screen.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>

using namespace std;

void loadScreenSize(int nope)
{
	(void)nope;
	printf("\e[9999;9999H\e[6n\e[H"); // print end screen coordinate
	fflush(stdout);
}
static struct termios old, new1;
void initTermios(int echo)
{
    tcgetattr(0, &old); /* grab old terminal i/o settings */
    new1 = old; /* make new settings same as old settings */
    new1.c_lflag &= ~ICANON; /* disable buffered i/o */
    new1.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
    tcsetattr(0, TCSANOW, &new1); /* use these new terminal i/o settings now */
	printf("\e[?1000h\e[?1003h\e[?1015h\e[?1006h"); // using mouse
	printf("\e[?25l"); // disable cursor
	loadScreenSize();
}

void resetTermios()
{
	printf("\e[?1000l\e[?1003l\e[?1015l\e[?1006l");
	printf("\e[9999;9999H\n"); // goto end
	printf("\e[?25h"); // enable cursor
	fflush(stdout);
    tcsetattr(0, TCSANOW, &old);
}

	SizePrior::SizePrior(){for(int i=0;i<SIZEPRIORLEN;i++) a[i]=0;}
	int & SizePrior::operator [] (int i) {return a[i];}
	SizePrior SizePrior::operator () (int i,int v) {a[i]+=v;return *this;}
	void SizePrior::operator += (SizePrior d) {for(int i=0;i<SIZEPRIORLEN;i++) a[i]+=d[i];r+=d.r;}
	void SizePrior::operator *= (SizePrior d)
	{
		r=max(r,d.r);
		a[0]=max(a[0],d[0]);
		int x=0,y=0;
		for(int i=1;i<SIZEPRIORLEN;i++)
		{
			while(x && a[i]) {x--;a[i]--;}
			while(y && d[i]) {y--;d[i]--;}
			if(a[i]<d[i]) {x+=d[i]-a[i];a[i]=d[i];}
			if(d[i]<a[i]) {y+=a[i]-d[i];}
		}
	}
	pair<SizePrior,SizePrior> SizePrior::toSize(int size)
	{
		SizePrior x,y;
		x[0]=1;
		size-=a[0];
		while(size>0)
		{
			int lsize=size;
			for(int i=1;i<SIZEPRIORLEN;i++)
				if(size>=a[i])
				{x[i]++;size-=a[i];}
				else
				{y[i]+=size;size=0;}
			if(lsize<=size) break;
		}
		return {x,y};
	}
	int SizePrior::eval(pair<SizePrior,SizePrior> &in)
	{
		int r=0;
		for(int i=0;i<SIZEPRIORLEN;i++)
		{
			r+=in.first[i]*a[i];
			int x=min(a[i],in.second[i]);
			r+=x;
			in.second[i]-=x;
		}
		return r;
	}

	SizePrior Box::minH()
	{
		return SizePrior();
	}
	SizePrior Box::minW()
	{
		return SizePrior();
	}
	void Box::calcPlace(int X,int Y,int H,int W)
	{
		x=X;y=Y;w=W;h=H;
	}
	void Box::drawSoft(){};
	void Box::mouse(int t,int X,int Y){}
	void Box::hide(){}

	HVBox::HVBox(bool Border):border(Border){}
	void HVBox::drawOutBox()
	{
		if(!border) return;
		int W=w,Y=y;
		int w=W-2;
		int y=Y+1;
		printf("\e[0m");
		for(int i=0;i<h;i++)
		{
			printf("\e[%d;%dH",x+i,y);
			if(i==0)    for(int j=0;j<w;j++) printf(j!=w-1?!j?"┌":"─":"┐");
			else
			if(i==h-1)  for(int j=0;j<w;j++) printf(j!=w-1?!j?"└":"─":"┘");
			else
				printf("│\e[%d;%dH│",x+i,y+w-1);
		}
	}
	void HVBox::draw()
	{
		drawBox();
		for(Box * it:in) it->draw();
	}
	void HVBox::drawSoft()
	{
		for(Box * it:in) it->drawSoft();
	}
	void HVBox::mouse(int t,int X,int Y)
	{
		int opt=1<<30;
		Box * optB=0;
		for(Box * it:in)
		{
			int v=0;
			if(!it->mouseMode) continue;
			if(X<it->x) v+=it->x-X;
			if(Y<it->y) v+=it->y-Y;
			if(X>it->x+it->h-1) v+=X-(it->x+it->h-1);
			if(Y>it->y+it->w-1) v+=Y-(it->y+it->w-1);
			if(v<opt) {opt=v;optB=it;}
		}
		if(optB && (!opt || optB->mouseMode==2)) optB->mouse(t,X,Y);
	}

	VBox::VBox(bool Border):HVBox(Border){}
	void VBox::drawBox()
	{
		drawOutBox();
		if(!border) return;
		int W=w,Y=y;
		int w=W-2;
		int y=Y+1;
		printf("\e[0m");
		for(Box * it:in) if(it->x>x+1)
		{
			printf("\e[%d;%dH",it->x-1,y);
			for(int j=0;j<w;j++) printf(j!=w-1?!j?"├":"─":"┤");
		}
	}
	SizePrior VBox::minH()
	{
		SizePrior r;
		r[0]+=0;
		for(Box * it:in) r+=it->minH();
		if(border) r[0]+=1+in.size();
		return r;
	}
	SizePrior VBox::minW()
	{
		SizePrior r;
		if(border) r[0]+=6;
		for(Box * it:in) r*=it->minW();
		return r;
	}
	void VBox::calcPlace(int X,int Y,int H,int W)
	{
		x=X;y=Y;w=W;h=H;
		auto haloc=minH().toSize(h);
		for(Box * it:in)
		{
			if(border) X+=1;
			it->calcPlace(X,Y+border*3,it->minH().eval(haloc),W-border*6);
			X+=it->h;
		}
	}

	HBox::HBox(bool Border):HVBox(Border){}
	void HBox::drawBox()
	{
		drawOutBox();
		if(!border) return;
		int W=w,Y=y;
		int w=W-2;
		int y=Y+1;
		printf("\e[0m");
		(void)w;
		for(Box * it:in) if(it->y>y+3)
		{
			printf("\e[%d;%dH",it->x-1,y);
			for(int j=0;j<h;j++)
				printf("\e[%d;%dH%s",x+j,it->y-3,j!=h-1?!j?"─┬─":" │ ":"─┴─");
		}
	}
	SizePrior HBox::minW()
	{
		SizePrior r;
		for(Box * it:in) r+=it->minW();
		if(border) r[0]+=3*(1+in.size());
		return r;
	}
	SizePrior HBox::minH()
	{
		SizePrior r;
		for(Box * it:in) r*=it->minH();
		if(border) r[0]+=2;
		return r;
	}
	void HBox::calcPlace(int X,int Y,int H,int W)
	{
		x=X;y=Y;w=W;h=H;
		auto waloc=minW().toSize(w);
		for(Box * it:in)
		{
			if(border) Y+=3;
			it->calcPlace(X+border,Y,H-border*2,it->minW().eval(waloc));
			Y+=it->w;
		}
	}

	BBox::BBox(const char * Name,std::function<void(int,int,int)> F,
			SizePrior MinHSP,SizePrior MinWSP):
		name(Name),minHSP(MinHSP),minWSP(MinWSP),f(F){}
	char BBox::getEmptyCh(){return ' ';}
	void BBox::draw()
	{
		char emptyCh=getEmptyCh();
		int nameLen=strlen(name);
		int nameStart = w/2-(nameLen+1)/2;
		printf("%s",color);
		for(int i=0;i<h;i++)
		{
			printf("\e[%d;%dH",x+i,y);
			for(int j=0;j<w;j++)
			{
				char ch=emptyCh;
				if(i==h/2 && nameStart<=j && j<nameStart+nameLen)
					ch=name[j-nameStart];
				putchar(ch);
			}
		}
		printf("\e[0m");
	}
	SizePrior BBox::minH()
	{
		return minHSP;
	}
	SizePrior BBox::minW()
	{
		return minWSP;
	}
	void BBox::mouse(int t,int X,int Y)
	{
		if(t<32)
			f(t,x,y);
	}

	NoBox::NoBox(SizePrior MinHSP,SizePrior MinWSP):BBox("",[](int,int,int){},MinHSP,MinWSP)
		{minHSP=MinHSP;minWSP=MinWSP;mouseMode=0;}
	void NoBox::draw()
	{
	}
	void NoBox::mouse(int t,int X,int Y)
	{
	}
