#include <string.h>
#include <stdexcept>
#include <random>

#include <unordered_map>


#define fo(a,b) for(int a=0;a<(b);++a)

#include "led-include.h"

using cch = const char;

MQTTClient LED::connect(Mqtt * mqtt,bool cleansession,bool NoErr)
{
	int rc;
	for(int i=0;i<2;i++)
	{
		MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;

		std::string addres = mqtt->tree->getAddres();
		std::string username = mqtt->tree->getUsername("led-include");
		std::string passwd = mqtt->tree->getPasswd("led-include");

		MQTTClient client  = 0;
		 std::random_device rd;
		if(!mqttConName[0]) sprintf(mqttConName,"led-include-%d-%d",int(rd()),int(rd()));
		MQTTClient_create(&client ,addres.c_str(), mqttConName,
				MQTTCLIENT_PERSISTENCE_NONE, NULL);

		conn_opts.keepAliveInterval = 10;
		conn_opts.cleansession = cleansession;
		conn_opts.username = username.c_str();
		conn_opts.password = passwd.c_str();

		if ((rc = MQTTClient_connect(client , &conn_opts)) == MQTTCLIENT_SUCCESS)
			return client ;
		MQTTClient_destroy(&client );
	}
	printf("Failed to connect MQTT, return code %d\n", rc);
	throw std::runtime_error("MQTT Connect error");
}
void LED::connectAndWriteToMap(Tree::Mosquitto * mosquitto,bool reconnect, bool NoErr)
{
	//fprintf(stderr,"CAWTM\n");
	if(!mqttMap.count(mosquitto) || !mqttMap[mosquitto]->connected || reconnect)
	{
		if(!mqttMap.count(mosquitto)) mqttMap[mosquitto] = new Mqtt;
		Mqtt * mqtt = mqttMap[mosquitto];
		mqtt->led = this;
		mqtt->tree = mosquitto;
		if(mqtt->runnungConnect)
		{
			if(!NoErr)
			{
				printf("Failed to connect MQTT, lock is on\n");
				throw std::runtime_error("MQTT Connect error lock is on");
			}
			return ;
		}
		auto r = connect(mqtt,!reconnect,NoErr);
		if(!r) return;
		mqtt->client = r;
		mqtt->connected = 1;



	}
}
void LED::mqttMessage(Tree::Mosquitto * mosquitto, const char * data, const char * topic, bool retained)
{
	int rc;
	for(int i=0;;i++)
	{
		for(int i=0;i<2;i++)
		{
			connectAndWriteToMap(mosquitto);
			MQTTClient mqtt = mqttMap[mosquitto]->client;
			MQTTClient_message pubmsg = MQTTClient_message_initializer;
			MQTTClient_deliveryToken token;
			pubmsg.payload = (void *)data;
			pubmsg.payloadlen = strlen(data);
			pubmsg.qos = retained?1:2;
			pubmsg.retained = retained;
			MQTTClient_publishMessage(mqtt, topic, &pubmsg, &token);
			rc = MQTTClient_waitForCompletion(mqtt, token, TIMEOUT);
			if(rc == MQTTCLIENT_SUCCESS) return;
		}
		if(i==1)
			throw std::runtime_error("MQTT Message faild");
		connectAndWriteToMap(mosquitto,1);
	}
}
void LED::mqttMessage(std::vector<Tree::Abs*> dev , const char * data)
{
	std::unordered_set<Tree::Mosquitto *> mosquitto;
	for(auto it: dev) for(auto jt: it->mosquitto)
		mosquitto.insert(jt);
	for(auto it: mosquitto)
		mqttMessage(it,data);
}
void LED::mqttMessage(cch * in , const char * data)
{
	char name[Tree::FNAMELEN];
	std::vector<Tree::Abs*> dev;
	for(int i=0,j=0;;i++,j++)
	{
		if(in[i]==0||in[i]==' '||in[i]==',')
		{
			name[j]=0;
			Tree::Abs * a = Tree::root->evalRelativePath(name);
			if(a)
				dev.push_back(a);
			else
				throw std::runtime_error("Device not exists");
			j=-1;
		}
		else name[j]=in[i];
		if(in[i]==0) break;
	}
	mqttMessage(dev,data);
}
void LED::light(cch * name, cch * val, int maxVal)
{
	char out[Tree::FLEN];
	sprintf(out,"led|%s|l|%s|%d",name,val, maxVal);
	mqttMessage(name,out);
}
void LED::lightBoost(cch * name, cch * val, int maxVal, double maxBoost)
{
	char out[Tree::FLEN];
	sprintf(out,"led|%s|lb|%s|%d|%lf",name,val, maxVal, maxBoost);
	mqttMessage(name,out);
}
void LED::lightBig(cch * name, cch * val)
{
	char out[Tree::FLEN];
	sprintf(out,"led|%s|L|%s",name,val);
	mqttMessage(name,out);
}
void LED::color(cch * name, cch * val, int maxVal)
{
	char out[Tree::FLEN];
	sprintf(out,"led|%s|c|%s|%d",name,val, maxVal);
	mqttMessage(name,out);
}
void LED::colorBig(cch * name, cch * val)
{
	char out[Tree::FLEN];
	sprintf(out,"led|%s|C|%s",name,val);
	mqttMessage(name,out);
}
void LED::colorSpeed(cch * name, cch * val)
{
	char out[Tree::FLEN];
	sprintf(out,"led|%s|S|%s",name,val);
	mqttMessage(name,out);
}
void LED::lightSpeed(cch * name, cch * val)
{
	char out[Tree::FLEN];
	sprintf(out,"led|%s|s|%s",name,val);
	mqttMessage(name,out);
}

void LED::f(cch * name,cch * light,cch * r,cch * g,cch * b,cch * lightSpeed,cch * colorSpeed)
{
	char out[Tree::FLEN];
	sprintf(out,"led|%s|%s|%s|%s|%s|%s|%s",name,light,r,g,b,lightSpeed,colorSpeed);
	mqttMessage(name,out);
}
void LED::channels(cch * name, cch ** val, int val_len)
{
	char out[Tree::FLEN];
	char * out_ptr = out;
	out_ptr += sprintf(out_ptr,"led|%s|ch",name);
	for(int i=0;i<val_len;i++) 
		out_ptr += sprintf(out_ptr,"|%s",val[i]?val[i]:"_");
	mqttMessage(name,out);
}



void LED::loadTree(Tree::Part * in)
{
	if(in->allName[0]==0) 
	{
		if(!partByName.count("_")) partByName["_"]=in;
		allNames.push_back("_");
	}
	{
		if(!partByName.count(in->allName)) partByName[in->allName]=in;
		allNames.push_back(in->allName);
	}
	for(auto & it:in->content) if(it->name[0])
	{
		if(it->type & Tree::TYPE_Device)
		{
			Tree::Device * it_d = (Tree::Device*) it;
			if(!deviceByName.count(it_d->allName)) deviceByName[it_d->allName]=it_d;
			allNames.push_back(it_d->allName);
		}
		if(it->type & Tree::TYPE_Part)
		{
			loadTree((Tree::Part*)it);
		}
	}
}
