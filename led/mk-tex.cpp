#include "deviceTree.h"
#include "colors.h"
#include<bits/stdc++.h>
using namespace std;
#ifdef DEB
#define D if(1)
#else
#define D if(0)
#endif


#define fo(a,b) for(int a=0;a<(b);++a)
using ll = long long;
const int STRLEN = 100000;
const char MODEPREFIX [] = "led-";

const char * modeSufix_set="-SET";

void bind(Tree::Key key)
{
	printf("\\BindKey{");
	char out[1234];
	key.texPrint(out);
	printf("%s",out);
	printf("}{");

}
void endBind()
{
	printf("}\n");
}

void bindCmd(Tree::Key key, const char * cmd)
{
#ifdef ADD_RPI_BACKLIGHT
	if(key == Tree::Key{6,2}) continue;
#endif
	bind(key);
	printf("\\Cmd{");

	for(int i=0;cmd[i];i++)
		printf(cmd[i]=='\\'?"\\\\":cmd[i]=='{'?"\\{":cmd[i]=='}'?"\\}":"%c",cmd[i]);
	printf("}");
	endBind();
}
void bindMqtt(Tree::Key key, Tree::Mosquitto *mosquitto, const char * topic, const char * payload, bool retained)
{
#ifdef ADD_RPI_BACKLIGHT
	if(key == Tree::Key{6,2}) continue;
#endif
	bind(key);
	printf("\\%s{%s}{%s}{%s}", retained?"MqttPubRetained":"MqttPub", mosquitto->name, topic, payload);
	endBind();
}

char actPath[Tree::FNAMELEN];

void fByType(Tree::Abs * p);
void fByType_as_device(Tree::Abs * d);
void fByType_as_part(Tree::Part   * p);
void f(Tree::Abs * p)
{
	const char * modeSufix =  ((p->type & Tree::TYPE_Device) || p->controlAsDevice)?modeSufix_set : "";
	int old_actPath_len = strlen(actPath);
	sprintf(actPath+old_actPath_len, (actPath[0]&&p->name[0])?"/%s":"%s", p->name);
	for(Tree::Action* it : p->actions)
	{
		if(it->type & Tree::TYPE_ActionKeyCmd)
		{
			auto action = (Tree::ActionKeyCmd*)it;
			bindCmd(action->key, action->cmd);
		}
		if(it->type & Tree::TYPE_ActionKeyMqtt)
		{
			auto action = (Tree::ActionKeyMqtt*)it;
			bindMqtt(action->key, action->mosquitto, action->topic, action->payload, action->retained);
		}
		if(it->type & Tree::TYPE_ActionOnOffCmd)
		{
			auto action = (Tree::ActionOnOffCmd*)it;
			char * cmd;
			cmd = action->getCmd(action->on);
			bindCmd(action->onKey, cmd);
			delete [] cmd;
			cmd = action->getCmd(action->off);
			bindCmd(action->offKey, cmd);
			delete [] cmd;
			if(action->toggle)
			{
				cmd = action->getCmd(action->toggle);
				bindCmd(action->toggleKey, cmd);
				delete [] cmd;
			}
		}
		if(it->type & Tree::TYPE_ActionOnOffMqtt)
		{
			auto action = (Tree::ActionOnOffMqtt*)it;
			bindMqtt(action->onKey, action->mosquitto, action->topic, action->on, 1);
			bindMqtt(action->offKey, action->mosquitto, action->topic, action->off, 1);
			if(action->toggle) bindMqtt(action->toggleKey, action->mosquitto, action->topic, action->toggle, 1);
		}
		if(it->type & Tree::TYPE_ActionSmoothMqtt)
		{
			auto action = (Tree::ActionSmoothMqtt*)it;
			int l = (int)action->keys.size();
			fo(i,l)
			{
				char val[20];
				sprintf(val, "%f", float(i)/(l-1));
				bindMqtt(action->keys[i], action->mosquitto, action->topic, val, 1);
			}
		}
	}
	for(int i=0;i<(int)p->colorKey.size();i++)
		if(!p->colorKey[i].null)
		{
			bind(p->colorKey[i]);
			printf("\\LedColor{%s}{%d}{%d}",actPath,i, (int)p->colorKey.size()-1);
			endBind();
		}
	for(int i=0;i<(int)p->lightKey.size();i++)
		if(!p->lightKey[i].null)
		{
			bind(p->lightKey[i]);
			printf("\\LedLight{%s}{%d}{%d}",actPath,i, (int)p->lightKey.size()-1);
			endBind();
		}
	for(int i=0;i<(int)p->lightBoostKey.size();i++)
		if(!p->lightBoostKey[i].null)
		{
			bind(p->lightBoostKey[i]);
			printf("\\LedLightBoost{%s}{%d}{%d}{%lf}",actPath,i+1, (int)p->lightBoostKey.size(), p->maxLightBoost);
			endBind();
		}
	if(!p->invertKey.null)
	{
		bind( p->invertKey);
		printf("\\LedOpSwap{%s}{_}{second}",actPath);
		endBind();
	}
	if(!p->key.null)
	{
		bind(p->key);
		printf("\\GoMode{%s%s%s}",MODEPREFIX,actPath,modeSufix);
		endBind();
		printf("\\Mode{%s%s%s}{\n",MODEPREFIX,actPath,modeSufix);
		bind(Tree::Key{2,13});
		printf("\\GoMode{\\UpModeName}");
		endBind();
		bind(Tree::Key{0,0});
		printf("\\GoMode{default}");
		endBind();
		bind(Tree::Key{1,13});
		printf("\\LedGui{%s}",actPath);
		endBind();
#ifdef ADD_RPI_BACKLIGHT
		bind(Tree::Key{6,2});
		printf("\\BackLightToggle\n");
		endBind();
#endif
	}
	fByType(p);
	if(!p->key.null)
		printf("}\n");
	actPath[old_actPath_len]=0;
}
void fByType_as_device(Tree::Abs * d)
{
	//printf("%s\n",d->allName);
	if(!d->key.null)
	{
		for(int i=0;i<Tree::ModeSetKey::lightLen;i++)
		{
			bind(Tree::ModeSetKey::light[i]);
			printf("\\LedLight{%s}{%d}{%d}",actPath,i, Tree::ModeSetKey::lightLen-1);
			endBind();
		}
		for(int i=0;i<Tree::ModeSetKey::lightBoostLen;i++)
		{
			bind(Tree::ModeSetKey::lightBoost[i]);
			printf("\\LedLightBoost{%s}{%d}{%d}{%lf}",actPath,i+1,Tree::ModeSetKey::lightBoostLen, d->maxLightBoost);
			endBind();
		}
		for(int i=0;i<Tree::ModeSetKey::colorLen;i++)
		{
			bind(Tree::ModeSetKey::color[i]);
			printf("\\LedColorBig{%s}{%d}",actPath,i);
			endBind();
		}
		for(int i=0;i<Tree::ModeSetKey::lightSpeedLen;i++)
		{
			bind(Tree::ModeSetKey::lightSpeed[i]);
			printf("\\LedLightSpeed{%s}{%d}",actPath,i);
			endBind();
		}
		for(int i=0;i<Tree::ModeSetKey::colorSpeedLen;i++)
		{
			bind(Tree::ModeSetKey::colorSpeed[i]);
			printf("\\LedColorSpeed{%s}{%d}",actPath,i);
			endBind();
		}
		bind(Tree::ModeSetKey::swap);
		printf("\\LedOpSwap{%s}{_}{second}",actPath);
		endBind();
	}
}
void fByType_as_part(Tree::Part   * p)
{
	for(auto & it:p->content)
		f(it);
}

void fByType(Tree::Abs * p)
{
	if(p->type & Tree::TYPE_Link)
		fByType(((Tree::Link *)p)->target);
	else
	if((p->type & Tree::TYPE_Device) || p->controlAsDevice)
		fByType_as_device(p);
	else
	if(p->type & Tree::TYPE_Part)
		fByType_as_part((Tree::Part*) p);
}

std::unordered_map<std::string,Tree::Part *> partByName;
void loadPartByName(Tree::Part *in=Tree::root)
{
	if(!partByName.count(in->allName)) partByName[in->allName]=in;
	for(auto & it:in->content) if(it->type & Tree::TYPE_Part) loadPartByName((Tree::Part*) it);
}
int main(int argc, char ** argv)
{
	loadPartByName();
	Tree::Abs * root = Tree::root;
	if(argc>1)
	{
		for(int i=0;argv[1][i];i++)
			if(argv[1][i]=='-') argv[1][i]='/';
		root = partByName[argv[1]];
		if(!root)
		{
			fprintf(stderr, "Root device %s not found", argv[1]);
			exit(1);
		}

	}
	printf("\\input led-cmddef.tex\n");
	for(int i=0;i<colorsLen;i++)
		printf("\\LedColorDef{%d}{%s}{%f}{%f}{%f}\n",i,
				colors(i).name,
				colors(i).rgb[0]/255.0,
				colors(i).rgb[1]/255.0,
				colors(i).rgb[2]/255.0);
	printf("\n");
	sprintf(actPath, "%s", root->allName);
	fByType(root);
	return 0;
}

