#include <string.h>

#include "postHooks.h"
#include <MQTTClient.h>

extern MQTTClient mqtt;
extern std::unordered_map<std::string,char *> mqttMap;
extern std::mutex mqttMapMutex;

#define NEED_RGB do{if(device->channels_len!=3) { printf("RGB post hook used on not wrong device %s\n", device->allName);exit(0);}}while(0)
#define NEED_CHANNELS_LEN(v) do{if(device->channels_len!=v) { printf("Post hook with %d channels used on wrong device %s with %d channels\n", v, device->allName, device->channels_len);exit(0);}}while(0)
#define NEED_MAX(v) do{if(data.max!=v) { printf("Post hook with max data value %d used on wrong device %s with max data value %d\n", v, device->allName, data.max);exit(0);}}while(0)
#define NEED_NEOPIXEL NEED_RGB; NEED_MAX(255)


namespace Tree
{
	struct ColorFloat
	{
		float rgb[3];
		float transparency=0;
		ColorFloat(float r, float g, float b, float t=0)
		{
			rgb[0]=r;rgb[1]=g;rgb[2]=b;
			transparency = t;
		}
		ColorFloat(){transparency=1;}
		void TeXDot(FILE * f) const 
		{
			fprintf(f,"\\colordot{%f %f %f}",rgb[0], rgb[1], rgb[2]);
		}
	};

	struct PostHookOneColor:public PostHook
	{
		virtual int setColor(const DeviceReal * device,ColorFloat & color, void  * context) =0;
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			NEED_RGB;
			ColorFloat color;
			int r = setColor(device, color, context);
			for(int i=0;i<data.l;i++)
				for(int j=0;j<3;j++)
					data.data[j][i]=
						data.data[j][i]*(color.transparency)+
						color.rgb[j]*data.max*(1-color.transparency);
			return r;
		}
	};

	struct PostHookMqttInput:public PostHook
	{
		const char * name;
		std::vector<int> input;
		PostHookMqttInput(const char * Name):name(Name){neededMqtt.push_back(name);}
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			NEED_NEOPIXEL;
			for(int i=0;3*i+2<int(input.size()) && i<data.l;i++)
				for(int j=0;j<3;j++)
					if(input[3*i+j]>=0) data.data[j][i]=input[3*i+j];
			return 1<<30;
		}
		virtual int beforeRun()
		{
			std::lock_guard<std::mutex> guard(mqttMapMutex);
			if(mqttMap.count(name))
			{
				char * in = mqttMap[name];
				std::vector<int> newInput;
				bool numOpen=0;
				int mul=1;
				int x=0;
				for(int i=0;!i||in[i-1];i++)
				{
					if(!numOpen && in[i]=='-')
						mul*=-1;
					else
					if(in[i]<='9' && in[i]>='0')
					{
						numOpen=1;
						x=10*x+in[i]-'0';
					}
					else if(numOpen)
					{
						newInput.push_back(x*mul);
						mul=1;
						x=0;
						numOpen=0;
					}
				}
				input=newInput;
			}
			return 1<<30;
		}
	};

	struct PostHookMqttInputMul:public PostHookMqttInput
	{
		PostHookMqttInputMul(const char * Name):PostHookMqttInput(Name){};
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			NEED_NEOPIXEL;
			for(int i=0;3*i+2<int(input.size()) && i<data.l;i++)
				for(int j=0;j<3;j++)
					if(input[3*i+j]>=0) data.data[j][i]=data.data[j][i]*input[3*i+j]/255;
			return 1<<30;
		}
	};

	struct PostHookTextFileInput:public PostHook
	{
		const char * name;
		std::vector<int> input;
		bool change;
		PostHookTextFileInput(const char * Name):name(Name){}
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			NEED_NEOPIXEL;
			for(int i=0;3*i+2<int(input.size()) && i<data.l;i++)
				for(int j=0;j<3;j++)
					if(input[3*i+j]>=0) data.data[j][i]=input[3*i+j];
			return 1<<30;
		}
		virtual int beforeRun()
		{
			change=0;
			FILE * f = fopen(name,"r");
			if(f)
			{
				std::vector<int> newInput;
				int x;
				while(fscanf(f,"%d",&x) == 1)
					newInput.push_back(x);
				if(input!=newInput) change=1;
				input=newInput;
				fclose(f);
			}
			return 1<<30;
		}
	};

	struct PostHookPowerLimit:public PostHook
	{
		int reaming;
		int total;
		PostHookPowerLimit(int t):total(t){}
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			NEED_NEOPIXEL;
			for(int i=0;i<data.l;i++)
			{
				int v = data.data[0][i]+data.data[1][i]+data.data[2][i];
				reaming-=v;
				if(reaming<0)
					data.data[0][i]=data.data[1][i]=data.data[2][i]=0;
			}
			return 1<<30;
		}
		virtual int beforeRun()
		{
			reaming=total;
			return 1<<30;
		}
	};

	struct PostHookOSD:public PostHook
	{
		struct Line
		{
			int r,g,b;
			int len;
		};
		long long t;
		std::vector<Line> lines;
		const char * FNAME;
		const char * DISABLEFNAME;
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			NEED_NEOPIXEL;
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(mqttMap.count(DISABLEFNAME) && !strcmp(mqttMap[DISABLEFNAME],"1")) return 1<<30;
			}
			if(!lines.size()) return 1<<30;
			int reamingLines=lines.size();
			int actLed=0;
			int reamingLed=data.l;
			int direction=1;
			for(Line & it: lines)
			{
				int len = 1 * reamingLed / reamingLines;
				reamingLines -= 1;
				for(int i=0;i<len;i++)
				{
					data.data[0][actLed]=it.r;
					data.data[1][actLed]=it.g;
					data.data[2][actLed]=it.b;
					reamingLed--;
					actLed+=direction;
				}
			}
			return 1<<30;
		}
		int nl(char *& in)
		{
			char * old = in;
			while(in[0] && in[0]!='\n') in++;
			if(in[0]) in++;
			return in - old;
		}
		int nw(char *& in)
		{
			char * old = in;
			while(in[0] && in[0]!='\n' && in[0]!=' ') in++;
			if(in[0]) in++;
			return in - old;
		}
		virtual int beforeRun()
		{
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(mqttMap.count(DISABLEFNAME) && !strcmp(mqttMap[DISABLEFNAME],"1")) return 1<<30;
			}
			lines.resize(0);
			std::lock_guard<std::mutex> guard(mqttMapMutex);
			if(mqttMap.count(FNAME))
			{
				char * f, * faloc;
				f=faloc=new char[strlen(mqttMap[FNAME])+2];
				strcpy(f,mqttMap[FNAME]);
				sscanf(f,"%lld",&t);
				if(t+120 > time(0))
				{
					nl(f);
					int r,g,b;
					while(sscanf(f,"%d%d%d",&r,&g,&b)==3)
					{
						nw(f);nw(f);nw(f);
						int len = nl(f);
						lines.push_back({r,g,b,len});
					}
				}
				delete [] faloc;
			}
			return 1<<30;
			//for(Boxes & it: boxes) printf("%d %d\n",it.cnew,it.ctotal); printf("\n");
		}
		PostHookOSD(const char * fname, const char * disablefname):FNAME(fname),DISABLEFNAME(disablefname)
		{
			neededMqtt.push_back(FNAME);
			neededMqtt.push_back(DISABLEFNAME);
		}
	};

	struct PostHookMail:public PostHook
	{
		struct Boxes
		{
			int r,g,b;
			std::vector<const char *> names;
			int cnew=0, ctotal=0;
			int oldnew=0, oldtotal=0;
		};
		std::vector<Boxes> boxes;
		long long t;
		int aNew, aTotal;
		const char * FNAME = "jk/cm";
		const char * DISABLEFNAME = "jk/led-hook/mail/disable";
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			NEED_NEOPIXEL;
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(mqttMap.count(DISABLEFNAME) && !strcmp(mqttMap[DISABLEFNAME],"1")) return 1<<30;
			}
			int reamingMail=0;
			for(Boxes & it: boxes) reamingMail += it.cnew;
			reamingMail=std::max(reamingMail,3);
			int actLed=0;
			int reamingLed=data.l;
			int direction=1;
			for(Boxes & it: boxes)
			{
				if(reamingMail <= 0) break;
				int len = it.cnew * reamingLed / reamingMail;
				reamingMail -= it.cnew;
				for(int i=0;i<len;i++)
				{
					data.data[0][actLed]=it.r;
					data.data[1][actLed]=it.g;
					data.data[2][actLed]=it.b;
					reamingLed--;
					actLed+=direction;
				}
			}
			return 1<<30;
		}
		void nl(char *& in)
		{
			while(in[0] && in[0]!='\n') in++;
			if(in[0]) in++;
		}
		virtual int beforeRun()
		{
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(mqttMap.count(DISABLEFNAME) && !strcmp(mqttMap[DISABLEFNAME],"1")) return 1<<30;
			}
			for(Boxes & it: boxes) 
			{
				it.oldnew = it.cnew;
				it.oldtotal = it.ctotal;
				it.cnew = it.ctotal = 0;
			}
			std::lock_guard<std::mutex> guard(mqttMapMutex);
			if(mqttMap.count(FNAME))
			{
				char * f, * faloc;
				f=faloc=new char[strlen(mqttMap[FNAME])+2];
				strcpy(f,mqttMap[FNAME]);
				sscanf(f,"%lld%d%d",&t,&aNew,&aTotal);
				if(t+120 > time(0))
				{
					nl(f);
					int ctotal,cnew;
					char tmp_name [1234];
					int r=0;
					while((r=sscanf(f," %[^>]>%d%d",tmp_name,&cnew,&ctotal))==3)
					{
						char *name = tmp_name+1;
						//if(name[0]==0) strcpy(name,"INBOX");
						bool isUsed=0;
						for(auto & it : boxes) for(const char * jt: it.names) if(!strcmp(jt,name))
						{
							it.ctotal+=ctotal;
							it.cnew+=cnew;
							isUsed=1;
						}
						if(!isUsed) for(auto & it : boxes) for(const char * jt: it.names) if(!strcmp(jt,"*"))
						{
							it.ctotal+=ctotal;
							it.cnew+=cnew;
							isUsed=1;
						}

						nl(f);
					}
				}
				delete [] faloc;
			}
			return 1<<30;
			//for(Boxes & it: boxes) printf("%d %d\n",it.cnew,it.ctotal); printf("\n");
		}
		PostHookMail(std::vector<Boxes> B):boxes(B)
		{
			neededMqtt.push_back(FNAME);
			neededMqtt.push_back(DISABLEFNAME);
		}
		virtual void printTeXDoc(FILE * f)
		{
			fprintf(f,"\\hd{Mail}");
			for(Boxes it: boxes)
			{
				fprintf(f,"\\colordot{%f %f %f}",it.r/255.0, it.g/255.0, it.b/255.0);
				for(const char * jt: it.names) fprintf(f,"%s ",jt[0]?jt:"inbox");
				fprintf(f,"\\\\\n");
			}
		}
	};

	struct PostHookMail2: public PostHookMail
	{
		PostHookMail2(std::vector<Boxes> B):PostHookMail(B){}
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			NEED_NEOPIXEL;
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(mqttMap.count(DISABLEFNAME) && !strcmp(mqttMap[DISABLEFNAME],"1")) return 1<<30;
			}
			int actLed=0;
			for(Boxes & it: boxes)
			{
				for(int i=0;i<it.cnew;i++)
				{
					data.data[0][actLed]=it.r;
					data.data[1][actLed]=it.g;
					data.data[2][actLed]=it.b;
					actLed++;
					if(actLed>= data.l) return 1<<30;
				}
			}
			return 1<<30;
		}
	};

	struct PostHookScale:public PostHook
	{
		int a,b; // Multiply every element by fraction a/b
		PostHookScale(int A, int B):a(A),b(B){};
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			for(int j=0;j<data.ch;j++)
				for(int i=0;i<data.l;i++)
					data.data[j][i] = data.data[j][i]*a/b;
			return 1<<30;
		}
	};

	struct PostHookSet:public PostHook
	{
		std::vector<int> val;
		PostHookSet(std::vector<int> VAL):val(VAL){};
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			NEED_CHANNELS_LEN(int(val.size()));
			for(int j=0;j<int(val.size());j++)
				for(int i=0;i<data.l;i++)
					data.data[j][i]=val[j];
			return 1<<30;
		}
	};

	int PostHookNightMode_begin = 1;
	int PostHookNightMode_end = 2;
	int PostHookNightMode_read = 4;
	int PostHookNightMode_begin_read = PostHookNightMode_begin | PostHookNightMode_read;
	struct PostHookNightMode:public PostHook
	{
		DataArr save;
		const char * fname;
		int led_count;
		int led_sum;

		int time_disable_begin = 10*60*60;
		int time_disable_end = 11*60*60;
		int time_if_enable_begin = 22*60*60;
		int time_if_enable_end = 5*60*60;
		int min_off_time = 30*60;

		bool done_if_enable=0;
		bool done_disable=0;
		long long start_off_time=1ll<<60;
		PostHookNightMode(const char * FName):fname(FName){neededMqtt.push_back(fname);save.aloc(1,1,-1);}
		static void cp_data(DataArr to, DataArr from, int l, int ch)
		{
				for(int j=0;j<ch;j++)
					for(int i=0;i<l;i++)
						to.data[j][i]=from.data[j][i];
		}
		virtual int run(const DeviceReal * device,DataArr & data, void  * context_ptr)
		{
			bool enable=1;
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(mqttMap.count(fname) && !strcmp(mqttMap[fname],"0")) enable=0;
			}
			if(data.l > save.l || data.ch > save.ch) save.dealoc(), save.aloc(data.l, data.ch, -1);
			int context = *((int *) context_ptr);
			if(context & PostHookNightMode_begin)
				cp_data(save, data, data.l, data.ch);
			if(context & PostHookNightMode_end)
				if(enable)
					cp_data(data, save, data.l, data.ch);
			if(context & PostHookNightMode_read)
				for(int i=0;i<data.l;i++)
				{
					for(int j=0; j<data.ch;j++)
					{
						led_count++;
						led_sum += data.data[j][i];
					}
				}
			return 1<<30;
		}
		virtual int beforeRun()
		{
			led_sum = led_count = 0;
			return 1<<30;
		}
		int in_time(int act, int begin, int end)
		{
			if(begin<end) return begin<=act&&act<end;
			else return begin<=act||act<end;
		}
		void pub(bool val)
		{
			char out[10];
			sprintf(out,"%d",val);
			MQTTClient_message pubmsg = MQTTClient_message_initializer;
			MQTTClient_deliveryToken token;
			pubmsg.payload = (void *)out;
			pubmsg.payloadlen = strlen(out);
			pubmsg.qos = 0;
			pubmsg.retained = 1;
			MQTTClient_publishMessage(mqtt, fname, &pubmsg, &token);
		}
		virtual int afterRun()
		{
			time_t t = time(NULL);
			int act_s;
			{
				struct tm tm = *localtime(&t);
				act_s = tm.tm_hour*60*60 + tm.tm_min*60 + tm.tm_sec;
			}

			if(in_time(act_s, time_if_enable_begin, time_if_enable_end))
			{
				if(led_count)
				{
					if(led_sum < led_count * 255 * 3  / 100)
					{
						start_off_time=std::min(start_off_time, (long long)t);
						if(t-start_off_time > min_off_time)
							if(!done_if_enable) pub(1), done_if_enable=1;
					}
					else
						start_off_time=1ll<<60;
				}
			}
			else
			{
				done_if_enable=0;
				start_off_time=1ll<<60;
			}
			/*if(in_time(act_s, time_disable_begin, time_disable_end))
			{
				if(!done_disable) pub(0), done_disable=1;
			}
			else
				done_disable=0;*/
			return 1<<30;
		}
	};

	struct PostHookTimer:public PostHook
	{
		const char * FNAME;
		virtual int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			NEED_RGB;
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(!mqttMap.count(FNAME)) return 1<<30;
				long long in = atoll(mqttMap[FNAME]);
				if(!in || in + 120 < time(0))
					return 1<<30;
			}
			struct timespec ts;
			if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
			{
				perror("clock_gettime");
				exit(EXIT_FAILURE);
			}
			if(ts.tv_sec % 2)
			{
				for(int i=0;i<data.l;i++)
				{
					data.data[0][i]=0;
					data.data[1][i]=0;
					data.data[2][i]=data.max;
				}
			}
			else
			{
				for(int i=0;i<data.l;i++)
				{
					data.data[0][i]=0;
					data.data[1][i]=data.max;
					data.data[2][i]=0;
				}
			}
			return 1'000 - ts.tv_nsec / 1'000'000 + 10;
		}
		PostHookTimer(const char * fname):FNAME(fname)
		{
			neededMqtt.push_back(FNAME);
		}
	};

	struct PostHookPowerControl:public PostHook
	{
		PowerControl * pc;
		std::unordered_map<std::string,int> last;

		PostHookPowerControl(PowerControl *PC):pc(PC)
		{
			neededMqtt.push_back(pc->state_fname);
		}
		int run(const DeviceReal * device,DataArr & data, void  * context)
		{
			bool state_on = false;
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(mqttMap.count(pc->state_fname))
				{
					if(!strcmp(mqttMap[pc->state_fname],"on")) state_on = true;
					if(!strcmp(mqttMap[pc->state_fname],"idle")) state_on = true;
				}
			}
			int act = 0;
			for(int i=0; i<data.ch; i++) for(int j=0;j<data.l;j++)
					if(data.data[i][j]) act=time(0);
			char fname[FNAMELEN];
			sprintf(fname, "power-control/%s/control/led/%s", pc->power_control_name, device->allName);
			char out[123];
			sprintf(out, "%d", act);
			if(!!last[device->allName]!=!!act || act>last[device->allName]+50)
			{
				MQTTClient_message pubmsg = MQTTClient_message_initializer;
				MQTTClient_deliveryToken token;
				pubmsg.payload = (void *)out;
				pubmsg.payloadlen = strlen(out);
				pubmsg.qos = 0;
				pubmsg.retained = 1;
				MQTTClient_publishMessage(mqtt, fname, &pubmsg, &token);
				printf("%s %d\n", fname, act);
				fflush(stdout);
				last[device->allName]=act;
			}
			if(!state_on)
				for(int i=0; i<data.ch; i++) for(int j=0;j<data.l;j++) data.data[i][j]=0;
			return act?100'000:1<<30;
		}
	};

	struct PostHookPowerControlStatus:public PostHookOneColor
	{
		const ColorFloat COLOR_ON = ColorFloat(0,1,0);
		const ColorFloat COLOR_OFF = ColorFloat(0,0,0);
		const ColorFloat COLOR_IDLE = ColorFloat(1,0,0);
		const ColorFloat COLOR_STARTING = ColorFloat(1,1,0);
		const ColorFloat COLOR_NO_DATA = ColorFloat(0,0,1);
		PowerControl * pc;
		PostHookPowerControlStatus(PowerControl *PC):pc(PC)
		{
			neededMqtt.push_back(pc->state_fname);
		}
		int setColor(const DeviceReal * device, ColorFloat & color, void  * context)
		{
			char status[FLEN]="";
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(mqttMap.count(pc->state_fname))
				{
					strcpy(status, mqttMap[pc->state_fname]);
				}
			}
			if(!strcmp(status, "on")) color = COLOR_ON; else
			if(!strcmp(status, "idle")) color = COLOR_IDLE; else
			if(!strcmp(status, "starting")) color = COLOR_STARTING; else
			if(!strcmp(status, "off")) color = COLOR_OFF; else
			color = COLOR_NO_DATA;
			return 1<<30;
		}
		virtual void printTeXDoc(FILE * f)
		{
			fprintf(f,"\\hd{Zdroj napájení}");
			COLOR_ON.TeXDot(f);fprintf(f,"aktivní\\\\\n");
			COLOR_OFF.TeXDot(f);fprintf(f,"vypnutý\\\\\n");
			COLOR_IDLE.TeXDot(f);fprintf(f,"nepotřebný, za chvíli se vypne\\\\\n");
			COLOR_STARTING.TeXDot(f);fprintf(f,"startuje\\\\\n");
			COLOR_NO_DATA.TeXDot(f);fprintf(f,"data nejsou k dispozici\\\\\n");
		}
	};

	struct PostHookLowCostEnergy:public PostHookOneColor
	{
		const char * file = "/home/jiri/jug9/levny_tarif/actual";
		std::vector<std::pair<int,int>> times_per_day[7];
		long long valid_from_ts, valid_to_ts;
		const ColorFloat COLOR_NO_DATA = ColorFloat(0,0,1);
		const ColorFloat COLOR_OFF = ColorFloat(1,0,0);
		const ColorFloat COLOR_ON = ColorFloat(0,1,0);
		int fscanftime(FILE * f)
		{
			int a,b;
			fscanf(f,"%d:%d", &a, &b);
			return a*60+b;
		}
		PostHookLowCostEnergy()
		{
			FILE *f = fopen(file,"r");
			if(f)
			{
				fscanf(f,"%lld %lld", &valid_from_ts, &valid_to_ts);
				//fprintf(stderr,"PostHookLowCostEnergy: valid: %lld %lld act: %ld\n", valid_from_ts, valid_to_ts, time(0));
				for(int i=0;i<7;i++)
				{
					char weekday[100];
					fscanf(f," %80s", weekday);
					for(int j=0;j<2;j++)
					{
						int from = fscanftime(f);
						int to = fscanftime(f);
						times_per_day[i].push_back({from,to});
						//fprintf(stderr,"PostHookLowCostEnergy: day %d (%s) from %d to %d\n", i, weekday, from, to);
					}
				}
			}
			else
				fprintf(stderr,"PostHookLowCostEnergy: file %s not found\n", file);
			fflush(stdout);
		}

		int setColor(const DeviceReal * device,ColorFloat & color, void  * context)
		{
			color = COLOR_NO_DATA;
			if(valid_from_ts<=time(0) && time(0)<=valid_to_ts)
			{
				time_t rawtime;
				time(&rawtime);
				struct tm *loctime = localtime(&rawtime);
				int t = loctime->tm_hour*60+loctime->tm_min;

				color = COLOR_OFF;

				for(auto it : times_per_day[(loctime->tm_wday+6)%7])
					if(it.first <= t && t < it.second)
						color = COLOR_ON;
			}
			return 1<<30;
		}
		virtual void printTeXDoc(FILE * f)
		{
			fprintf(f,"\\hd{Levný tarif}");
			COLOR_ON.TeXDot(f);fprintf(f,"levný\\\\\n");
			COLOR_OFF.TeXDot(f);fprintf(f,"drahý\\\\\n");
			COLOR_NO_DATA.TeXDot(f);fprintf(f,"data nejsou k dispozici\\\\\n");
		}
	};

	struct PostHookHeatingMotor:public PostHookOneColor
	{
		const ColorFloat COLOR_0 = ColorFloat(0,0,0);
		const ColorFloat COLOR_1 = ColorFloat(0,1,0);
		const ColorFloat COLOR_2 = ColorFloat(0.5,1,0);
		const ColorFloat COLOR_3 = ColorFloat(1,1,0);
		const ColorFloat COLOR_4 = ColorFloat(1,0.5,0);
		const ColorFloat COLOR_5 = ColorFloat(1,0,0);
		const ColorFloat COLOR_NO_DATA = ColorFloat(0,0,1);
		const char * mqttTopic;
		PostHookHeatingMotor(const char * m):mqttTopic(m)
		{
			neededMqtt.push_back(mqttTopic);
		}
		int setColor(const DeviceReal * device, ColorFloat & color, void  * context)
		{
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(mqttMap.count(mqttTopic))
				{
					float val = atof(mqttMap[mqttTopic]);
					if(val<=0  ) color = COLOR_0; else
					if(val<=0.3) color = COLOR_1; else
					if(val<=0.5) color = COLOR_2; else
					if(val<=0.7) color = COLOR_3; else
					if(val< 1  ) color = COLOR_4; else
					color = COLOR_5;
				}
				else color = COLOR_NO_DATA;
			}
			return 1<<30;
		}
		virtual void printTeXDoc(FILE * f)
		{
			fprintf(f,"\\hd{Topení}");
			COLOR_0.TeXDot(f);fprintf(f,"$\\left<0;0\\right>$\\\\\n");
			COLOR_1.TeXDot(f);fprintf(f,"$\\left(0;0.3\\right>$\\\\\n");
			COLOR_2.TeXDot(f);fprintf(f,"$\\left(0.3;0.5\\right>$\\\\\n");
			COLOR_3.TeXDot(f);fprintf(f,"$\\left(0.5;0.7\\right>$\\\\\n");
			COLOR_4.TeXDot(f);fprintf(f,"$\\left(0.7;1\\right)$\\\\\n");
			COLOR_5.TeXDot(f);fprintf(f,"$\\left<1;1\\right>$\\\\\n");
			COLOR_NO_DATA.TeXDot(f);fprintf(f,"data nejsou k dispozici\\\\\n");
		}
	};
	struct PostHookSensor:public PostHookOneColor
	{
		const ColorFloat COLOR_NO_DATA = ColorFloat(0,0,0);
		struct Configuration
		{
			const char * name;
			std::vector<std::pair<float, ColorFloat>> colors;
		};
		Configuration config;
		const char * mqttTopic;
		int timeout_s;
		PostHookSensor(const char * m, Configuration c, int t=60):config(c),mqttTopic(m),timeout_s(t)
		{
			neededMqtt.push_back(mqttTopic);
		}
		int setColor(const DeviceReal * device, ColorFloat & color, void * context)
		{
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(mqttMap.count(mqttTopic))
				{
					float val;
					long long timestamp;
					sscanf(mqttMap[mqttTopic], "%lld%f", &timestamp, &val);
					if(timestamp+timeout_s > time(NULL))
					{
						for(auto it: config.colors)
							if(val <= it.first)
							{
								color = it.second;
								return 1000*timeout_s;
							}
						color = config.colors.rbegin()[0].second;
						return 1000*timeout_s;
					}
				}
				else color = COLOR_NO_DATA;
			}
			return 1<<30;
		}
		virtual void printTeXDoc(FILE * f)
		{
			fprintf(f,"\\hd{Senzor -- %s}", config.name);
			for(int i=0;i<(int)config.colors.size();i++)
			{
				config.colors[i].second.TeXDot(f);
				if(i==0) fprintf(f,"$\\left(-\\infty;");
				else fprintf(f,"$\\left(%.1f;", config.colors[i-1].first);
				if(i==(int)config.colors.size()-1) fprintf(f,"+\\infty\\right>$\\\\\n");
				else fprintf(f,"%.1f\\right>$\\\\\n", config.colors[i].first);
			}
			COLOR_NO_DATA.TeXDot(f);fprintf(f,"data nejsou k dispozici\\\\\n");
		}
	};

	struct PostHookNextAlarm:public PostHookOneColor
	{
		const ColorFloat COLOR_ERR = ColorFloat(0,0,1);
		const ColorFloat COLOR_0 = ColorFloat(1,1,1);
		const ColorFloat COLOR_1 = ColorFloat(1,0,0);
		const ColorFloat COLOR_2 = ColorFloat(0,1,0);
		const ColorFloat COLOR_NO = ColorFloat(0,0,0);
		const char * mqttTopic;
		PostHookNextAlarm(const char * m):mqttTopic(m)
		{
			neededMqtt.push_back(mqttTopic);
		}
		int setColor(const DeviceReal * device, ColorFloat & color, void * context)
		{
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				if(mqttMap.count(mqttTopic))
				{
					long long val;
					if(sscanf(mqttMap[mqttTopic], "%lld", &val)!=1)
					{
						color = COLOR_NO;
						return 1<<30;
					}
					if(val + 15*60 < time(NULL))
					{
						color = COLOR_ERR;
						return 1<<30;
					}
					if(val > time(NULL)+12*60*60)
					{
						color = COLOR_NO;
						return 1<<30;
					}
					if(val > time(NULL)+8*60*60) color = COLOR_2; else
					if(val > time(NULL)+15*60) color = COLOR_1; else
						color = COLOR_0;
					return 1000*5*60;
				}
				else color = COLOR_ERR;
			}
			return 1<<30;
		}
		virtual void printTeXDoc(FILE * f)
		{
			fprintf(f,"\\hd{Budík}");
			COLOR_NO.TeXDot(f);fprintf(f,"žádný plánovaný budík následujících 12 hodin\\\\\n");
			COLOR_2.TeXDot(f);fprintf(f,"8 až 12 hodin do budíku\\\\\n");
			COLOR_1.TeXDot(f);fprintf(f,"15 minut až 8 hodin do budíku\\\\\n");
			COLOR_0.TeXDot(f);fprintf(f,"méně než 15 minut\\\\\n");
			COLOR_ERR.TeXDot(f);fprintf(f,"chyba\\\\\n");
		}
	};

	struct PreHookWardrobe:public PreHook
	{
		std::vector<const char *> mqttTopics;
		const char * mqttTopicDisable;
		ld val;
		PreHookWardrobe(std::vector<const char *> T, const char *D,  ld V): mqttTopics(T), mqttTopicDisable(D), val(V)
		{
			for(auto it:mqttTopics) neededMqtt.push_back(it);
			neededMqtt.push_back(mqttTopicDisable);
		}
		virtual int run(const DeviceReal * device,InputArr & data, void  * context)
		{
			bool isOpen=false;
			{
				std::lock_guard<std::mutex> guard(mqttMapMutex);
				for(auto topic:mqttTopics)
				{
					if(mqttMap.count(topic) && atoi(mqttMap[topic])==1)
					{
						isOpen = 1;
					}
				}
				if(mqttMap.count(mqttTopicDisable) && atoi(mqttMap[mqttTopicDisable])==1)
					isOpen = 0;
			}
			if(isOpen)
				for(auto & it :data.light) for(auto & jt:it) jt=std::max(jt,val);
			return 1<<30;
		}
	};





}
