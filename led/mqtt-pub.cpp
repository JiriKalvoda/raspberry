#include <string.h>

#include "led-include.h"

LED led;

void hint(const char * world,int parnum,int argc,const char ** argv,FILE * out=stdout)
{
	if(!strcmp(world,"--")) world="";
	if(parnum==0)
	{
		for(const char * it:led.allNames)
		{
			for(int i=0;;i++)
			{
				if(!world[i]) {fprintf(out,"%s\n",it);break;}
				if(it[i]!=world[i]) break;
			}
		}

	}
	if(parnum==1)
	{
		for(const char * it:{"l","c","C","s","S"})
		{
			for(int i=0;;i++)
			{
				if(!world[i]) {fprintf(out,"%s\n",it);break;}
				if(it[i]!=world[i]) break;
			}
		}
	}
}

void usageExit(char * name)
{
	printf("USAGE:\n"
			"\t%s [-r] MQTT_NAME TOPIC [TEXT]\n"
			"\n"
			"If NMAE is not set read from stdin\n"
			"-r make message retained \n"
			"Available name:\n"
			,name);
	for(auto it : Tree::mosquitto_map)  printf("	%s\n",it.first.c_str());
	exit(1);
}


int main(int argc,char ** argv)
{
	for(int i=1;i<argc;i++)
	{
		if(i!=3 && (!strcmp(argv[i],"-h") || !strcmp(argv[i],"--help"))) usageExit(argv[0]);
	}
	int argit = 1;
	if(argit >= argc) usageExit(argv[0]);
	bool retained=0;
	if(!strcmp(argv[argit],"-r"))
	{
		retained=1;
		argit++;
	}
	if(argit >= argc) usageExit(argv[0]);
	if(!Tree::mosquitto_map.count(argv[argit])) usageExit(argv[0]);
	Tree::Mosquitto * mosquitto = Tree::mosquitto_map[argv[argit++]];
	if(argit >= argc) usageExit(argv[0]);
	char * topic = argv[argit++];
	char * data;
	if(argit >= argc)
	{
		data = new char [12345];
		data[fread(data,1,12345,stdin)]=0;
	}
	else
		data=argv[argit++];
	if(argit != argc) usageExit(argv[0]);
	led.mqttMessage(mosquitto,data,topic,retained);
	return 0;
}

