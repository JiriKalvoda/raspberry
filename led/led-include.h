#ifndef LED_INCLUDE_H_INCLUDE
#define LED_INCLUDE_H_INCLUDE

#include <MQTTClient.h>
#include <map>
#include <set>
#include <string>
#include <mutex>

#include "deviceTree.h"

struct LED
{
	using cch = const char;

	struct Mqtt
	{
		MQTTClient client=0;
		Tree::Mosquitto * tree=0;
		LED * led=0;
		bool connected=0;
		bool runnungConnect=0;
		std::set<std::string> subscribe; // only for LEDWithRead
		std::map<std::string, const char *> data; // only for LEDWithRead
		std::mutex dataMutex;
	};

	int TIMEOUT=10000;

	std::unordered_map<std::string,Tree::Part *> partByName;
	std::unordered_map<std::string,Tree::Device *> deviceByName;
	std::vector<cch *> allNames;


	char mqttConName[1234];
	std::unordered_map<Tree::Mosquitto *,Mqtt *> mqttMap;

	virtual MQTTClient connect(Mqtt * mqtt,bool cleansession=1,bool NoErr=0);
	void connectAndWriteToMap(Tree::Mosquitto * mosquitto,bool reconnect=0, bool NoErr=0);

	void mqttMessage(Tree::Mosquitto * mosquitto, const char * data, const char * topic=Tree::MQTTMESSAGE, bool retained=0);
	void mqttMessage(std::vector<Tree::Abs*> dev , const char * data);
	void mqttMessage(cch * in , const char * data);

	void light(cch * name, cch * val, int maxVal=3);
	void lightBig(cch * name, cch * val);
	void lightBoost(cch * name, cch * val, int maxVal, double maxBoost=-1);
	void color(cch * name, cch * val, int maxVal=3);
	void colorBig(cch * name, cch * val);
	void colorSpeed(cch * name, cch * val);
	void lightSpeed(cch * name, cch * val);

	void f(cch * name,cch * light,cch * r="_",cch * g="_",cch * b="_",cch * lightSpeed="_",cch * colorSpeed="_");
	void channels(cch * name, cch ** val, int val_len);

	void loadTree(Tree::Part * in);
};

#endif
