#include "deviceTree.h"
#include <cassert>
#include <cstring>
#include <math.h>
#define fo(a,b) for(int a=0;a<(b);++a)


#define STANDARD_CONSTRUCTOR(t, p) t::t(Abs A):p(A){type=Type(type|TYPE_ ## t);}

namespace Tree
{
	void ChLArr::aloc(int len, int channels)
	{
		l=len;
		ch=channels;
		data=new ld*[ch];
		data[0]=new ld[l*ch];
		for(int i=1;i<ch;i++) data[i]=data[i-1]+len;
		for(int j=0;j<ch;j++)
			for(int i=0;i<l;i++) data[j][i]=1;
		light=new ld[l];
		for(int i=0;i<l;i++) light[i]=0;
	}
	void ChLArr::dealoc()
	{
		delete [] data[0];
		delete [] data;
	}

	bool DataArr::operator == (const DataArr & d) const
	{
		if(l!=d.l) return 0;
		if(ch!=d.ch) return 0;
		for(int a=0;a<ch;a++) for(int i=0;i<l;i++) if(data[a][i]!=d.data[a][i]) return 0;
		return 1;
	}
	DataArr & DataArr::operator = (const DataArr & d)
	{
		for(int a=0;a<ch&&a<d.ch;a++)
			for(int i=0;i<l&&i<d.l;i++)
				data[a][i]=d.data[a][i];
		return *this;
	}
	void DataArr::aloc(int len, int channels, int max_val)
	{
		ch = channels;
		l=len;
		max = max_val;
		data=new int*[ch];
		data[0]=new int[l*ch];
		for(int i=1;i<ch;i++) data[i]=data[i-1]+len;
	}
	void DataArr::dealoc()
	{
		delete [] data[0];
		delete [] data;
	}


	InputArr::InputArr(int CH)
	{
		ch = CH;
		data = new ld2d[ch];
	}
	InputArr::~InputArr()
	{
		delete[] data;
	}


	Key Key::operator + (int i)
	{
		Key r = *this;
		r.col += i;
		return r;
	}
	int Key::i3Code()
	{
		if(row==0 && col==0) return 9;
		if(row==0 && col>=11) return 95+col-11;
		if(row==0) return col+67-1;
		if(row==1 && col==0) return 49;
		if(row==1) return col+10-1;
		if(row==2) return col+23;
		if(row==3 && col==0) return 66;
		if(row==3 && col==12) return 51;
		if(row==3) return col+38-1;
		if(row==4 && col==1) return 94;
		if(row==4) return col+50;
		if(row==5) return std::vector<int>{37,133,64,65,108,105}[col];

		if(row==6) return std::vector<int>{107,78,127}[col];
		if(row==7) return std::vector<int>{118,110,112}[col];
		if(row==8) return std::vector<int>{119,115,117}[col];
		if(row==9) return std::vector<int>{111}[col];
		if(row==10) return std::vector<int>{113,116,114}[col];

		if(row==11) return std::vector<int>{77,106,63,82}[col];
		if(row==12) return std::vector<int>{79,80,81,86}[col];
		if(row==13) return std::vector<int>{83,84,85}[col];
		if(row==14) return std::vector<int>{87,88,89,104}[col];
		if(row==15) return std::vector<int>{90,91}[col];
		return -1;
	}
	int Key::i3Print(char * str)
	{
		return sprintf(str,"%s%s%s%d",
				mod&CTRL?"Ctrl+":"",
				mod&SHIFT?"Shift+":"",
				mod&SUPER?"$mod+":"",
					i3Code());
	}
	int Key::texPrint(char * str)
	{
		return sprintf(str,"\\Key{%s%s%s}{%d}{%d}",
				mod&CTRL?"\\Ctrl":"",
				mod&SHIFT?"\\Shift":"",
				mod&SUPER?"\\Super":"",
					row, col);
	}
	bool Key::operator == (const Key &  d) const
	{
		if(null!=d.null) return 0;
		if(row!=d.row) return 0;
		if(col!=d.col) return 0;
		if(mod!=d.mod) return 0;
		return 1;
	}
	bool Key::operator < (const Key &  d) const
	{
		if(null!=d.null) return null < d.null;
		if(row!=d.row) return row < d.row;
		if(col!=d.col) return col < d.col;
		if(mod!=d.mod) return mod < d.mod;
		return 0;
	}

	
	std::vector<Key> rowKeys(Key base, int len)
	{
		std::vector<Key> out;
		for(int i=0;i<len;i++) out.push_back(base+i);
		return out;
	}

	namespace ModeSetKey
	{
		Key light[lightLen];
		Key lightBoost[lightBoostLen];
		Key color[colorLen];
		Key lightSpeed[lightSpeedLen];
		Key colorSpeed[colorSpeedLen];
		Key swap={5,3};		
		struct RUN{RUN()
			{
			for(int i=0;i<lightLen;i++)
				light[i]=Key{1,0}+i;
			for(int i=0;i<lightBoostLen;i++)
				lightBoost[i]=Key{1,lightLen}+i;
			for(int i=0;i<12;i++)
				color[i]=Key{2,1}+i;
			for(int i=0;i<11;i++)
				color[12+i]=Key{3,1}+i;
			for(int i=0;i<10;i++)
				color[12+11+i]=Key{4,2}+i;
			for(int i=0;i<lightSpeedLen;i++)
				lightSpeed[i]=Key{0,1}+i;
			for(int i=0;i<colorSpeedLen;i++)
				colorSpeed[i]=Key{0,9}+i;
		}}RUN_IMP;
	};

	std::unordered_map<std::string,Mosquitto *> mosquitto_map;
	Mosquitto::Mosquitto(const char * N):name(N)
	{
		mosquitto_map[name]=this;
	}
	std::string Mosquitto::get(const char * app,const char * sufix, bool noErr)
	{
		char fname[FNAMELEN];
		char * home = getenv("HOME");
		if(!home)
		{
			fprintf(stderr,"Missing home env.\n");
			fprintf(stdout,"Missing home env.\n");
			fflush(stderr);
			fflush(stdout);
			exit(111);
		}
		sprintf(fname,"%s/.secret/mqtt/%s-%s%s",getenv("HOME"),name,app,sufix);
		FILE *f = fopen(fname,"r");
		if(!f)
		{
			sprintf(fname,"%s/.secret/mqtt/%s%s",getenv("HOME"),name,sufix);
			f = fopen(fname,"r");
			if(!f)
			{
				if(noErr) return "";
				fprintf(stderr,"Missing password file %s\n",fname);
				fprintf(stdout,"Missing password file %s\n",fname);
				fflush(stderr);
				fflush(stdout);
				exit(111);
			}
		}
		char in[FNAMELEN];
		fscanf(f,"%100[^\n]",in);
		fclose(f);
		return std::string(in);
	}
	std::string Mosquitto::getUsername(const char * app)
	{
		return  get(app,".user", true);
	}
	std::string Mosquitto::getPasswd(const char * app)
	{
		return  get(app,".passwd", true);
	}
	std::string Mosquitto::getAddres()
	{
		std::string tunel = get("all",".tunel",1);
		if(tunel=="")
			return  get("all",".addres");
		else
			return tunel;
	}

	PostHookOnStrip::PostHookOnStrip(PostHook * PH, int BeginLed, int EndLed, int Step, void * Context)
	{
		ph = PH;
		beginLed = BeginLed;
		endLed = EndLed;
		step = Step;
		context = Context;
		if(!step) step=1;
		int len = (endLed - beginLed)/step;
		if(len<0) len*=-1, step *=-1;
		len++;
		endLed = beginLed + (step*len);
	}

	PreHookOnStrip::PreHookOnStrip(PreHook * PRH, void * Context)
	{
		prh=PRH;
		context=Context;
	}

	std::unordered_map<std::string,Maschine *> maschine_map;
	Maschine::Maschine(const char * N,Mosquitto * M, int DP, int CP, int LP, int CH, const char * S, std::vector<int> DIRPS):
		name(N),mosquitto(M),dpin(DP),cpin(CP),lpin(LP),checkpin(CH),serial(S), directpins(DIRPS)
	{
		maschine_map[name]=this;
	}

	Abs::Abs(const char * NAME, Key KEY, std::vector<Key> LIGHTKEY,std::vector<Key> LIGHTBOOSTKEY, std::vector<Key> COLORKEY, Key INVERTKEY, std::vector<Action*> ACTIONS):
		name(NAME), key(KEY), lightKey(LIGHTKEY), lightBoostKey(LIGHTBOOSTKEY),colorKey(COLORKEY), invertKey(INVERTKEY), actions(ACTIONS)
		{
			type = Type(type | TYPE_Abs);
		}
	Abs * Abs::guiHide(){ guiHidden=true; return this;}
	Part * Part::guiHide(){ guiHidden=true; return this;}
	Abs * Abs::setControlAsDevice(){controlAsDevice=true; return this;}
	Part * Part::setControlAsDevice(){controlAsDevice=true; return this;}


	STANDARD_CONSTRUCTOR(Part, Abs)
    void  calc_link(Link *l)
	{
		if(!l->target)
			l->target = l->up->evalRelativePath(l->relativePath);
		if(!l->target)
		{
			fprintf(stderr, "DeviceTree: %s bad link addres %s\n", l->allName, l->relativePath);
			exit(1);
		}
	}
    void  calc_names(Part *p)
	{
		for(Abs * it:p->content)
			sprintf(it->allName,"%s%s%s",p->allName,p->allName[0]&&it->name[0]?"/":"",it->name);
		for(Abs * it:p->content) if(it->type & TYPE_Link)
			calc_link((Link*)it);
		for(Abs * it:p->content) if(it->type & TYPE_Part)
			calc_names((Part*)it);
	}
    void  calc_mosquittos(Abs *a); // and light boost
    void  calc_mosquittos(Link *l)
	{
		calc_mosquittos(l->target);
		l->mosquitto = l->target->mosquitto;
		l->maxLightBoost = l->target->maxLightBoost;
	}
    void  calc_mosquittos(Part *p)
	{
		for(Abs * it:p->content)
			calc_mosquittos(it);
		for(Abs * it:p->content)
		{
			for(auto jt : it->mosquitto)
				p->mosquitto.insert(jt);
			p->maxLightBoost = std::max(p->maxLightBoost, it->maxLightBoost);
		}
		if(p->contentByName.count("")) p->contentByName[""]->maxLightBoost = p->maxLightBoost;

	}
    void  calc_mosquittos(Abs *a)
	{
		if(a->mosquitto_calculated) return;
		a->mosquitto_calculated = true;
		if(a->type & TYPE_Link) calc_mosquittos((Link*)a);
		if(a->type & TYPE_Part) calc_mosquittos((Part*)a);
	}
	Part * Part::calc()
	{
		calc_names(this);
		calc_mosquittos(this);
		return this;
	}
	Part * Part::add(Abs * in)
	{
		content.push_back(in);
		contentByName[in->name]=in;
		in->up = this;
		return this;
	}
	Abs * Abs::evalRelativePath(const char * path)
	{
		if(path[0]=='\0') return this;
		int l=0; while(path[l]&&path[l]!='/')l++;
		const char * nextPath=path+l; while(nextPath[0]&&nextPath[0]=='/') nextPath++;
		if(l==0) return root->evalRelativePath(nextPath);
		if(!strncmp(path,".",l)) return evalRelativePath(nextPath);
		if(!strncmp(path,"..",l)) return up?up->evalRelativePath(nextPath):0;
		return NULL;
	}
	Abs * Part::evalRelativePath(const char * path)
	{
		if(path[0]=='\0') return this;
		int l=0; while(path[l]&&path[l]!='/')l++;
		const char * nextPath=path+l; while(nextPath[0]&&nextPath[0]=='/') nextPath++;
		{
			std::string partPath(path, l);
			if(contentByName.count(partPath))
				return contentByName[partPath]->evalRelativePath(nextPath);
		}
		if(l==0) return root->evalRelativePath(nextPath);
		if(!strncmp(path,".",l)) return evalRelativePath(nextPath);
		if(!strncmp(path,"..",l)) return up?up->evalRelativePath(nextPath):0;
		return NULL;
	}
	Abs * Link::evalRelativePath(const char * path)
	{
		if(!target) calc_link(this);
		return target->evalRelativePath(path);
	}

	STANDARD_CONSTRUCTOR(Link, Abs)
	
	Link * Link::setPath(const char * p)
	{
		relativePath = p;
		return this;
	}
	

	STANDARD_CONSTRUCTOR(DeviceVirtual, Device)


	STANDARD_CONSTRUCTOR(Device, Abs)


	DeviceReal::DeviceReal(Abs A):Device(A){type=Type(type|TYPE_DeviceReal);
		set_rgbToChannel(base_rgbToChannel);
		set_channelToRgb(base_channelToRgb);
	}
	DeviceReal * DeviceReal::addPostHook(PostHook * in, int start, int end, int step, void * context)
	{
		if(start<0) start = len-1;
		if(end<0) end = len-1;
		postHook.push_back(PostHookOnStrip(in, start, end, step, context));
		return this;
	}
	DeviceReal * DeviceReal::addPreHook(PreHook * in, void * context)
	{
		preHook.push_back(PreHookOnStrip(in, context));
		return this;
	}

	FuncChannelToRgb DeviceReal::base_channelToRgb(DeviceReal *d)
	{
		return [d](ld *in, ld *out){
			fo(i,3) out[i]=0;
			ld sum = 0;
			fo(i,d->channels_len)
				sum += in[i];
			fo(i,d->channels_len)
				fo(j,3)
					out[j]+=in[i]*d->channels[i]->rgb[j];
			ld max = 0;
			fo(i,3) max = std::max(max, out[i]);
			if(max>0.001)
				fo(i,3) out[i]*=sum/max;

		};
	}
	FuncRgbToChannel DeviceReal::base_rgbToChannel(DeviceReal *d)
	{
		return [d](ld *in, ld *out){
			fo(i,d->channels_len)
					out[i]=i<3?in[i]:0;
			ld sum = 0, max = 0;
			fo(i,d->channels_len)
				sum += out[i], max = std::max(max, out[i]);
			if(sum>0.001)
				fo(i,d->channels_len)
					out[i]*=max/sum;
		};
	}
	


	STANDARD_CONSTRUCTOR(DeviceNeoPixel, DeviceReal)

	DeviceNeoPixel * DeviceNeoPixel::set(Maschine * M,int P,int L,int CHECK)
	{
		channels.clear();
		for(int i=0;i<3;i++)
		{
			channels.push_back(new ChannelDMX());
			for(int j=0;j<3;j++) channels[i]->rgb[j] = i==j;
		}
		channels_len = 3;
		maxLightBoost = pow(channels_len,1.0/3);
		maschine=(M);
		port=(P);
		len=(L);
		check=(CHECK);
		if(M) mosquitto={M->mosquitto};
		return this;
	}
	/*void DeviceNeoPixel::rgbToChannel(double * in, double * out)
	{
		for(int i=0;i<channels_len;i++) out[i]=in[i];
	}*/


	STANDARD_CONSTRUCTOR(DeviceDMX, DeviceReal)
	DeviceDMX * DeviceDMX::set(Maschine * M)
	{
		//rescaleLightByColor=1;
		maschine=(M);
		len=1;
		if(M) mosquitto={M->mosquitto};
		return this;
	}
	DeviceDMX * DeviceDMX::add_channel(double R, double G, double B, int C, int P)
	{
		double rgb[3] = {R,G,B};
		return add_channel(rgb, C, P);
	}
	DeviceDMX * DeviceDMX::add_channel(double RGB[3], int C, int P)
	{
		ChannelDMX *ch = new ChannelDMX;
		for(int i=0;i<3;i++) ch->rgb[i]=RGB[i];
		ch->card = C;
		ch->port = P;
		channels.push_back(ch);
		((DeviceReal *)this)->channels.push_back(ch); // HACK polymorfismus of vector
		channels_len++;
		maxLightBoost = pow(channels_len,1.0/3);
		return this;
	}
	/*void DeviceDMX::rgbToChannel(double * in, double * out)
	{
		for(int i=0;i<channels_len;i++) out[i]=in[i];
	}*/
	void rgbToXYA(ld *in, ld *out)
	{
		ld s = in[0]+in[1]+in[2];
		if(s<0.0001) 
		{
			out[0]=out[1]=out[2]=0;
			return;
		}
		out[0]=(-in[0]+(in[1]+in[2])/2)/s;
		out[1]=(in[1]-in[2])*0.8660254/s; // 0.8660254 = sqrt(1-1/2**2)
		out[2]=std::max(std::max(in[0],in[1]),in[2]);
	}
	template<class T> T pow2(T in){return in*in;};
	ld len2(ld *x, ld *y)
	{
		return pow2(x[0]-y[0])+pow2(x[1]-y[1]);
	}
	FuncRgbToChannel DeviceReal::oneChannel_rgbToChannel(DeviceReal *d)
	{
		assert(d->channels_len == 1);
		return [d](ld *in, ld *out){
			ld xya[3];
			ld xyaCh[3];
			rgbToXYA(in, xya);
			rgbToXYA(d->channels[0]->rgb, xyaCh);
			ld light = xya[2]*(1-pow2(2)*len2(xya, xyaCh));
			if(light<0) light=0;
			out[0]=light;
		};
	}
	FuncRgbToChannel DeviceReal::alwaysOn_rgbToChannel(DeviceReal *d)
	{
		assert(d->channels_len == 1);
		return [d](ld *in, ld *out){
			out[0]=1;
		};
	}
	std::pair<ld,ld> placeDist(ld A[2], ld v[2], ld B[2])
	{
		ld m[2][3] = {{v[0], -v[1], B[0]-A[0]},{v[1], v[0], B[1]-A[1]}};
		ld D = m[0][0]*m[1][1]-m[0][1]*m[1][0];
		ld x = m[0][2]*m[1][1]-m[0][1]*m[1][2];
		ld y = m[0][0]*m[1][2]-m[0][2]*m[1][0];
		ld place  = x/D;
		ld dist = pow2(y/D) * (pow2(v[0])+pow2(v[1]));
		return std::make_pair(place, dist);
	}
	FuncRgbToChannel DeviceReal::linerarSpaceChannels_rgbToChannel(DeviceReal *d)
	{
		return [d](ld *in, ld *out){
			int ch_len = d->channels_len;
			ld xya[3];
			ld xyaCh[ch_len][3];
			rgbToXYA(in, xya);
			fo(i,ch_len) rgbToXYA(d->channels[i]->rgb, xyaCh[i]);

			ld *A = xyaCh[0];
			ld v[]={xyaCh[ch_len-1][0]-A[0], xyaCh[ch_len-1][1]-A[1]};
			ld place, dist; std::tie(place, dist) = placeDist(A,v, xya);
			if(place<0) place=0, dist = len2(xya, xyaCh[0]);
			if(place>1) place=1, dist = len2(xya, xyaCh[ch_len-1]);

			ld placeDCh[ch_len], distCh[ch_len];
			fo(i,ch_len)
			{
				std::tie(placeDCh[i], distCh[i]) = placeDist(A,v, xyaCh[i]);
				placeDCh[i] -= place;
			}



			ld light = xya[2]*(1-pow2(3)*dist);
			if(light<0) light=0;

			fo(i,ch_len) out[i]=1;

			ld placeDAct = 0;
			fo(i,ch_len) placeDAct += out[i] * placeDCh[i];

			fo(i,ch_len-1) assert(placeDCh[i] <= placeDCh[i+1]);
			
			if(placeDAct<0)
			{
				for(int i=0;i<ch_len;i++)
				{
					if(placeDAct > -0.01) break;
					ld placeDNew = placeDAct - out[i] * placeDCh[i];
					if(placeDNew>=0)
					{
						out[i] =  placeDNew/(placeDNew-placeDAct);
						placeDAct=0;
						break;
					}
					else
					{
						out[i]=0;
						placeDAct=placeDNew;
					}
				}
			}
			else
			{
				for(int i=ch_len;i-->0;)
				{
					if(placeDAct < 0.01) break;
					ld placeDNew = placeDAct - out[i] * placeDCh[i];
					if(placeDNew<=0)
					{
						out[i] =  placeDNew/(placeDNew-placeDAct);
						placeDAct=0;
					}
					else
					{
						out[i]=0;
						placeDAct=placeDNew;
					}
				}
			}

			ld sum = 0;
			fo(i,ch_len) sum += out[i];
			fo(i,ch_len) out[i]*=xya[2]*light/sum;
		};
	}



	
	PowerControl::PowerControl(const char * N,Mosquitto * M, int P)
	{
		power_control_name = N;
		mosquitto = M;
		pin = P;
		power_control_map[power_control_name]=this;
			sprintf(state_fname, "power-control/%s/state", power_control_name);
	}
	std::unordered_map<std::string,PowerControl *> power_control_map;



	ld2d parse2D(const char * in)
	{
		ld2d out(1,std::vector<ld>(1,0));
		double exp=1;
		for(int i=0;in[i];i++)
		{
			if('0'<=in[i]&&in[i]<='9')
			{
				if(exp==1)
					out.rbegin()[0].rbegin()[0]=out.rbegin()[0].rbegin()[0]*10+(in[i]-'0');
				else
					if(exp==-1)
						out.rbegin()[0].rbegin()[0]=out.rbegin()[0].rbegin()[0]*10-(in[i]-'0');
					else
					{
						out.rbegin()[0].rbegin()[0]+=exp*(in[i]-'0');
						exp*=0.1;
					}
			}
			else if('.'==in[i]) exp=exp*0.1;
			else if('-'==in[i]) exp=-1;
			else if(in[i]==' ') {out.push_back(std::vector<double>(1,0));exp=1;}
			else if(in[i]==':') {out.rbegin()[0].push_back(0);exp=1;}
		}
		return out;
	}

	void createStrFrom2D(ld2d in, char * out)
	{
		char * outpt = out;
		for(int i=0;i<int(in.size());i++)
		{
			if(i) outpt += sprintf(outpt," ");
			for(int j=0;j<int(in[i].size());j++)
				outpt += sprintf(outpt,j?":%F":"%F",in[i][j]);
		}
	}




	Action::Action()
	{
		type = Type(type | TYPE_Action);
	}
	ActionKey::ActionKey(Key K):key(K)
	{
		type = Type(type | TYPE_ActionKey);
	}
	ActionKeyCmd::ActionKeyCmd(Key K, const char * CMD):ActionKey(K),cmd(CMD)
	{
		type = Type(type | TYPE_ActionKeyCmd);
	}
	ActionKeyMqtt::ActionKeyMqtt(Key K, Mosquitto * MOSQUITTO, const char * TOPIC, const char * PAYLOAD, bool RETAINED):
		ActionKey(K),mosquitto(MOSQUITTO),topic(TOPIC),payload(PAYLOAD),retained(RETAINED)
	{
		type = Type(type | TYPE_ActionKeyMqtt);
	}
	ActionOnOff::ActionOnOff(Key ONK, Key OFFK): onKey(ONK), offKey(OFFK)
	{
		type = Type(type | TYPE_ActionOnOff);
	}
	ActionOnOff * ActionOnOff::setOnOffMsq(const char * ON, const char * OFF)
	{
		on=ON, off=OFF;
		return this;
	}
	ActionOnOff * ActionOnOff::setToggle(Key K, const char * MSG)
	{
		toggleKey=K; toggle=MSG;
		return this;
	}
	ActionOnOffCmd::ActionOnOffCmd(Key ONK, Key OFFK, const char * CMD):ActionOnOff(ONK, OFFK),cmd(CMD)
	{
		type = Type(type | TYPE_ActionOnOffCmd);
	}
	char * ActionOnOffCmd::getCmd(const char * val)
	{
		int l=strlen(cmd)+10;
		for(int i=0;cmd[i];i++) if(cmd[i]=='%') l+=strlen(val);
		char * r = new char[l];
		int j=0;
		for(int i=0;cmd[i]&&j<l-10;i++)
		{
			if(cmd[i]=='%' && cmd[i+1]=='%') r[j++]='%',i++;
			else
			if(cmd[i]=='%' && cmd[i+1]=='s')
			{
				i++;
				for(int k=0;val[k]&&j<l-10;k++)
					r[j++]=val[k];
			}
			else r[j++]=cmd[i];
		}
		r[j]=0;
		return r;
	}
	ActionOnOffMqtt::ActionOnOffMqtt(Key ONK, Key OFFK, Mosquitto * MOSQUITTO, const char * TOPIC):ActionOnOff(ONK, OFFK),mosquitto(MOSQUITTO),topic(TOPIC)
	{
		type = Type(type | TYPE_ActionOnOffMqtt);
	}
	ActionSmoothMqtt::ActionSmoothMqtt(std::vector<Key> K, Mosquitto * MOSQUITTO, const char * TOPIC):keys(K),mosquitto(MOSQUITTO),topic(TOPIC)
	{
		type = Type(type | TYPE_ActionSmoothMqtt);
	}
	ActionMotor::ActionMotor(std::vector<Key> K, Mosquitto * MOSQUITTO, const char * TOPIC):ActionSmoothMqtt(K, MOSQUITTO, TOPIC)
	{
		type = Type(type | TYPE_ActionMotor);
	}
	ActionVztFan::ActionVztFan(std::vector<Key> K, Mosquitto * MOSQUITTO, const char * TOPIC):ActionSmoothMqtt(K, MOSQUITTO, TOPIC)
	{
		type = Type(type | TYPE_ActionVztFan);
	}
}
