#ifndef DEVICETREE_H_INCLUDE
#define DEVICETREE_H_INCLUDE

#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <functional>

namespace Tree
{
	using ld = double;
	using ld2d = std::vector<std::vector<ld>>;
	using ld1d = std::vector<ld>;
	struct ChLArr
	{
		ld **data;// in <0,1>
		ld *light;
		int ch; // num of channels
		int l; // led of strip
		void aloc(int len, int channels);
		void dealoc();
	};

	struct DataArr
	{
		int max;
		int **data; // array size [ch][l]
		int ch; // num of channels
		int l; // led of strip
		bool operator == (const DataArr & d) const;
		DataArr & operator = (const DataArr & d);
		void aloc(int len, int channels, int max_val);
		void dealoc();
	};

	struct InputArr
	{
		ld2d *data;
		ld2d light;
		ld colorSpeed;
		ld lightSpeed;
		int ch;
		InputArr(int CH);
		~InputArr();
	};



	const int FNAMELEN=1000;
	const int FLEN=10000;
	extern const char * MQTTPREFIX;
	extern const char * MQTTMESSAGE;
	const int STRLEN = 1000;
	struct Key
	{
		enum MOD {NOMOD=0, CTRL=1<<0, SHIFT=1<<1,SUPER=1<<2,ALT=1<<3};
		int row,col;
		MOD mod=NOMOD;
		bool null=0;
		Key operator + (int i);
		int i3Code();
		int i3Print(char * str);
		int texPrint(char * str);
		bool operator == (const Key &  d) const;
		bool operator < (const Key &  d) const;
	};
	namespace ModeSetKey
	{
		const int lightLen = 10;
		const int lightBoostLen = 3;
		extern Key light[lightLen];
		extern Key lightBoost[lightBoostLen];
		const int colorLen = 12+11+10;
		extern Key color[colorLen];
		const int lightSpeedLen=4;
		extern Key lightSpeed[lightSpeedLen];
		const int colorSpeedLen=4;
		extern Key colorSpeed[colorSpeedLen];
		extern Key swap;		
	};

	const Key NULLKEY = {0,0,Key::NOMOD,1};

	std::vector<Key> rowKeys(Key base, int len);

	struct Mosquitto
	{
		const char * name;
		Mosquitto(const Mosquitto &)=delete;
		Mosquitto(const char * N);
		std::string get(const char * app,const char * sufix, bool noErr=0);
		std::string getUsername(const char * app);
		std::string getPasswd(const char * app);
		std::string getAddres();
	};
	extern std::unordered_map<std::string,Mosquitto *> mosquitto_map;
	struct Maschine
	{
		const char * name;
		Mosquitto * mosquitto;
		int dpin; // Shift registr data pin
		int cpin; // Shift registr clock pin
		int lpin; // Shift registr latch pin
		int checkpin; // Shift check data pin
		const char * serial;
		std::vector<int> directpins;
		Maschine(const Maschine &)=delete;
		Maschine(const char * N,Mosquitto * M, int DP=23, int CP=21, int LP=22, int CH=25, const char * S = "/dev/ttyS0", std::vector<int> DIRPS={});
	};
	extern std::unordered_map<std::string,Maschine *> maschine_map;
	struct PostHook;
	struct Hook;
	struct PreHook;

	struct PostHookOnStrip
	{
		PostHook * ph;
		int beginLed;
		int endLed;
		int step;
		void * context;
		DataArr dataArr;
		PostHookOnStrip(PostHook * PH, int BeginLed, int EndLed, int Step, void * Context);
	};
	struct PreHookOnStrip
	{
		PreHook * prh;
		void * context;
		PreHookOnStrip(PreHook * PRH, void * Context);
	};
	enum Type
	{
		TYPE_Device = 1<<0,
		TYPE_Part = 1<<1,
		TYPE_DeviceReal = 1<<2,
		TYPE_DeviceVirtual = 1<<3,
		TYPE_DeviceDMX = 1<<4,
		TYPE_DeviceNeoPixel = 1<<5,
		TYPE_Link = 1<<6,
		TYPE_Abs = 1<<7,

		TYPE_Action = 1<<8,
		TYPE_ActionKey = 1<<9,
		TYPE_ActionKeyCmd = 1<<10,
		TYPE_ActionKeyMqtt = 1<<11,
		TYPE_ActionOnOff = 1<<12,
		TYPE_ActionOnOffCmd = 1<<13,
		TYPE_ActionOnOffMqtt = 1<<14,
		TYPE_ActionSmoothMqtt = 1<<15,
		TYPE_ActionMotor = 1<<16,
		TYPE_ActionVztFan = 1<<17,
	};
	struct Typeing
	{
		Type type = Type(0); // bitmask of types
	};

	struct Action:Typeing
	{
		Action();
	};
	struct ActionKey:Action
	{
		Key key;
		ActionKey(Key K);
	};
	struct ActionKeyCmd:ActionKey
	{
		const char * cmd;
		ActionKeyCmd(Key K, const char * CMD);
	};
	struct ActionKeyMqtt:ActionKey
	{
		Mosquitto *mosquitto;
		const char * topic;
		const char * payload;
		bool retained;
		ActionKeyMqtt(Key K, Mosquitto * MOSQUITTO, const char * TOPIC, const char * PAYLOAD, bool RETAINED=0);
	};
	struct ActionOnOff:public Action
	{
		Key onKey, offKey, toggleKey=NULLKEY;
		const char * on="1";
		const char * off="0";
		const char * toggle=NULL;
		ActionOnOff(Key ONK, Key OFFK);
		ActionOnOff * setOnOffMsq(const char * ON, const char * OFF);
		ActionOnOff * setToggle(Key K, const char * MSG="toggle");
	};
	struct ActionOnOffCmd:public ActionOnOff
	{
		const char * cmd;
		ActionOnOffCmd(Key ONK, Key OFFK, const char * cmd);
		char * getCmd(const char * val);
	};
	struct ActionOnOffMqtt:public ActionOnOff
	{
		Mosquitto *mosquitto;
		const char * topic;
		ActionOnOffMqtt(Key ONK, Key OFFK, Mosquitto * MOSQUITTO, const char * TOPIC);
	};
	struct ActionSmoothMqtt:public Action
	{
		std::vector<Key> keys;
		Mosquitto *mosquitto;
		const char * topic;
		ActionSmoothMqtt(std::vector<Key> K, Mosquitto * MOSQUITTO, const char * TOPIC);
	};
	struct ActionMotor:public ActionSmoothMqtt
	{
		ActionMotor(std::vector<Key> K, Mosquitto * MOSQUITTO, const char * TOPIC);
	};
	struct ActionVztFan:public ActionSmoothMqtt
	{
		ActionVztFan(std::vector<Key> K, Mosquitto * MOSQUITTO, const char * TOPIC);
	};

	struct Abs:Typeing
	{
		const char * name;
		Key key=NULLKEY;
		std::vector<Key> lightKey;
		std::vector<Key> lightBoostKey;
		std::vector<Key> colorKey;
		Key invertKey=NULLKEY;
		std::vector<std::pair<Key,const char *> > execKey; // TO DELETE
		std::vector<Action*> actions;

		Abs(const char * NAME, Key KEY=NULLKEY, std::vector<Key> LIGHTKEY={}, std::vector<Key> LIGHTBOOSTKEY={},std::vector<Key> COLORKEY={}, Key INVERTKEY=NULLKEY, std::vector<Action*> ACTIONS={});

		char allName[STRLEN]="";
		std::unordered_set<Mosquitto *> mosquitto;
		bool mosquitto_calculated=0; // Temporary in tree calc; finally should by true
		Abs * up=NULL;
		ld maxLightBoost = 1;
		bool guiHidden = false;
		bool controlAsDevice = false;
		Abs * setControlAsDevice();
		Abs * guiHide();
		virtual Abs * evalRelativePath(const char * path);
	};

	struct Channel
	{
		double rgb[3];
	};

	struct ChannelDMX: public Channel
	{
		int card;
		int port;
	};


	struct Device:public Abs
	{

		Device(Abs A);
	};

	struct DeviceVirtual:public Device
	{
		DeviceVirtual(Abs A);
	};

	using FuncChannelToRgb = std::function<void(double * channel, double * rgb)>;
	using FuncRgbToChannel = std::function<void(double * rgb, double * channel)>;
	struct DeviceReal:public Device
	{
		Maschine * maschine=0;
		DeviceReal(Abs A);
		int len=-1;
		std::vector<PostHookOnStrip> postHook;
		std::vector<PreHookOnStrip> preHook;
		DeviceReal * addPreHook(PreHook * in, void * context = 0);
		DeviceReal * addPostHook(PostHook * in, int start=0, int end=-1, int step=1, void * context = 0);
		std::vector<Channel *> channels;
		int channels_len;
		FuncRgbToChannel rgbToChannel;
		FuncChannelToRgb channelToRgb;
		template<class T> DeviceReal * set_rgbToChannel(T f)
		{
			rgbToChannel = f(this);
			return this;
		}
		template<class T> DeviceReal * set_channelToRgb(T f)
		{
			channelToRgb = f(this);
			return this;
		}
		bool rescaleLightByColor = 0; // If true, all colors wil have the same light

		static FuncChannelToRgb base_channelToRgb(DeviceReal *);
		static FuncRgbToChannel base_rgbToChannel(DeviceReal *);
		static FuncRgbToChannel oneChannel_rgbToChannel(DeviceReal *d);
		static FuncRgbToChannel alwaysOn_rgbToChannel(DeviceReal *d);
		static FuncRgbToChannel linerarSpaceChannels_rgbToChannel(DeviceReal *d);
	};


	struct DeviceNeoPixel:public DeviceReal
	{
		int port=-1;
		int check=-1;
		DeviceNeoPixel * set(Maschine * M,int P,int L,int CHECK=-1);
		DeviceNeoPixel(Abs A);

	};
	struct DeviceDMX:public DeviceReal
	{
		std::vector<ChannelDMX *> channels; // HACK DeviceReal::channels is wisile too
		DeviceDMX * set(Maschine * M);
		DeviceDMX * add_channel(double RGB[3], int C, int P);
		DeviceDMX * add_channel(double R, double G, double B, int C, int P);
		DeviceDMX(Abs A);
	};

	struct Part:public Abs
	{
		std::vector<Abs*> content;
		std::unordered_map<std::string, Abs*> contentByName;
		const char * modeSufix="";
		Part(Abs A);
		Part *calc();
		Part * add(Abs * in);
		Part * guiHide();
		Part * setControlAsDevice();
		virtual Abs * evalRelativePath(const char * path);
	};

	struct Link:public Abs
	{
		const char * relativePath;
		Abs * target;
		Link * setPath(const char * p);
		Link(Abs A);
		virtual Abs * evalRelativePath(const char * path);
	};

	struct PowerControl
	{
		const char * power_control_name;
		Mosquitto * mosquitto=0;
		int pin=-1;
		char state_fname[FNAMELEN];
		PowerControl(const char * N,Mosquitto * M, int P);
	};
	extern std::unordered_map<std::string,PowerControl *> power_control_map;


	ld2d parse2D(const char * in);
	void createStrFrom2D(ld2d in, char * out);

	extern Part *root;
	extern std::map<std::string, std::vector<Hook*>> hooksForDoc;
}


#endif
