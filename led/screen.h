#ifndef SCREEN_H_INCLUDE
#define SCREEN_INCLUDE

#include <signal.h>
#include <utility> // std::pair
#include <vector>
#include <functional>

using namespace std;

const int SIZEPRIORLEN = 10;

void loadScreenSize(int nope=0);

void initTermios(int echo);
void resetTermios();

struct SizePrior
{
	int r=0; // needed size
	int a[SIZEPRIORLEN]; // ask for reaming space (big index => small priority)

	SizePrior();
	int & operator [] (int i);
	SizePrior operator () (int i,int v=1); // add to a
	void operator += (SizePrior d); // add to SizePrior
	void operator *= (SizePrior d); // maximum of two SizePrior
	pair<SizePrior,SizePrior> toSize(int size);
	int eval(pair<SizePrior,SizePrior> &in);
};

struct Box
{
	int x,y;
	int h,w;
	int mouseMode=2; // 0 - disable; 1 - only in; 2 - magnetic
	vector<Box * > in;
	virtual SizePrior minH();
	virtual SizePrior minW();
	virtual void calcPlace(int X,int Y,int H,int W);
	virtual void draw()=0;
	virtual void drawSoft();
	virtual void mouse(int t,int X,int Y);
	virtual void hide();
};

struct HVBox:public Box
{
	bool border;
	HVBox(bool Border=1);
	void drawOutBox();
	virtual void drawBox()=0;
	virtual void draw();
	virtual void drawSoft();
	virtual void mouse(int t,int X,int Y);
};
struct VBox:public HVBox
{
	VBox(bool Border=1);
	virtual void drawBox();
	virtual SizePrior minH();
	virtual SizePrior minW();
	virtual void calcPlace(int X,int Y,int H,int W);
};
struct HBox:public HVBox
{
	HBox(bool Border=1);
	virtual void drawBox();
	virtual SizePrior minW();
	virtual SizePrior minH();
	virtual void calcPlace(int X,int Y,int H,int W);
};

struct BBox:public Box
{
	const char * name;
	SizePrior minHSP;
	SizePrior minWSP;
	std::function<void(int,int,int)> f;
	BBox(const char * Name="BBox",std::function<void(int,int,int)> F=[](int t,int x,int y){},
			SizePrior MinHSP=SizePrior()(1),SizePrior MinWSP=SizePrior()(1));
	virtual char getEmptyCh();
	const char * color= "\e[44;97m";
	virtual void draw();
	virtual SizePrior minH();
	virtual SizePrior minW();
	virtual void mouse(int t,int X,int Y);
};

struct NoBox:public BBox
{
	NoBox(SizePrior MinHSP=SizePrior()(1),SizePrior MinWSP=SizePrior()(1));
	const char * color= "\e[44;97m";
	virtual void draw();
	virtual void mouse(int t,int X,int Y);
};
#endif
