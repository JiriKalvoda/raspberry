#ifndef LED_WITHREAD_H_INCLUDE
#define LED_WITHREAD_H_INCLUDE

#include "led-include.h"
#include <functional>

#define DMQTT if(0)

using ld=double;



struct LEDWithRead : public LED
{
	LEDWithRead();

	struct Read
	{
		std::vector<Read *>  predecessor;
		void addPredecessor(Read * p);
		std::vector<Read *> descendant;
		Tree::Device * dev=0;
		Tree::DeviceReal * devReal=0;
		Tree::Part * part=0;
		char light[Tree::FLEN];
		char r[Tree::FLEN];
		char g[Tree::FLEN];
		char b[Tree::FLEN];
		char lightSpeed[Tree::FLEN];
		char colorSpeed[Tree::FLEN];
		char ** channels;
		int channels_len=0;
		char ** byId=0;
		ld lightNum=-2;
		ld rNum=-2;
		ld gNum=-2;
		ld bNum=-2;
		ld lightSpeedNum=-2;
		ld colorSpeedNum=-2;
		ld * channelsNum;
		ld ** byIdNum=0;
		int dataVersion=0;
		Read(Read * Pred);
		ld strToNum(char * in);
		void calc();
		void aloc();
		void dealoc();
		void setDev(Tree::Device *d);
	};

	std::vector<std::function<void()> > everyNew;

	std::unordered_map<std::string,Read *> readByName;
	std::unordered_map<Tree::Abs *,Read *> readByTreePtr;
	std::vector<Read *>  reads;

	void loadReadTree_Device(Tree::Device * in,Read * predecessor=0);
	void loadReadTree_Part(Tree::Part * in,Read * predecessor=0);
	void loadReadTree(Tree::Abs * in,Read * predecessor=0);

	void delivered(MQTTClient_deliveryToken dt);
	int msgarrvd(char *topicName, int topicLen, MQTTClient_message *message, Mqtt * mqtt);
	virtual MQTTClient connect(Mqtt * mqtt,bool cleansession=1,bool noErr=0);
	std::unordered_map<Tree::Mosquitto *, bool> mqttHaveConnectThread;
	virtual void connectThread(Tree::Mosquitto * mosquitto,bool reconnect=0);
	virtual void connectTree(Tree::Abs * part,bool byNewThread=0);
	void connlost(char *cause,Mqtt * mqtt);
	void subscribe(Tree::Mosquitto *mosquitto, const char * topic);
	const char * readTopic(Tree::Mosquitto *mosquitto, const char * topic);
};


#endif
