#ifndef COLORS_H_INCLUDE
#define COLORS_H_INCLUDE

struct Color
{
	char name[30];
	int rgb[3];
};

const int colorsLen=36;

Color colors(int i);

void colorRedScaleWhiteYellow(double p,int rgb[3]);
void colorRedScaleYellowRed(double p,int rgb[3]);
void colorRedScale(double p,int rgb[3]);

void colorButtonRedScale(int v, int maxVal,double rgb[3]);
double colorButtonLight(int v, int maxVal);

#endif
