#include "colors.h"
#include <algorithm>

Color colors(int i)
{
	if(i>=12)
	{
		static Color colors[] =
			{
				{"Auburn",	113,35,29},
				{"White",	255,255,255},
				{"Red", 	255,0,0},
				{"Lime",	0,255,0},
				{"Blue",	0,0,255},
				{"Yellow",	255,255,0},
				{"Cyan",	0,255,255},
				{"Magenta",	255,0,255},
				{"Silver",	192,192,192},
				{"Gray",	128,128,128},
				{"Maroon",	128,0,0},
				{"Olive",	128,128,0},
				{"Green",	0,128,0},
				{"Purple",	128,0,128},
				{"Teal",	0,128,128},
				{"Navy",	0,0,128},
				{"orange_red",	255,69,0},
				{"orange",	255,165,0},
				{"gold",	255,215,0},
				{"golden_rod",	218,165,32},
				{"dark_orchid",	153,50,204},
				{"hot_pink",	255,105,180},
				{"chocolate",	210,105,30},
				{"green_yellow",173,255,47}
			};
		return colors[i-12];
	}
	Color color;
	if(i<8)
	{
		color.rgb[0]=255;
		color.rgb[1]=std::max(0,255*(7-i)/7/2+256/2);
		color.rgb[2]=std::max(0,255*(7-i)/7);
	}
	else
	{
		i-=8;
		color.rgb[0]=255;
		color.rgb[1]=std::max(0,255*(3-i)/4/2);
		color.rgb[2]=0;
	}
	return color;
}

void colorRedScaleWhiteYellow(double p,int rgb[3])
{
		rgb[0]=255;
		rgb[1]=std::max(0,int(255*(1-p)/2+256/2));
		rgb[2]=std::max(0,int(255*(1-p)));
}
void colorRedScaleYellowRed(double p,int rgb[3])
{
		rgb[0]=255;
		rgb[1]=std::max(0,int(255*(1-p)/2));
		rgb[2]=std::max(0,int(0));
}
void colorRedScale(double p,int rgb[3])
{
	double i=p*11;
	if(i<=7)
	{
		colorRedScaleWhiteYellow(i/7,rgb);
	}
	else
	{
		colorRedScaleYellowRed((i-7)/4,rgb);
	}
}

void colorButtonRedScale(int v, int maxVal,double rgb[3])
{
	if(maxVal == 0)
		rgb[0]=rgb[1]=rgb[2]=1;
	else
	if(maxVal == 3)
	{
		rgb[0]=rgb[1]=rgb[2]=1;
		if(v==1) rgb[0]=1, rgb[1]=0.82 ,rgb[2]=0.64;
		if(v==2) rgb[0]=1, rgb[1]=0.60 ,rgb[2]=0.20;
		if(v==3) rgb[0]=1, rgb[1]=0 ,rgb[2]=0;
	}
	else
	{
		int rgb_int[3];
		colorRedScale(v/(double)maxVal, rgb_int);
		for(int i=0;i<3;i++) rgb[i]=rgb_int[i]/(double)255;
	}
}
double colorButtonLight(int v, int maxVal)
{
	if(maxVal==0) return 0;
	return v/(double)maxVal;
}
