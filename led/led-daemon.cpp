//#include <vector>
#define USING_POST_HOOK
#include <string.h>
#include <algorithm>
#include "../lib/gpio.h"
#include <math.h>
#include <semaphore.h>
#include <unistd.h>
#include <random>
#include <poll.h>
#include <stdarg.h>


#include <fcntl.h>


#include <MQTTClient.h>

#include "deviceTree.h"
#include "postHooks.h"

#ifdef OUTPUT_NEOPIXEL
#define OUTPUT_TYPE NeoPixel
#define      DeviceActual      DeviceNeoPixel
#define TYPE_DeviceActual TYPE_DeviceNeoPixel
#else
#ifdef OUTPUT_DMX
#define OUTPUT_TYPE DMX
#define      DeviceActual      DeviceDMX
#define TYPE_DeviceActual TYPE_DeviceDMX
#else
#error Not implemented OUTPUT
#endif
#endif

#define fo(a,b) for(int a=0;a<(b);++a)

using Tree::FNAMELEN;
using Tree::FLEN;

using Tree::MQTTPREFIX;
using Tree::ChLArr;
using Tree::DataArr;
using Tree::PostHook;
using Tree::PreHook;
using Tree::Hook;
using Tree::PostHookOnStrip;
using Tree::PreHookOnStrip;
using Tree::InputArr;

using Tree::ld;
using Tree::ld2d;

using Gpio::p;
using Gpio::pinModeInput;
using Gpio::pinModeOutput;
using Gpio::digitalWrite;
using Gpio::digitalRead;

const Tree::Maschine * maschine; // Coresponding to deviceTree.h

#ifdef OUTPUT_NEOPIXEL
const int MAX_OUT_VAL = 255;
#endif
#ifdef OUTPUT_DMX
const int MAX_OUT_VAL = 64;
#endif

void __attribute__((noreturn)) __attribute__((format(printf,1,2)))
exit_errno(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "Oh dear, something went wrong with");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "! %s\n", strerror(errno));
	exit(1);
}


unsigned long long micros()
{
	struct timespec monotime;
	clock_gettime(CLOCK_MONOTONIC, &monotime);
	return 1000000 * (unsigned long long)monotime.tv_sec + monotime.tv_nsec/1000;

}

MQTTClient mqtt;
std::unordered_map<std::string,char *> mqttMap;
std::mutex mqttMapMutex;
bool mqttConnLost=0;

#define DOUTPUT if(0)
#define DMQTT if(0)
#define DDEVICE if(0)
#define DLOAD if(0)
#define DMAIN if(0)

void sem_all(sem_t * sem,int ms=-1)
{//wait and null semaphore
	if(ms==-1)
		sem_wait(sem);
	else
	{
		struct timespec ts;
		if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
		{
			perror("clock_gettime");
			exit(EXIT_FAILURE);
		}
		long long x = ms + (long long)ts.tv_nsec/1'000'000;
		ts.tv_sec +=  x/1000;
		ts.tv_nsec = x%1000*1'000'000;
		sem_timedwait(sem, &ts);
	}
	int semval;
	sem_getvalue(sem,&semval);
	for(;semval>0;semval--) sem_wait(sem);
}

namespace DELAY
{ // in MS
	int SHORT=5;
#ifdef OUTPUT_DMX
	int LONG=500;
#else
	int LONG=300'000;
#endif
	int LONG_CHANGE_TIME=SHORT;
	int WRITEWITHOUTCHANGE=LONG-10'000;
}

int MAXWRONGWRITEINLINE = 100;


std::vector<std::vector<ld>> load2D(const char * fname)
{// Load two dimensional array from mqtt
	char in[FLEN];
	{
		std::lock_guard<std::mutex> guard(mqttMapMutex);
		if(!mqttMap.count(fname))
		{
			DLOAD printf("LOAD %s FAILD\n",fname);
			return Tree::parse2D("");
		}
		strcpy(in,mqttMap[fname]);
	}
	return Tree::parse2D(in);
}


int serialFd;

bool shiftReg(int val)
{
	if(maschine->dpin==-1)
	{
		for(int i=0;i<int(maschine->directpins.size());i++)
			digitalWrite(p(maschine->directpins[i]), (val>>i)&1);
	}
	else
	{
	auto d = []{usleep(1);};
	for(int t=0;t<10;t++)
	{
		d();
		digitalWrite(p(maschine->lpin),0);
		d();
		for(int i=7;i>=0;i--)
		{
			digitalWrite(p(maschine->dpin),(val>>i)&1);
			d();
			digitalWrite(p(maschine->cpin),1);
			d();
			digitalWrite(p(maschine->cpin),0);
			d();

		}
		digitalWrite(p(maschine->lpin),1);
		digitalWrite(p(maschine->dpin),0);
		d();
		int check=0;
		for(int i=7;i>=0;i--)
		{ // Check, that output is correct
			check |= digitalRead(p(maschine->checkpin))<<i;
			d();
			digitalWrite(p(maschine->cpin),1);
			d();
			digitalWrite(p(maschine->cpin),0);
			d();
		}
		d();
		if(check == val) return 1;
		printf("shiftReg faild %d -> %d (%d/10)\n",val,check,t);
		fflush(stdout);
	}
	}
	return 0;
}

struct Strip
{
	ChLArr state;
	DataArr raw;
	DataArr lastRaw;

	Tree::DeviceActual *config;
	const char * name;
	int len;

	int wrongWriteLimit=0;
	int wrongWriteInLine=0;

	Strip(Tree::DeviceActual * CONFIG,int LEN,const char * NAME);
	~Strip();

	auto initMqttFile(const char * n,const char * val);
	void initMqtt();
	auto load2DStrip(const char * in);

	int lastRunUs=-(1<<30);
	int lastWriteUs=-(1<<30);

	InputArr loadInputArr();
	bool prepareRecalc(ld * val, Tree::ld2d & in, ld * out, ld * maxim);
	void recalcVal(ld * val,ld speed,ld timeChange,ld * z,ld maxim);
	void stateToRaw();
	void writeOutput();
	int run(bool lastShort);
};

Strip::Strip(Tree::DeviceActual * CONFIG,int LEN,const char * NAME) 
{
	config=CONFIG;
	len=LEN;
	name=NAME;
	state.aloc(len, config->channels_len);
	raw.aloc(len, config->channels_len, MAX_OUT_VAL);
	lastRaw.aloc(len, config->channels_len, MAX_OUT_VAL);
}

Strip::~Strip()
{
	state.dealoc();
	raw.dealoc();
	lastRaw.dealoc();
}

auto Strip::initMqttFile(const char * n,const char * val)
{
	char fname[FNAMELEN];
	sprintf(fname,"%smaster/%s/%s",MQTTPREFIX,name,n);
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;
	pubmsg.payload = (void *)val;
	pubmsg.payloadlen = strlen(val);
	pubmsg.qos = 1;
	pubmsg.retained = 1;
	MQTTClient_publishMessage(mqtt, fname, &pubmsg, &token);
	int rc = MQTTClient_waitForCompletion(mqtt, token, 1000);
	if(rc != MQTTCLIENT_SUCCESS)
	{
		printf("MQTT Write error");
		exit(101);
	}
}

void Strip::initMqtt()
{
	initMqttFile("r","1");
	initMqttFile("g","1");
	initMqttFile("b","1");
	ld rgb[3], channels[config->channels_len];
	fo(i,3) rgb[i]=1;
	config->rgbToChannel(rgb,channels);
	fo(i,config->channels_len)
	{
		char fname[100], val[100];
		sprintf(fname,"ch%d",i);
		sprintf(val, "%lf", channels[i]);
		initMqttFile(fname, val);
	}
	initMqttFile("light","0");
	initMqttFile("colorSpeed","1");
	initMqttFile("lightSpeed","2");
	char fname[FNAMELEN];
	sprintf(fname,"%smaster/%s/#",MQTTPREFIX,name);
	MQTTClient_subscribe(mqtt, fname, 1);
}

auto Strip::load2DStrip(const char * in)
{
	char fname[FNAMELEN];
	sprintf(fname,"%smaster/%s/%s",MQTTPREFIX,name,in);
	return load2D(fname);
}


#ifdef OUTPUT_DMX
struct Card
{
	bool do_write;
	int data[4];
};
std::unordered_map<int, Card> cards;
void finalizeOutput()
{
	for(std::pair<const int, Card> &it: cards)
	{
		if(it.second.do_write)
		{
			char out[100];
			int outLen = 0;
			out[outLen++] = 253;
			out[outLen++] = it.first;

			for(int i=0;i<4;i++)
				out[outLen++] = it.second.data[i];

			out[outLen++] = 49;
			int csum = 0;
			for(int i=1;i<outLen;i++) // do not calculate from first byte
			{
				int k = 3;
				csum = (((csum) << (k)) | ((csum) >> (8-(k)))) & 0xFF;
				csum ^= out[i];
			}
			out[outLen++] = csum;
			for(int i=0;i<outLen;i++)
			{
				//printf("%d ", out[i]);
				//fflush(stdout);
				write(serialFd, out+i, 1);
				usleep(1 * 1000);
			}
			//printf("\n");
			it.second.do_write = 0;
		}
	}
}
#endif

bool dataAvailable(int fd)
{
	struct pollfd poll_data[1];
	poll_data[0].fd = fd;
	poll_data[0].events = POLLIN;
	if(poll(poll_data, 1, 0)<0) exit_errno("poll()");
	return poll_data[0].revents & POLLIN;
}

void Strip::writeOutput()
{
	DOUTPUT {
		for(int i=0;i<config->channels_len;i++)
			printf("%d ", raw.data[i][0]);
		printf("%s\n", name);
	}
#ifdef OUTPUT_NEOPIXEL
	char data[3*len];
	for(int i=0;i<len;i++)
	{
		data[3*i+0] = raw.data[1][i];
		data[3*i+1] = raw.data[0][i];
		data[3*i+2] = raw.data[2][i];
	}
	shiftReg(1<<config->port);
	char out[len*8*3/3+10];
	int outIndex=0;
	for(int i=0,j=7;i<3*len;)
	{
		int d[3];
		for(int k=0;k<3;k++)
		{
			d[k] = (data[i]>>j)&1;
			j--;
			if(j<0){j=7;i++;}
		}
		out[outIndex++]=~((d[0]<<0)+0+(1<<2)+(d[1]<<3)+0+0+(1<<6)+(d[2]<<7));
	}
	if(config->check>=0)
	{
		out[outIndex++]=1;
		out[outIndex++]=255;
	}
	out[outIndex]=0;
	for(int tryIt=0;;tryIt++)
	{
		if(config->check>=0)
		{
			while(dataAvailable(serialFd)) 
			{
				char buf [1000];
				if(read(serialFd, buf, 900)<0) exit_errno("read()");
			}

			//while(serialDataAvail(serialFd)>0) serialGetchar(serialFd);
		}
		write(serialFd,out, outIndex);
		usleep(len*8*3*10/3*10/20+10);
		if(config->check < 0) break;
		if(!dataAvailable(serialFd))
			printf("ledWriteData %s fail error %d [%3d/%3d]\n",name,0,wrongWriteInLine,wrongWriteLimit);
		else
		{
			auto byteprint = [](int in){putchar('l');for(int i=0;i<8;i++) putchar('0'+(((~in)>>i)&1));putchar('o');};
			int checkDataArrLen = 8*config->check+1000;
			char checkData[checkDataArrLen];
			int checkLen = read(serialFd, checkData, checkDataArrLen);
			if(checkLen<0) exit_errno("read()");
			if(checkLen>=1 && checkLen<=8*config->check+1)
			{
				wrongWriteInLine=0;
				shiftReg(0);
				return;
			}
			else
			{
				printf(  "ledWriteData %s faild ",name);
				for(int i=8*(len-config->check);out[i];i++) byteprint(out[i]);
				printf("\n[%3d/%3d]     -> ",wrongWriteInLine,wrongWriteLimit);
				for(int i=0;i<checkLen;i++)
				{
					if(i==10 && i<checkLen-12) {i=checkLen-5;printf("..(%d)..",checkLen);}
					byteprint(checkData[i]);
				}
				printf("\n");
			}
		}
		wrongWriteInLine++;
		if(wrongWriteInLine >= wrongWriteLimit)
		{
			printf("TOTAL FATAL!\n");
			fflush(stdout);
			break;
		}
		if(tryIt>=2)
		{
			printf("FATAL!\n");
			fflush(stdout);
			break;
		}
		usleep(100);
	}
	shiftReg(0);
	return;
#endif // OUTPUT_NEOPIXEL
#ifdef OUTPUT_DMX
	int i=0;
	for(Tree::ChannelDMX * it: config->channels)
	{
		cards[it->card].do_write = 1;
		cards[it->card].data[it->port]=raw.data[i][0];
		i++;
	}
	wrongWriteInLine=0; // there is no check
#endif // OUTPUT_DMX
}

bool Strip::prepareRecalc(ld * val, Tree::ld2d & in, ld * out, ld * maxim)
{
	bool change=0;
	for(int i=0;i<len;i++)
	{
		int j=i*in.size()/len;
		ld & v = in[j][i%in[j].size()];
		ld z = v-val[i];
		out[i]=z;
		if(maxim) *maxim = std::max(*maxim,std::abs(z));
		change |= (z < -0.0001 || z> 0.0001);
	}
	return change;
}

void Strip::recalcVal(ld * val,ld speed,ld timeChange,ld * z,ld maxim)
{
	for(int i=0;i<len;i++)
	{
		if(timeChange&&speed)
			val[i]+=z[i]*speed*timeChange/maxim;
		else
		if(!speed)
			val[i]+=z[i];
	}
}

int limitTo255(int val)
{
	if(val>MAX_OUT_VAL) return MAX_OUT_VAL;
	if(val<0)   return 0;
	return val;
}
void Strip::stateToRaw()
{
	for(int i=0;i<len;i++)
	{
		ld x = pow(1.0/(MAX_OUT_VAL+1),1.0/3);
		ld raw_light = state.light[i];
		if(raw_light < 1)
			raw_light = raw_light*(1-x)+x;
		ld light = pow(raw_light,3)*MAX_OUT_VAL;
		//if(i==0) printf("light %lf | %lf -> %lf\n", state.light[i], x, light);
		if(config->rescaleLightByColor)
		{
			ld sum = 0;
			ld max = 0;
			for(int ch=0;ch<raw.ch;ch++)
				sum += state.data[ch][i], max = std::max(state.data[ch][i], max);
			if(sum>1e-7) light*=max/sum;
		}
		int overLimit = 0;
		fo(ch,raw.ch)
		{
			raw.data[ch][i] =state.data[ch][i]*light;
			overLimit += std::max(raw.data[ch][i]-MAX_OUT_VAL,0);
		}
		fo(ch,raw.ch)
		{
			int modif = std::max(std::min(MAX_OUT_VAL-raw.data[ch][i], overLimit),0);
			raw.data[ch][i]+= modif;
			overLimit -= modif;
		}
		fo(ch,raw.ch) raw.data[ch][i] = limitTo255(raw.data[ch][i]);
	}
}

InputArr Strip::loadInputArr()
{
	InputArr input(config->channels_len);
	input.colorSpeed = load2DStrip("colorSpeed")[0][0];
	input.lightSpeed = load2DStrip("lightSpeed")[0][0];
	fo(ch, config->channels_len)
	{
		char fname[100];
		sprintf(fname, "ch%d", ch);
		input.data[ch] = load2DStrip(fname);
	}
	input.light = load2DStrip("light");
	return input;
}

int Strip::run(bool lastShort)
{
	unsigned int timeUs = micros();
	ld timeChangeS = ld(timeUs-lastRunUs)/1'000'000;
	if(timeChangeS>0.3 || !lastShort) timeChangeS=DELAY::LONG_CHANGE_TIME/1000.0;
	InputArr input = loadInputArr();
	for(PreHookOnStrip it: config->preHook)
		it.prh->run(config, input, it.context);
	int nextRunInMs=1<<30;
	{
		ld Z[config->channels_len][len];
		ld lightZ[len];
		ld maxColorCange  = input.colorSpeed*timeChangeS;
		ld maxLightChange = input.lightSpeed*timeChangeS;
		for(int ch=0;ch<config->channels_len;ch++) for(int i=0;i<len;i++) Z[ch][i]=0;
		bool changing=0;
		fo(ch, config->channels_len)
			changing |= prepareRecalc(state.data[ch], input.data[ch], Z[ch], &maxColorCange);
		changing |= prepareRecalc(state.light,input.light,lightZ,&maxLightChange);
		if(changing) nextRunInMs=0;
		ld lightSum=0;
		for(int i=0;i<len;i++) lightSum += state.light[i];
		bool quickColorChange=lightSum<len*0.1;
		fo(ch, config->channels_len)
		{
			recalcVal(state.data[ch] ,quickColorChange?0:input.colorSpeed,timeChangeS,Z[ch]    ,maxColorCange );
		}
		recalcVal(state.light,input.lightSpeed ,timeChangeS,lightZ,maxLightChange);
	}
	stateToRaw();
	for(PostHookOnStrip &it : config->postHook)
	{
		for(int ch=0; ch<config->channels_len;ch++)
			for(int i = it.beginLed, j=0; i != it.endLed; i += it.step, j++)
				it.dataArr.data[ch][j] = raw.data[ch][i];
		nextRunInMs = std::min(nextRunInMs,it.ph->run(config, it.dataArr, it.context));
		for(int ch=0; ch<config->channels_len;ch++)
			for(int i = it.beginLed, j=0; i != it.endLed; i += it.step, j++)
				raw.data[ch][i] = it.dataArr.data[ch][j];
	}
	bool change = !(lastRaw == raw);
	if(change || int(timeUs-lastWriteUs)/1000>DELAY::WRITEWITHOUTCHANGE) {wrongWriteLimit=wrongWriteInLine+MAXWRONGWRITEINLINE;goto strip_run_go;}
	else if(wrongWriteInLine && wrongWriteInLine<wrongWriteLimit) goto strip_run_go;
	if(0)
	{
		strip_run_go:
		DMAIN printf("Write %s\n",name);
		lastWriteUs = timeUs;
		writeOutput();
		if(wrongWriteInLine && wrongWriteInLine<wrongWriteLimit) nextRunInMs=0;
	}
	lastRunUs = timeUs;
	if(change) lastRaw = raw;
	return nextRunInMs;
}

std::vector<Hook *> allHook;
std::vector<Strip*> strips;
sem_t runSem;

void load(Tree::Part * in)
{
	for(Tree::Abs * it_abs:in->content)
	{
		if(it_abs->type & Tree::TYPE_DeviceActual)
		{
			Tree::DeviceActual * it = (Tree::DeviceActual *)it_abs;
			if(it->len>0 && it->maschine==maschine)
			{
				 DDEVICE printf("DEVICE %s\n",it->allName);
				 for(PostHookOnStrip &jt: it->postHook)
				 {
					 jt.dataArr.aloc((jt.endLed - jt.beginLed)/jt.step, it->channels_len, MAX_OUT_VAL);
					 allHook.push_back(jt.ph);
				 }
				 for(PreHookOnStrip &jt: it->preHook)
				 {
					 allHook.push_back(jt.prh);
				 }
				strips.push_back(new Strip{it,it->len,it->allName});
			}
		}
		if(it_abs->type & Tree::TYPE_Part)
			load((Tree::Part*) it_abs);
	}
}

void init()
{
	load(Tree::root);
	std::sort(allHook.begin(),allHook.end());
	std::vector<Hook *> nph;
	for(Hook* it:allHook)
		if(nph.size()==0||it!=nph.rbegin()[0])
			nph.push_back(it);
	allHook=nph;
	fflush(stdout);

	Gpio::init();

#ifdef OUTPUT_NEOPIXEL
	{
		char tmp[1234];
		sprintf(tmp,"stty -F %s 2500000",maschine->serial);
		system(tmp);
	}
	if((serialFd=open(maschine->serial, O_WRONLY))<0){
		fprintf(stderr,"Unable to open serial device: %s\n",strerror(errno));
		exit(1);
	}
	if(maschine->dpin>=0) pinModeOutput(p(maschine->dpin));
	if(maschine->cpin>=0) pinModeOutput(p(maschine->cpin));
	if(maschine->lpin>=0) pinModeOutput(p(maschine->lpin));
	if(maschine->checkpin>=0) pinModeInput(p(maschine->checkpin));
	for(int i : maschine->directpins) pinModeOutput(p(i));
#endif // OUTPUT_NEOPIXEL
#ifdef OUTPUT_DMX
	{
		char tmp[1234];
		sprintf(tmp,"stty -F %s 9600 cs8 -cstopb -parenb",maschine->serial);
		system(tmp);
	}
	if((serialFd=open(maschine->serial, O_WRONLY))<0){
		fprintf(stderr,"Unable to open serial device: %s\n",strerror(errno));
		exit(1);
	}
#endif // OUTPUT_DMX

	for(auto & it: strips) it->initMqtt();
	for(Hook* it:allHook)
		for(const char * jt:it->neededMqtt)
			MQTTClient_subscribe(mqtt, jt, 1);
}


void delivered(void *context, MQTTClient_deliveryToken dt)
{
}
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    int i;
    DMQTT printf("Message arrived\n");
    DMQTT printf("     topic: %s\n", topicName);
    DMQTT printf("   message: ");
    char * payloadptr = (char *)message->payload;
    DMQTT for(i=0; i<message->payloadlen; i++)
    {
        putchar(payloadptr[i]);
    }
    DMQTT putchar('\n');
    char * in = new char [message->payloadlen+1];
    for(int i=0;i<message->payloadlen;i++)
	    in[i]=payloadptr[i];
    in[message->payloadlen]=0;
    char * old;
    std::string topic = std::string(topicName);
    {
	std::lock_guard<std::mutex> guard(mqttMapMutex);
	    old = mqttMap[topic];
	    mqttMap[topic]=in;
    }
    if(old) delete [] old;
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    sem_post(&runSem);
    return 1;
}
void connlost(void *context, char *cause)
{
    printf("\nMQTT connection lost\n");
    printf("     cause: %s\n", cause);
	mqttConnLost=1;
    sem_post(&runSem);
	exit(1);
}

void initMqtt()
{
	for(int i=0;;i++)
	{
		MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
		int rc;

		std::string addres = maschine->mosquitto->getAddres();
		std::string username = maschine->mosquitto->getUsername("led-daemon");
		std::string passwd = maschine->mosquitto->getPasswd("led-daemon");
		static char mqttConName[1234] = "";
		std::random_device rd;
		if(!mqttConName[0]) sprintf(mqttConName,"led-daemon-%d-%d",int(rd()),int(rd()));
		MQTTClient_create(&mqtt, addres.c_str(), mqttConName,
				MQTTCLIENT_PERSISTENCE_NONE, NULL);
		conn_opts.keepAliveInterval = 20;
		conn_opts.cleansession = 1;
		conn_opts.username = username.c_str();
		conn_opts.password = passwd.c_str();
		//conn_opts.password = "fnnvjfdnvjfnjkvnsiuuredvuj";
		MQTTClient_setCallbacks(mqtt, NULL, connlost, msgarrvd, delivered);

		if ((rc = MQTTClient_connect(mqtt, &conn_opts)) != MQTTCLIENT_SUCCESS && i==10)
		{
			printf("Failed to connect MQTT, return code %d\n", rc);
			exit(-1);

		}
		if(rc == MQTTCLIENT_SUCCESS) break;
		usleep(1000 * 1000);
	}
	printf("CONECT OK\n");
	fflush(stdout);
}

void usageExit(char * name)
{
#define PREPROC_XSTR(s) PREPROC_STR(s)
#define PREPROC_STR(s) #s
	printf(
			"This is program for writing data to to LED strips.\n"
			"Start me as daemon by executing:\n"
			"\n"
			"	%s <device_name>\n"
			"\n"
			"Configuration file is located at %s/deviceTree.h (apply charges need recompilation)\n"
			,name,PREPROC_XSTR(BUILDPATH));
	exit(0);
#undef PREPROC_XSTR
#undef PREPROC_STR
}

int main(int argc,char ** argv)
{
	if(argc!=2) usageExit(argv[0]);
	maschine = Tree::maschine_map[argv[1]];
	if(!maschine)
	{
		printf("Machine not found.\n");
		usageExit(argv[0]);
	}
	sem_init(&runSem, 0, 1);
	for(int i=1;i<argc;i++)
	{
		if(!strcmp(argv[i],"-h") || !strcmp(argv[i],"--help")) usageExit(argv[0]);
	}
	initMqtt();
	init();
	bool lastShort=0;
	printf("INIT OK\n");
	fflush(stdout);
	for(;;)
	{
		int nextRunInMs=DELAY::LONG;
		for(auto it : allHook) nextRunInMs = std::min(nextRunInMs,it->beforeRun());
		if(mqttConnLost) return 1;
		for(auto & it: strips) nextRunInMs = std::min(nextRunInMs,it->run(lastShort));
#ifdef OUTPUT_DMX
		finalizeOutput();
#endif
		for(auto it : allHook) nextRunInMs = std::min(nextRunInMs,it->afterRun());
		lastShort = nextRunInMs<=DELAY::SHORT;
		if(lastShort)
			nextRunInMs = DELAY::SHORT;
		DMAIN printf("WAIT %5dms\n",nextRunInMs);
		sem_all(&runSem,nextRunInMs);
	}
	sem_destroy(&runSem);
	return 0;
}
