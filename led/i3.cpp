#include "deviceTree.h"
#include<bits/stdc++.h>
using namespace std;
#ifdef DEB
#define D if(1)
#else
#define D if(0)
#endif

#define fo(a,b) for(int a=0;a<(b);++a)
using ll = long long;
const int STRLEN = 100000;
const char MODEPREFIX [] = "led-";

void fByType(Tree::Device * d,char *& out,const char * upMode="default");
void fByType(Tree::Part   * p,char *& out,const char * upMode="default");
template<class T>
void f(T * p,char *& out,const char * upMode="default")
{
	for(auto &it : p->execKey)
	{
#ifdef ADD_RPI_BACKLIGHT
		if(it.first == Tree::Key{6,2}) continue;
#endif
		out += sprintf(out,"bindcode ");
		out += it.first.i3Print(out);
		out += sprintf(out," exec \"");
		for(int i=0;it.second[i];i++)
			out += sprintf(out,it.second[i]=='\\'?"\\\\":it.second[i]=='"'?"\\\\\"":"%c",it.second[i]);
		out += sprintf(out,	"\"\n");
	}
	for(int i=0;i<4;i++)
		if(!p->colorKey[i].null)
		{
			out += sprintf(out,"bindcode ");
			out +=	p->colorKey[i].i3Print(out);
			out += sprintf(out," exec \"led %s c %d\"\n",p->allName,i);
		}
	for(int i=0;i<4;i++)
		if(!p->lightKey[i].null)
		{
			out += sprintf(out,"bindcode ");
			out +=	p->lightKey[i].i3Print(out);
			out += sprintf(out," exec \"led %s l %d\"\n",p->allName,i);
		}
	if(!p->invertKey.null)
	{
		out += sprintf(out,"bindcode ");
		out +=	p->invertKey.i3Print(out);
		out += sprintf(out," exec \"LED_SLOT=second LED_WRITEIF=2 led %s 0 1 1 1 ;led-op swap %s _ second OUTPUT\"\n",p->allName,p->allName);
	}
	char * buf=0;
	char * tmpOut=0;
	if(!p->key.null)
	{
		out += sprintf(out,"bindcode ");
		out +=	p->key.i3Print(out);
		out += sprintf(out," mode \"%s%s%s\" ",MODEPREFIX,p->allName,p->modeSufix);
		out += sprintf(out," ; exec i3-mode-log \"%s%s%s\" \n",MODEPREFIX,p->allName,p->modeSufix);
		{ buf = new char[STRLEN]; tmpOut=out;out=buf; }
		out += sprintf(out,"mode %s%s%s\n{\n"
			"bindsym $mod+Return mode \"%s\"; exec i3-mode-log \"%s\"\n"
			"bindsym $mod+Escape mode \"default\"; exec i3-mode-log \"default\"\n"
			"bindsym Return mode \"%s\" ; exec i3-mode-log \"%s\"\n"
			"bindsym Escape mode \"default\" ; exec i3-mode-log \"default\"\n"
				,MODEPREFIX,p->allName,p->modeSufix,upMode,upMode,upMode,upMode);
			out += sprintf(out,"bindcode %d exec \"led %s -1 -1 -1 -1\"\n",22,p->allName);
#ifdef ADD_RPI_BACKLIGHT
		out += sprintf(out,"bindcode 127 exec \"rpi-backlight -p toggle\"\n");
#endif
		upMode = new char [1000];
		snprintf((char *)upMode,999,"%s%s",MODEPREFIX,p->allName);
	}
	fByType(p,out,upMode);
	if(buf) 
		out += sprintf(out,"}\n");
	if(buf) { printf("%s\n",buf); delete [] buf;out=tmpOut;delete [] upMode;}
}
void fByType(Tree::Device * d,char *& out,const char * upMode)
{
	//out += sprintf(out,"%s\n",d->allName);
	if(!d->key.null)
	{
		for(int i=0;i<Tree::ModeSetKey::lightLen;i++)
		{
			out += sprintf(out,"bindcode ");
			out += Tree::ModeSetKey::light[i].i3Print(out);
			out += sprintf(out," exec \"led %s L %d\"\n",d->allName,i);
		}
		for(int i=0;i<Tree::ModeSetKey::colorLen;i++)
		{
			out += sprintf(out,"bindcode ");
			out += Tree::ModeSetKey::color[i].i3Print(out);
			out += sprintf(out," exec \"led %s C %d\"\n",d->allName,i);
		}
		for(int i=0;i<Tree::ModeSetKey::lightSpeedLen;i++)
		{
			out += sprintf(out,"bindcode ");
			out += Tree::ModeSetKey::lightSpeed[i].i3Print(out);
			out += sprintf(out," exec \"led %s s %d\"\n",d->allName,i);
		}
		for(int i=0;i<Tree::ModeSetKey::colorSpeedLen;i++)
		{
			out += sprintf(out,"bindcode ");
			out += Tree::ModeSetKey::colorSpeed[i].i3Print(out);
			out += sprintf(out," exec \"led %s S %d\"\n",d->allName,i);
		}
		out += sprintf(out,"bindcode ");
			out += Tree::ModeSetKey::swap.i3Print(out);
		out += sprintf(out," exec \"LED_SLOT=second LED_WRITEIF=2 led %s 0 1 1 1 ;led-op swap %s _ second OUTPUT\"\n",d->allName,d->allName);
	}
}
void fByType(Tree::Part   * p,char *& out,const char * upMode)
{
	for(auto & it:p->dev)
	{
		f(it,out,upMode);
	}
	for(auto & it:p->subPart)
	{
		f(it,out,upMode);
	}
}
std::unordered_map<std::string,Tree::Part *> partByName;
void loadPartByName(Tree::Part *in=Tree::root)
{
	if(!partByName.count(in->allName)) partByName[in->allName]=in;
	for(auto & it:in->subPart) loadPartByName(it);
}
int main(int argc, char ** argv)
{
	char * buf = new char[STRLEN];
	char * out = buf;
	loadPartByName();
	fByType(argc>1?partByName[argv[1]]:Tree::root,out);
	if(buf) { printf("%s\n",buf); delete [] buf;}
	return 0;
}

