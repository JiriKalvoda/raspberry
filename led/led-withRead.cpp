#include "led-withRead.h"
#include <thread>
#include <string.h>
#include <random>
#include <stdexcept>

const char *slot = "master";

int msgarrvdFunc(void *context, char *topicName, int topicLen, MQTTClient_message *message);
void deliveredFunc(void *context, MQTTClient_deliveryToken dt);
void connlostFunc(void *context, char *cause);

LEDWithRead::LEDWithRead(){}

LEDWithRead::Read::Read(Read * Pred)
{
	addPredecessor(Pred);
	aloc();
}
void LEDWithRead::Read::addPredecessor(Read * Pred)
{
	if(Pred)
	{
		predecessor.push_back(Pred);
		Pred->descendant.push_back(this);
	}
}
void LEDWithRead::Read::dealoc()
{
	delete channelsNum;
	for(int i=0;i<channels_len;i++)
	{
		delete [] channels[i];
	}
	delete [] byId;
	delete [] byIdNum;
}
void LEDWithRead::Read::aloc()
{
	byId = new char * [6+channels_len];
	channels = byId+6;
	byId[0]=light;
	byId[1]=r;
	byId[2]=g;
	byId[3]=b;
	byId[4]=lightSpeed;
	byId[5]=colorSpeed;
	byIdNum = new ld * [6+channels_len];
	byIdNum[0]=&lightNum;
	byIdNum[1]=&rNum;
	byIdNum[2]=&gNum;
	byIdNum[3]=&bNum;
	byIdNum[4]=&lightSpeedNum;
	byIdNum[5]=&colorSpeedNum;
	channelsNum = new double[channels_len];
	for(int i=0;i<channels_len;i++)
	{
		channels[i] = new char[Tree::FLEN];
		channels[i][0]=0;
		byIdNum[i+6] = channelsNum + i;
		channelsNum[i]=-2;
	}
}
ld LEDWithRead::Read::strToNum(char * in)
{
	for(int i=0;in[i];i++) if(in[i]==' ' || in[i]==':') return -3;
	ld num;
	if(1==sscanf(in,"%lF",&num)) return num;
	return -4;
}
void LEDWithRead::Read::setDev(Tree::Device *d)
{
	dealoc();
	channels_len = 0;
	dev = d;
	if(dev->type & Tree::TYPE_DeviceReal)
	{
		devReal = (Tree::DeviceReal*)dev;
		channels_len = devReal->channels_len;
	}
	aloc();
}
void LEDWithRead::Read::calc()
{
	//fprintf(stderr,"CALC %s | %s\n",dev?dev->allName:"---",part?part->allName:"---");
	if(descendant.size())
	{
		for(int i=0;i<6+channels_len;i++)
		{
			strcpy(byId[i],descendant[0]->byId[i]);
			for(int j=1;j<int(descendant.size());j++)
			{
				if(strcmp(byId[i],descendant[j]->byId[i])) byId[i][0]=0;
			}
		}
	}
	for(int i=0;i<6+channels_len;i++)
		*byIdNum[i] = strToNum(byId[i]);
	for(Read * it : predecessor) it->calc(); // Could be too slow
	dataVersion++;
}

void LEDWithRead::loadReadTree_Device(Tree::Device * in,Read * predecessor)
{
	if(!readByName.count(in->allName)) reads.push_back(readByName[in->allName] = new Read(predecessor));
	if(!readByName[in->allName]->dev)  readByName[in->allName]->setDev(in), readByTreePtr[in]=readByName[in->allName];
}

void LEDWithRead::loadReadTree_Part(Tree::Part * in,Read * predecessor)
{
	if(!readByName.count(in->allName)) reads.push_back(readByName[in->allName] = predecessor = new Read(predecessor));
	if(!readByName[in->allName]->part) readByName[in->allName]->part=in, readByTreePtr[in]=readByName[in->allName];
	for(auto & it:in->content)
	{
		loadReadTree(it,predecessor);
	}
	if(readByName.count("")) readByName["_"]=readByName[""];
}
void LEDWithRead::loadReadTree(Tree::Abs * in,Read * predecessor)
{
	if(readByTreePtr.count(in))
	{
		readByTreePtr[in]->addPredecessor(predecessor);
	}
	else
	{
		if(in->type & Tree::TYPE_Link) loadReadTree(((Tree::Link *)in)->target,predecessor);
		if(in->type & Tree::TYPE_Device) loadReadTree_Device((Tree::Device *)in,predecessor);
		if(in->type & Tree::TYPE_Part) loadReadTree_Part((Tree::Part*)in,predecessor);
	}
}

void LEDWithRead::delivered(MQTTClient_deliveryToken dt)
{
	DMQTT printf("Message with token value %d delivery confirmed\n", dt);
}

int LEDWithRead::msgarrvd(char *topicName, int topicLen, MQTTClient_message *message, Mqtt *mqtt)
{
	int i;
	DMQTT printf("Message arrived\n");
	DMQTT printf("     topic: %s\n", topicName);
	DMQTT printf("   message: ");
	char * payloadptr = (char *)message->payload;
	DMQTT for(i=0; i<message->payloadlen; i++)
	{
		putchar(payloadptr[i]);
	}
	DMQTT putchar('\n');
	char mqttPrefixSlot[Tree::FNAMELEN];
	sprintf(mqttPrefixSlot,"%s%s/",Tree::MQTTPREFIX,slot);
	if(!strncmp(topicName,mqttPrefixSlot,strlen(mqttPrefixSlot)))
	{
		char *deviceName=topicName+strlen(mqttPrefixSlot);
		for(int lastSlash=strlen(deviceName);lastSlash;lastSlash--)
			if(deviceName[lastSlash]=='/')
			{
				deviceName[lastSlash]=0;
				if(readByName.count(deviceName))
				{
					Read * read = readByName[deviceName];
					char * fname = deviceName+lastSlash+1;
					//fprintf(stderr,"%s|%s\n",deviceName,fname);
					if(!strcmp(fname,"r")) strcpy(read->r,payloadptr);
					if(!strcmp(fname,"g")) strcpy(read->g,payloadptr);
					if(!strcmp(fname,"b")) strcpy(read->b,payloadptr);
					if(!strcmp(fname,"light")) strcpy(read->light,payloadptr);
					if(!strcmp(fname,"colorSpeed")) strcpy(read->colorSpeed,payloadptr);
					if(!strcmp(fname,"lightSpeed")) strcpy(read->lightSpeed,payloadptr);
					if(!strncmp(fname,"ch", 2) && strlen(fname)==3)
					{
						int ch = fname[2]-'0';
						//printf("INPUT CH %d %s\n", ch, payloadptr);
						if(ch>=0 && ch<read->channels_len)
							strcpy(read->channels[ch],payloadptr);
					}
					read->calc();
					for(auto & it : everyNew) it();

				}
				break;
			}
	}
	else
	{
		std::lock_guard<std::mutex> guard(mqtt->dataMutex);
		const char * old = mqtt->data[topicName];
		char * a = new char[strlen(payloadptr)+1];
		strcpy(a, payloadptr);
		mqtt->data[topicName]=a;
		if(old)
			delete [] old;
		for(auto & it : everyNew) it();
	}
	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}
MQTTClient LEDWithRead::connect(Mqtt * mqtt,bool cleansession,bool noErr)
{
	int rc;
	for(int i=0;i<8;i++)
	{
		MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;

		std::string addres = mqtt->tree->getAddres();
		std::string username = mqtt->tree->getUsername("led-include");
		std::string passwd = mqtt->tree->getPasswd("led-include");

		//fprintf(stderr,"CONNECT %s START\n",addres.c_str());

		MQTTClient client  = 0;
		 std::random_device rd;
		sprintf(mqttConName,"led-include-read-%d-%d",int(rd()),int(rd()));
		MQTTClient_create(&client ,addres.c_str(), mqttConName,
				MQTTCLIENT_PERSISTENCE_NONE, NULL);

		MQTTClient_setCallbacks(client , mqtt, connlostFunc, msgarrvdFunc, deliveredFunc);

		conn_opts.keepAliveInterval = 10;
		conn_opts.cleansession = cleansession;
		conn_opts.username = username.c_str();
		conn_opts.password = passwd.c_str();
		if(noErr) conn_opts.connectTimeout=2;

		if ((rc = MQTTClient_connect(client , &conn_opts)) == MQTTCLIENT_SUCCESS)
		{
			//fprintf(stderr,"CONNECT %s OK\n",addres.c_str());
			char fname[Tree::FNAMELEN];
			sprintf(fname,"%s%s/#",Tree::MQTTPREFIX,slot);
			MQTTClient_subscribe(client , fname, 1);
			for(std::string it: mqtt->subscribe)
				MQTTClient_subscribe(client , it.c_str(), 1);
			return client;
		}
		MQTTClient_destroy(&client);
		//fprintf(stderr,"CONNECT %s NOPE\n",addres.c_str());
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
	if(!noErr)
	{
		printf("Failed to connect MQTT, return code %d\n", rc);
		throw std::runtime_error("MQTT Connect error");
	}
	return 0;
}

void LEDWithRead::connectThread(Tree::Mosquitto * mosquitto,bool reconnect)
{
	if(!mqttHaveConnectThread[mosquitto] || reconnect)
	{
		if(!mqttMap.count(mosquitto))
		{
			Mqtt * mqtt = mqttMap[mosquitto] = new Mqtt;
			mqtt->led = this;
			mqtt->tree = mosquitto;
		}
		auto t = new std::thread ([=]{this->connectAndWriteToMap(mosquitto,1,1);});
		t->detach();
		mqttHaveConnectThread[mosquitto] = 1;
	}
}

void LEDWithRead::connectTree(Tree::Abs * part,bool byNewThread)
{
		for(auto & mosquitto : part->mosquitto)
			if(!mqttMap.count(mosquitto))
			{
				if(byNewThread)
					connectThread(mosquitto);
				else
					connectAndWriteToMap(mosquitto);
			}
}

void LEDWithRead::connlost(char *cause,Mqtt * mqtt)
{
	//fprintf(stderr,"CONNLOST %s cause: %s\n",mqtt->tree->name,cause);
	for(Read * it : reads)
	{
		if(it->dev && it->dev->mosquitto.count(mqtt->tree))
		{
			for(int i=0;i<6;i++) strcpy(it->byId[i],"");
			it->calc();
		}
	}
	for(auto & it : everyNew) it();
	mqtt->connected = 0;
	connectThread(mqtt->tree,1);
}

void LEDWithRead::subscribe(Tree::Mosquitto *mosquitto, const char * topic)
{
	connectThread(mosquitto);
	Mqtt * mqtt = mqttMap[mosquitto];
	mqtt->subscribe.insert(topic);
	//fprintf(stderr,"SUBSCRUIBE %ld", mqtt->client);
	if(mqtt->client) 
		MQTTClient_subscribe(mqtt->client , topic, 1);
}
const char * LEDWithRead::readTopic(Tree::Mosquitto *mosquitto, const char * topic)
{
	if(!mqttMap.count(mosquitto)) return 0;
	Mqtt * mqtt = mqttMap[mosquitto];
	std::lock_guard<std::mutex> guard(mqtt->dataMutex);
	if(!mqtt->data.count(topic)) return 0;
	auto data = mqtt->data[topic];
	return data;
}


int msgarrvdFunc(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
	return ((LEDWithRead*)((LED::Mqtt*)context)->led)->msgarrvd(topicName,topicLen,message, (LED::Mqtt*)context);
}
void deliveredFunc(void *context, MQTTClient_deliveryToken dt)
{
	return ((LEDWithRead*)((LED::Mqtt*)context)->led)->delivered(dt);
}
void connlostFunc(void *context, char *cause)
{
	return ((LEDWithRead*)((LED::Mqtt*)context)->led)->connlost(cause,(LED::Mqtt*)context);
}
