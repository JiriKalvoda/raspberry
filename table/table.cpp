//#include <vector>
#include <string.h>
#include <algorithm>
#include <unistd.h>
#include <random>
#include <poll.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
#include <termios.h>
#include <time.h>


using ll = long long;
const ll LL_INF =  1000000000'000000;

void __attribute__((noreturn)) __attribute__((format(printf,1,2)))
exit_errno(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "Oh dear, something went wrong with ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "! %s\n", strerror(errno));
	exit(1);
}

void __attribute__((format(printf,1,2)))
lprintf(const char *fmt, ...)
{
#ifdef DEBUG
	va_list args;
	va_start(args, fmt);
	vfprintf(stdout, fmt, args);
	va_end(args);
#endif
}


ll millis()
{
	struct timespec monotime;
	clock_gettime(CLOCK_MONOTONIC, &monotime);
	return 1000 * (unsigned long long)monotime.tv_sec + monotime.tv_nsec/1000/1000;

}

char hex(int i)
{
	if(i<0 || i >= 16) return '?';
	if(i<10) return '0'+i;
	return 'A'+i-10;

}

int init_serial(const char * s)
{
	int serialFd;
	if((serialFd=open(s, O_RDWR | O_NOCTTY | O_NONBLOCK))<0) exit_errno("open");
	static struct termios old, new1;
    if(tcgetattr(serialFd, &old)<0) exit_errno("tcgetattr"); /* grab old terminal i/o settings */
    new1 = old; /* make new settings same as old settings */
    new1.c_lflag &= ~ICANON; /* disable buffered i/o */
    // new1.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
	new1.c_cc[VMIN] = 0;
	new1.c_cc[VTIME] = 0;
	cfsetspeed(&new1, B19200);
    if(tcsetattr(serialFd, TCSANOW, &new1)<0) exit_errno("tcsetattr"); /* use these new terminal i/o settings now */
	return serialFd;
}


struct CommandsParser
{
	int fd;
	char buf[1000];
	int buf_len=0;

	void do_read()
	{
		int r = 0;
		if((r = read(fd, buf+buf_len, 999 - buf_len))<0) exit_errno("read()");
		buf_len += r;
		int data_begin = 0;
		for(int i=0; i<buf_len; i++)
		{
			if(buf[i] == '\n')
			{
				buf[i] = 0;
				input_packet(buf + data_begin);
				data_begin = i+1;
			}
		}
		for(int i=0, j=data_begin;j<buf_len;i++,j++)
			buf[i] = buf[j];
		buf_len -= data_begin;
		if(buf_len > 900)
		{
			buf_len = 0;
		}
	}
	virtual void input_packet(char * msg) = 0;
};


struct Line
{
	int in_fd, out_fd;
	Line * contra_line;
	private:
	char buf[1000];
	char to_repeat[1000];
	int buf_len = 0;
	public:

	virtual void print_repeat(bool on_repeat_msg, const char * msg) = 0;
	virtual void print_input_err(const char * log) = 0;
	virtual void print(const char * msg, bool in, bool out) = 0;
	virtual const char * line_color() = 0;

	public:
	void do_read()
	{
		int r = 0;
		if((r = read(in_fd, buf+buf_len, 999 - buf_len))<0) exit_errno("read()");
		buf_len += r;
		bool in_packet = false;
		int data_begin = 0;
		for(int i=0; i<buf_len; i++)
		{
			if(!in_packet)
			{
				if(buf[i] == ':') in_packet = true;
				else
				{
					out_off_packet_ch(buf[i]);
					data_begin = i+1;
				}
			}
			else
			{
				if(buf[i] == ';')
				{
					buf[i] = 0;
					input_packet(buf + data_begin + 1);
					data_begin = i+1;
				}
			}
		}
		for(int i=0, j=data_begin;j<buf_len;i++,j++)
			buf[i] = buf[j];
		buf_len -= data_begin;
		if(buf_len > 900)
		{
			for(int i=0;i<buf_len;i++)
				out_off_packet_ch(i);
			buf_len = 0;
		}
	}
	private:
	void input_packet(char * packet)
	{
		int stl = strlen(packet);
		if(stl < 3)
		{
			char s[stl+100];
			sprintf(s, "Packet '%s' is too short", packet);
			print_input_err(s);
		}
		int sum = 0;
		for(int i=0;i<stl-2;i++)
			sum += packet[i];
		char check_sum[3];
		check_sum[0] = hex(sum/16%16);
		check_sum[1] = hex(sum%16);
		check_sum[2] = 0;
		if(packet[stl-2] != check_sum[0] || packet[stl-1] != check_sum[1])
		{
			char s[stl+100];
			sprintf(s, "Packet '%s' has bad check sum (has %s should be %s)", packet, packet+stl-2, check_sum);
			print_input_err(s);
		}
		else
		{
			packet[stl-2] = 0;
			if(!strcmp(packet, "A"))  // ACK
			{
				// TODO
			}
			else if(!strcmp(packet, "R"))  // ACK
			{
				contra_line->do_repeat();
			}
			else
			{
				contra_line->do_write("A");
				process(packet);
			}
		}
	}
	void out_off_packet_ch(char c)
	{
		char s[100];
		sprintf(s, "Out off packet char %d '%c'\n", c, c);
		print_input_err(s);
	}
	public:
	virtual void process(const char * msg)
	{
		return accept(msg);
	}
	void accept(const char * msg)
	{
		print(msg, true, true);
		do_write(msg);
	}
	void eat(const char * msg)
	{
		print(msg, true, false);
	}
	void send(const char * msg)
	{
		print(msg, false, true);
		do_write(msg);
	}
	private:
	void do_repeat()
	{
		print_repeat(true, to_repeat);
		do_write(to_repeat, false);
	}
	void do_write(const char * msg, bool save=true)
	{
		if(save)
			if(!!strcmp(msg, "A") && !!strcmp(msg, "R"))
				strcpy(to_repeat, msg);
		int stl = strlen(msg);
		char outbuf[stl+10];
		outbuf[0] = ':';
		int sum = 0;
		for(int i=0;i<stl;i++) sum += (outbuf[i+1] = msg[i]);
		outbuf[stl+1] = hex(sum/16%16);
		outbuf[stl+2] = hex(sum%16);
		outbuf[stl+3] = ';';
		stl += 4;
		if(write(out_fd, outbuf, stl)!=stl) exit_errno("write()");
	}
};

struct GreenLine: public Line
{
	virtual const char * line_color(){return "\e[92m";}
	virtual void print(const char * msg, bool in, bool out)
	{
		lprintf("S%sR%sO %s%s\e[39m\n", in?"->":"  ", out?"->":"  ", line_color(), msg);
	}
	virtual void print_input_err(const char * msg)
	{
		lprintf("S-XR  O %s**ERROR**\e[39m %s\n", line_color(), msg);
	}
	virtual void print_repeat(bool on_repeat_msg, const char * msg)
	{
		lprintf("S  R->O REPEAT: %s%s\e[39m\n", line_color(), msg);
	}
};

struct YellowLine: public Line
{
	virtual const char * line_color(){return "\e[93m";}
	virtual void print(const char * msg, bool in, bool out)
	{
		lprintf("S%sR%sO %s%s\e[39m\n", out?"<-":"  ", in?"<-":"  ", line_color(), msg);
	}
	virtual void print_input_err(const char * msg)
	{
		lprintf("S  RX-O %s**ERROR**\e[39m %s\n", line_color(), msg);
	}
	virtual void print_repeat(bool on_repeat_msg, const char * msg)
	{
		lprintf("S<-R  O REPEAT: %s%s\e[39m\n", line_color(), msg);
	}
};

int table_fd;
int controller_fd;

const char * SERIAL_TABLE = "/dev/ttyS1";
const char * SERIAL_CONTROLLER = "/dev/ttyS2";

//// MQTT
#include "../led/deviceTree.h"
#include <MQTTClient.h>
#define DMQTT if(1)
#define MQTT_PREFIX "table/1"
#define MQTT_NIGHT_MODE_TOPIC "jk/led-hook/night/enable"

int cmd_pipe[2];


MQTTClient mqtt;
void delivered(void *context, MQTTClient_deliveryToken dt)
{
}
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
	int i;
	printf("Message arrived\n");
	DMQTT printf("     topic: %s\n", topicName);
	DMQTT printf("   message: ");
	char * payloadptr = (char *)message->payload;
	DMQTT for(i=0; i<message->payloadlen; i++)
	{
		putchar(payloadptr[i]);
	}
	DMQTT putchar('\n');

	char * in = new char [message->payloadlen+1];
	for(int i=0;i<message->payloadlen;i++)
		in[i]=payloadptr[i];
	in[message->payloadlen]=0;

	if(!strcmp(topicName, MQTT_PREFIX "/action"))
	{
		write(cmd_pipe[1], in, strlen(in));
		if(strlen(in) && in[strlen(in)-1] != '\n')
			write(cmd_pipe[1], "\n", 1);
	}
	if(!strcmp(topicName, MQTT_NIGHT_MODE_TOPIC))
	{
		int val = atoi(in);
		const char * msg = val?"c1":"c0";
		write(cmd_pipe[1], msg, strlen(msg));
	}

	delete [] in;
	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}
void connlost(void *context, char *cause)
{
    printf("\nMQTT connection lost\n");
    printf("     cause: %s\n", cause);
	exit(1);
}
void initMqtt()
{
	auto mosquitto_dev = Tree::mosquitto_map["jug9"];
	for(int i=0;;i++)
	{
		MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
		int rc;

		std::string addres = mosquitto_dev->getAddres();
		std::string username = mosquitto_dev->getUsername("led-include");
		std::string passwd = mosquitto_dev->getPasswd("led-include");
		static char mqttConName[1234] = "";
		std::random_device rd;
		if(!mqttConName[0]) sprintf(mqttConName,"led-daemon-%d-%d",int(rd()),int(rd()));
		MQTTClient_create(&mqtt, addres.c_str(), mqttConName,
				MQTTCLIENT_PERSISTENCE_NONE, NULL);
		conn_opts.keepAliveInterval = 20;
		conn_opts.cleansession = 1;
		conn_opts.username = username.c_str();
		conn_opts.password = passwd.c_str();
		MQTTClient_setCallbacks(mqtt, NULL, connlost, msgarrvd, delivered);

		if ((rc = MQTTClient_connect(mqtt, &conn_opts)) != MQTTCLIENT_SUCCESS && i==10)
		{
			printf("Failed to connect MQTT, return code %d\n", rc);
			exit(-1);

		}
		if(rc == MQTTCLIENT_SUCCESS) break;
		usleep(1000 * 1000);
	}
	printf("CONECT OK\n");
	MQTTClient_subscribe(mqtt, MQTT_PREFIX"/action", 1);
	MQTTClient_subscribe(mqtt, MQTT_NIGHT_MODE_TOPIC, 1);
	fflush(stdout);
}

void mqtt_pub(const char * topic, const char * out, bool retained=false)
{
				MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;
	pubmsg.payload = (void *)out;
	pubmsg.payloadlen = strlen(out);
	pubmsg.qos = 0;
	pubmsg.retained = retained;
	MQTTClient_publishMessage(mqtt, topic, &pubmsg, &token);
}

//// MAIN LOGIC

ll wake_at;
void order_wake_at(ll at)
{
	ll t = millis();
	wake_at = t+(std::min(wake_at-t, at-t));
}

const int buttons_lens = 7;
const char * buttons_ids[buttons_lens] = {"UA", "DA", " 1", " 2", " 3", " 4", " S"};

bool buttons_in_state[buttons_lens];
int button_out=-1; // -1 = none or button index
ll button_out_relase_time;
//ll button_out_repeat_time;
struct YellowLine *yellow_line;
struct GreenLine *green_line;
struct Commands *commands_stdin;
struct Commands *commands_mqtt;

bool rewrite_display_on;
bool rewrite_display_off;
bool show_time_if_display_is_off;
char rewrite_display_msg[100];
char orig_display_msg[100];
char out_display_msg[100];
char out_display_mode[100];
ll display_change_time;

void push_display()
{
	display_change_time = millis()+LL_INF;
	char out[100];
	char mode[100];
	if(orig_display_msg[1]?rewrite_display_on:rewrite_display_off)
	{
		sprintf(out, "%s", rewrite_display_msg);
		sprintf(mode, "manual_rewrite");
	}
	else if(orig_display_msg[1]?show_time_if_display_is_off:false)
	{
		struct timespec spec;
		struct tm tm = *localtime(&spec.tv_sec);
		sprintf(out, "D%d%s%d%d",
				tm.tm_hour%10,
				tm.tm_sec%2?".":"",
				tm.tm_min/10, tm.tm_min%10);
		sprintf(mode, "time");
		int ms = spec.tv_nsec/1000000;
		display_change_time = millis()+(1000-ms);
	}
	else
	{
		sprintf(out, "%s", orig_display_msg);
		sprintf(mode, "original");
	}
	green_line->send(out);
	if(strcmp(out_display_msg, out) || strcmp(out_display_mode, mode))
	{
		strcpy(out_display_msg, out);
		strcpy(out_display_mode, mode);
	}
	order_wake_at(display_change_time);
}

void orig_display_msg_change(const char * msg)
{
	bool change = !!strcmp(orig_display_msg, msg);
	strcpy(orig_display_msg, msg);

	push_display();
	
	if(change)
	{
		char m[1234];
		int m_len = 0;
		for(int i=1;msg[i] && msg[i]!=','; i++)
		{
			m[m_len++] = msg[i];
		}
		m[m_len]=0;
		mqtt_pub(MQTT_PREFIX "/display_orig", m, true);
	}
}


void button_out_set(int id, int timeout=0)
// timeout -1 = do not change
// timeout 0 = default (25s -- max travel time)
{
	if(timeout == 0) timeout = 25'000;
	if(id != button_out)
	{
		if(button_out >= 0)
		{
			char m[123];
			sprintf(m, "K%sB", buttons_ids[button_out]);
			yellow_line->send(m);
		}
		button_out = id;
	}
	if(timeout != -1) button_out_relase_time = millis()+timeout;
	//button_out_repeat_time = millis()+100;
	if(button_out >= 0)
	{
		char m[123];
		sprintf(m, "K%sM", buttons_ids[button_out]);
		yellow_line->send(m);
	}
}

void buttons_in_state_change(int id, bool state)
{
	buttons_in_state[id] = state;

	button_out_set(state?id:-1, 3600'000);
}

void light_set(char to)
{
	char msg[]="L?";
	msg[1] = to;
	green_line->send(msg);
}

void loop()
{
	if(button_out != -1)
	{
		if(button_out_relase_time - millis() <= 0) button_out_set(-1);
		else
		{
			//if(button_out_repeat_time - millis() <= 0)
				//button_out_set(button_out, -1);
			order_wake_at(button_out_relase_time);
			//order_wake_at(button_out_repeat_time);
		}
	}
	if(display_change_time - millis() <= 0) push_display();
	else order_wake_at(display_change_time);
}


struct Commands: public CommandsParser
{
	void input_packet(char * msg)
	{
		lprintf("Command: %s\n", msg);
		if(msg[0]=='D')
		{
			rewrite_display_off = rewrite_display_on = true;
			strcpy(rewrite_display_msg, msg);
			push_display();
		}
		if(msg[0]=='d')
		{
			rewrite_display_off = rewrite_display_on = false;
			push_display();
		}
		if(msg[0]=='K')
		{
			int id, timeout;
			if(sscanf(msg, "K%d,%d", &id, &timeout)==2)
				button_out_set(id, timeout);
			else if(sscanf(msg, "K%d", &id)==1)
				button_out_set(id);
		}
		if(msg[0]=='L')
			light_set(msg[1]);
		if(msg[0]=='c')
		{
			show_time_if_display_is_off = atoi(msg+1);
			push_display();
		}
	}
};

struct YellowLineWithLogic: public YellowLine
{
	virtual void process(const char * msg)
	{
		if(msg[0] == 'K' && strlen(msg) == 4)
		{
			eat(msg);
			for(int i=0;i<buttons_lens;i++)
				if(msg[1] == buttons_ids[i][0] && msg[2] == buttons_ids[i][1])
					buttons_in_state_change(i, msg[3]=='M');
			return;
		}
		return accept(msg);
	}
};

struct GreenLineWithLogic: public GreenLine
{
	virtual void process(const char * msg)
	{
		if(msg[0] == 'D')
		{
			eat(msg);
			orig_display_msg_change(msg);
			return;
		}
		return accept(msg);
	}
};

int main(int argc,char ** argv)
{
	pipe(cmd_pipe);
	initMqtt();
	table_fd = init_serial(SERIAL_TABLE);
	controller_fd = init_serial(SERIAL_CONTROLLER);

	green_line  = new GreenLineWithLogic;
	green_line->in_fd  = table_fd;
	green_line->out_fd = controller_fd;
	yellow_line = new YellowLineWithLogic;
	yellow_line->out_fd  = table_fd;
	yellow_line->in_fd = controller_fd;

	yellow_line->contra_line = green_line ;
	green_line ->contra_line = yellow_line;

	commands_stdin = new Commands;
	commands_stdin->fd = 0;

	commands_mqtt = new Commands;
	commands_mqtt->fd = cmd_pipe[0];

	while(true)
	{
		wake_at = millis()+3600'000;
		loop();
		ll timeout = wake_at-millis();
		if(timeout < 0) timeout = 1;

		struct pollfd poll_data[4];
		poll_data[0].fd = table_fd;
		poll_data[0].events = POLLIN;
		poll_data[1].fd = controller_fd;
		poll_data[1].events = POLLIN;
		poll_data[2].fd = commands_stdin->fd;
		poll_data[2].events = POLLIN;
		poll_data[3].fd = commands_mqtt->fd;
		poll_data[3].events = POLLIN;
		lprintf("pool timeout %lld\n", timeout);
		if(poll(poll_data, 4, timeout)<0) exit_errno("poll()");
		if(poll_data[0].revents & POLLIN) green_line->do_read();
		if(poll_data[1].revents & POLLIN) yellow_line->do_read();
		if(poll_data[2].revents & POLLIN) commands_stdin->do_read();
		if(poll_data[3].revents & POLLIN) commands_mqtt->do_read();
	}
	return 0;
}
