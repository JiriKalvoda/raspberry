#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include<fstream>
using namespace std;
#define MAXTIMINGS	85
using ld = long double;
using ll = long long;
using ui = unsigned int;

#include <database.h>
#include <mysql++.h>

const int intervalTime=60*5;

	volatile ui times[MAXTIMINGS];
	volatile int timesLen;
	volatile bool interuptActive=0;
	void interupt(void)
	{
		if(interuptActive && timesLen < MAXTIMINGS)
			times[timesLen++]=micros();
	}
 
pair<ld,ld> read_dht11_dat(int pin)
{
	int dht11_dat[6] = { 0, 0, 0, 0, 0,0 };
	uint8_t laststate	= HIGH;
	int counter		= 0;
	uint8_t j		= 0, i;
	float	f; 
 
 
	pinMode( pin, OUTPUT );
	digitalWrite( pin, LOW );
	delay( 20 );
	digitalWrite( pin, HIGH );
	pinMode( pin, INPUT );
	timesLen=0;
	interuptActive=1;
	delay(50);
	interuptActive=0;
	for(int i=timesLen-1;i>0;i--) times[i]-=times[i-1];
	for(int i=0;i<timesLen;i++)
	{
		/*if(i>=3 && i%2)
			while(times[i]>100)
			{
				dht11_dat[j / 8] <<= 1;
				if(j%8==0) putchar(' ');
				putchar('X');
				j++;
				if(j>=48) goto endCalc;
				times[i]-=78;
			}
		if ( (i >= 4) && (i % 2 == 0) )
		{
			if(j%8==0) putchar(' ');
			dht11_dat[j / 8] <<= 1;
			if ( times[i] > 58 )
			{
				dht11_dat[j / 8] |= 1;
				putchar('1');
			}
			else
				putchar('0');
			j++;
			if(j>=48) goto endCalc;
		}*/
		if ( i >= 2 )
		{
			if(j%8==0) putchar(' ');
			dht11_dat[j / 8] <<= 1;
			if ( times[i] >  100 )
			{
				dht11_dat[j / 8] |= 1;
				putchar('1');
			}
			else
				putchar('0');
			j++;
			if(j>=48) goto endCalc;
		}
	}
	endCalc:;
	for(;j<40;j++)
	{
		putchar(' ');
	}
	printf("  ");
	//printf( "[%2d] %3d%3d%3d%3d%3d ",j,dht11_dat[0],dht11_dat[1],dht11_dat[2],dht11_dat[3],dht11_dat[4] );
	if ( (j >= 40) &&
	     (dht11_dat[4] == ( (dht11_dat[0] + dht11_dat[1] + dht11_dat[2] + dht11_dat[3]) & 0xFF) ) )
	{
		printf( "H %d.%d %% T %d.%d\n",
			dht11_dat[0], dht11_dat[1], dht11_dat[2], dht11_dat[3]);
		return {dht11_dat[0]+dht11_dat[1]/10.0,dht11_dat[2]+dht11_dat[3]/10.0};
	}else  {
		printf( "Data not good, skip\n",j,dht11_dat[0],dht11_dat[1],dht11_dat[2],dht11_dat[3],dht11_dat[4] );
		return {-100,-100};
	}
}
 
int main(int argc,char ** argv)
{
	if ( wiringPiSetup() < 0)
		exit( 1 );
	ll lastIntervalTime = 0;
	int lastIntervalCount=0;
	pair<ld,ld> lastIntervalSum={0,0};
	if(argc<=3)
	{
		printf("USAGE: %s DHTPIN NAME PERIOD[ms] [SLEEP BEFORE RUN[ms]]\n",argv[0]);
		exit(1);
	}
	if(argc>4) delay(atoi(argv[4]));
	int pin = atoi(argv[1]);
	char * name = argv[2];
	int period = atoi(argv[3]);
	for(char * it=name;*it;it++) 
		if(!('a'<=*it&&*it<='z' ||  'A'<=*it&&*it<='Z' ||  '0'<=*it&&*it<='9'))
			*it='_';
	mysqlpp::Connection conn(false);
	if ( wiringPiISR (0, INT_EDGE_FALLING, &interupt) < 0 ) {
	fprintf (stderr, "Unable to setup ISR: %s\n", strerror (errno));
	      return 1;
	  }
	while ( 1 )
	{
		if(!conn.connected() || !conn.ping())
		{
			printf("(RE)INIT DB\n");
			if (!conn.connect(databaseConfig::database, databaseConfig::host,
						databaseConfig::user, databaseConfig::passwd))
			{
				printf("DB CONECTION FAILD\n");
			}
		}
		auto in = read_dht11_dat(pin);
		fflush(stdout);
		if(in.second>=0)
		{
			char q[1234];
			sprintf(q,"insert into input (station,t,humidity,temperature) values ('%s',UTC_TIMESTAMP(),%lF,%lF)",name,in.first,in.second);
			conn.query(q).store();
			lastIntervalCount++;
			lastIntervalSum.first+=in.first;
			lastIntervalSum.second+=in.second;
		}
		if(lastIntervalTime < time(0)/intervalTime)
		{
			lastIntervalTime=time(0)/intervalTime;
			if(lastIntervalCount)
			{
				char q[1234];
				sprintf(q,"insert into input_interval (station,t,humidity,temperature) values ('%s',UTC_TIMESTAMP(),%lF,%lF)",name,lastIntervalSum.first/lastIntervalCount,lastIntervalSum.second/lastIntervalCount);
				conn.query(q).store();
			}
			lastIntervalCount=0;
			lastIntervalSum={0,0};
		}
		delay(period); 
	}

	return(0);
}
