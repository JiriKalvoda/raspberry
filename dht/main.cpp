#include "../lib/gpio.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include<fstream>
#include <unistd.h>
#include "../led/led-include.h"
#include <sys/time.h>
using namespace std;
#define MAXTIMINGS	85
using ld = double;
using ll = long long;
using ui = unsigned int;

const int intervalTime=60*5;

using Gpio::digitalWrite;
using Gpio::digitalRead;
using Gpio::pinModeOutput;
using Gpio::pinModeInput;
using Gpio::p;

unsigned long long micros()
{
	struct timespec monotime;
	clock_gettime(CLOCK_MONOTONIC, &monotime);
	return 1000000 * (unsigned long long)monotime.tv_sec + monotime.tv_nsec/1000;
}
 
pair<ld,ld> read_dht11_dat(int pin)
{
	int dht11_dat[6] = { 0, 0, 0, 0, 0,0 };
	uint8_t laststate	= 1;
	int counter		= 0;
	uint8_t j		= 0, i;
	ui times[MAXTIMINGS];
 
 
	pinModeOutput(p(pin));
	digitalWrite(p(pin), 0 );
	usleep(20 * 1000);
	//digitalWrite(p(pin), 1 );
	gpiod_line_release(p(pin));
	pinModeInput(p(pin));
	for ( i = 0; i < MAXTIMINGS; )
	{
		counter = 0;
		while ( digitalRead(p(pin)) == laststate )
		{
			counter++;
			if ( counter == 500 )
				goto end;
		}
		laststate ^= 1;
		times[i++]=micros();
	}
	end:;
	gpiod_line_release(p(pin));
	int timesLen=i;
	for(int i=timesLen-1;i>0;i--) times[i]-=times[i-1];
	for(int i=0;i<timesLen;i++)
	{
		if(i>=3 && i%2)
			while(times[i]>100)
			{
				dht11_dat[j / 8] <<= 1;
				if(j%8==0) putchar(' ');
				putchar('X');
				j++;
				if(j>=48) goto endCalc;
				times[i]-=78;
			}
		if ( (i >= 4) && (i % 2 == 0) )
		{
			if(j%8==0) putchar(' ');
			dht11_dat[j / 8] <<= 1;
			if ( times[i] > 58 )
			{
				dht11_dat[j / 8] |= 1;
				putchar('1');
			}
			else
				putchar('0');
			j++;
			if(j>=48) goto endCalc;
		}
	}
	endCalc:;
	for(int i=j;i<40;i++)
	{
		putchar(' ');
	}
	printf("  ");
	//printf( "[%2d] %3d%3d%3d%3d%3d ",j,dht11_dat[0],dht11_dat[1],dht11_dat[2],dht11_dat[3],dht11_dat[4] );
	if ( (j >= 40) &&
	     (dht11_dat[4] == ( (dht11_dat[0] + dht11_dat[1] + dht11_dat[2] + dht11_dat[3]) & 0xFF) ) )
	{
#ifdef DHTAA
		pair<ld,ld> r = {dht11_dat[0]+dht11_dat[1]/10.0,(dht11_dat[3]>=128?-1:1)*(dht11_dat[2]+dht11_dat[3]%16/10.0)};
#else
#ifdef DHTBB
		pair<ld,ld> r = {(dht11_dat[0]*256+dht11_dat[1])/10.0,(dht11_dat[2]>=0x7F?-1:1)*(((dht11_dat[2]&0x7F)*256+dht11_dat[3])/10.0)};
#else
		SELECT -DDHT11 OR -DDHT22 AS COMPILATION PARAMETR
#endif
#endif
		printf( "H %.2lf %% T %.2lf\n",r.first,r.second);
		return r;
	}else  {
		printf( "Data not good, skip\n");
		return {-100,-100};
	}
}

LED led;

ll get_msec()
{
	struct timespec tv;
	clock_gettime(CLOCK_MONOTONIC, &tv);
	return tv.tv_sec*1000ll+tv.tv_nsec/1000/1000;
}
 
int main(int argc,char ** argv)
{
	Gpio::init();
	nice(-20);
	if(argc<=3)
	{
		printf("USAGE: %s DHTPIN NAME PERIOD[ms] [SLEEP BEFORE RUN[ms]] [mosquitto]\n",argv[0]);
		exit(1);
	}
	if(argc>4) usleep(atoi(argv[4])*1000);
	Tree::Mosquitto * mosquitto = argc>5 ? Tree::mosquitto_map[argv[5]] : 0;
	int pin = atoi(argv[1]);
	char * name = argv[2];
	int period = atoi(argv[3]);
	for(char * it=name;*it;it++) 
		if(!(('a'<=*it&&*it<='z') ||  ('A'<=*it&&*it<='Z') ||  ('0'<=*it&&*it<='9') || *it=='/'))
			*it='_';

	char fname_temperature[Tree::FNAMELEN];
	char fname_humidity[Tree::FNAMELEN];
	char fname_all[Tree::FNAMELEN];
	sprintf(fname_temperature, "sensors/%s/temperature", name);
	sprintf(fname_humidity,    "sensors/%s/humidity"   , name);
	sprintf(fname_all     ,    "sensors/%s/all"        , name);
	pair<ld, ld> last = {-100, -100};
	int last_simular = 0;
	while ( 1 )
	{
		ll time_before = get_msec();
		auto in = read_dht11_dat(pin);
		fflush(stdout);


		if(in.first>=0)
		{
			if(abs(last.first-in.first) > 10 ||
					abs(last.second-in.second) > 1)
			{
				printf("Too much different, skip\n");
				last_simular = 1;
			}
			else
			{
				last_simular++;
				if(last_simular >= 3)
					if(mosquitto)
					{
						try
						{
							char msg_temperature[Tree::FLEN];
							char msg_humidity   [Tree::FLEN];
							char msg_all   [Tree::FLEN];
							sprintf(msg_temperature, "%lld\n%.2lf", (ll)time(0), in.second);
							sprintf(msg_humidity, "%lld\n%.2lf",    (ll)time(0), in.first);
							sprintf(msg_all, "{\n\"time\": %lld,\n\"temperature\": %.2lf,\n\"humidity\": %.2lf\n}",    (ll)time(0), in.second, in.first);
							led.mqttMessage(mosquitto,msg_all        ,fname_all        ,1);
							led.mqttMessage(mosquitto,msg_temperature,fname_temperature,1);
							led.mqttMessage(mosquitto,msg_humidity   ,fname_humidity   ,1);
						}
						catch(std::runtime_error * e)
						{
							printf("Runtime error: %s\n", e->what());
						}
				}
			}
			last = in;
		}
		ll time_after = get_msec();
		ll time_delay = time_before-time_after+period;
		if(time_delay>0) usleep(time_delay * 1000);
	}

	return(0);
}
